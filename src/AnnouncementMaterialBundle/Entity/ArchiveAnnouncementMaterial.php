<?php

namespace AnnouncementMaterialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArchiveAnnouncementMaterial
 *
 * @ORM\Table(name="archive_announcement_material")
 * @ORM\Entity(repositoryClass="AnnouncementMaterialBundle\Repository\ArchiveAnnouncementMaterialRepository")
 */
class ArchiveAnnouncementMaterial
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="goods", type="string", length=255)
     */
    private $goods;

    /**
     * @var int
     *
     * @ORM\Column(name="idUser", type="integer")
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=255)
     */
    private $place;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

	/**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAnnouncement", type="datetimetz", nullable=true)
     */
    private $dateAnnouncement;

	/**
     * @var integer
     *
     * @ORM\Column(name="numberDays", type="integer", nullable=true)
     */
    private $numberDays;
    

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateBegin", type="datetime")
     */
    private $dateBegin;


	/**
     * @var integer
     *
     * @ORM\Column(name="views", type="integer", nullable=true)
     */
    private $views;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="quality", type="integer", nullable=true)
     */
    private $quality;

	/**
     * @var boolean
     *
     * @ORM\Column(name="cleanliness", type="boolean", nullable=true)
     */
    private $cleanliness;
	
	/**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255)
     */
    private $state;
	
	/**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255)
     */
    private $region;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="postcode", type="integer", nullable=true)
     */
    private $postcode;
	
	/**
     * @var \Date
     *
     * @ORM\Column(name="dateEnd", type="date")
     */
    private $dateEnd;

	/**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sport")
     * @ORM\JoinColumn(name="sport_id", referencedColumnName="id")
     */
    private $sport;

	/**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $imageFirst;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $originalImageFirst;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $imageTwice;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $originalImageTwice;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $imageThird;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $originalImageThird;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

	/**
     * Set quality
     *
     * @param integer $quality
     *
     * @return AnnouncementMaterial
     */
    public function setQuality($quality)
    {
        $this->quality = $quality;

        return $this;
    }

    /**
     * Get quality
     *
     * @return integer
     */
    public function getQuality()
    {
        return $this->quality;
    }


    /**
     * Set goods
     *
     * @param string $goods
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setGoods($goods)
    {
        $this->goods = $goods;

        return $this;
    }

    /**
     * Get goods
     *
     * @return string
     */
    public function getGoods()
    {
        return $this->goods;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set dateBegin
     *
     * @param \DateTime $dateBegin
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setDateBegin($dateBegin)
    {
        $this->dateBegin = $dateBegin;

        return $this;
    }

    /**
     * Get dateBegin
     *
     * @return \DateTime
     */
    public function getDateBegin()
    {
        return $this->dateBegin;
    }

	/**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set dateAnnouncement
     *
     * @param \DateTime $dateAnnouncement
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setDateAnnouncement($dateAnnouncement)
    {
        $this->dateAnnouncement = $dateAnnouncement;

        return $this;
    }

    /**
     * Get dateAnnouncement
     *
     * @return \DateTime
     */
    public function getDateAnnouncement()
    {
        return $this->dateAnnouncement;
    }

    /**
     * Set numberDays
     *
     * @param integer $numberDays
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setNumberDays($numberDays)
    {
        $this->numberDays = $numberDays;

        return $this;
    }

    /**
     * Get numberDays
     *
     * @return integer
     */
    public function getNumberDays()
    {
        return $this->numberDays;
    }

    /**
     * Set views
     *
     * @param integer $views
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set cleanliness
     *
     * @param boolean $cleanliness
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setCleanliness($cleanliness)
    {
        $this->cleanliness = $cleanliness;

        return $this;
    }

    /**
     * Get cleanliness
     *
     * @return boolean
     */
    public function getCleanliness()
    {
        return $this->cleanliness;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set postcode
     *
     * @param integer $postcode
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return integer
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set imageFirst
     *
     * @param string $imageFirst
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setImageFirst($imageFirst)
    {
        $this->imageFirst = $imageFirst;

        return $this;
    }

    /**
     * Get imageFirst
     *
     * @return string
     */
    public function getImageFirst()
    {
        return $this->imageFirst;
    }

    /**
     * Set originalImageFirst
     *
     * @param string $originalImageFirst
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setOriginalImageFirst($originalImageFirst)
    {
        $this->originalImageFirst = $originalImageFirst;

        return $this;
    }

    /**
     * Get originalImageFirst
     *
     * @return string
     */
    public function getOriginalImageFirst()
    {
        return $this->originalImageFirst;
    }

    /**
     * Set imageTwice
     *
     * @param string $imageTwice
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setImageTwice($imageTwice)
    {
        $this->imageTwice = $imageTwice;

        return $this;
    }

    /**
     * Get imageTwice
     *
     * @return string
     */
    public function getImageTwice()
    {
        return $this->imageTwice;
    }

    /**
     * Set originalImageTwice
     *
     * @param string $originalImageTwice
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setOriginalImageTwice($originalImageTwice)
    {
        $this->originalImageTwice = $originalImageTwice;

        return $this;
    }

    /**
     * Get originalImageTwice
     *
     * @return string
     */
    public function getOriginalImageTwice()
    {
        return $this->originalImageTwice;
    }

    /**
     * Set imageThird
     *
     * @param string $imageThird
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setImageThird($imageThird)
    {
        $this->imageThird = $imageThird;

        return $this;
    }

    /**
     * Get imageThird
     *
     * @return string
     */
    public function getImageThird()
    {
        return $this->imageThird;
    }

    /**
     * Set originalImageThird
     *
     * @param string $originalImageThird
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setOriginalImageThird($originalImageThird)
    {
        $this->originalImageThird = $originalImageThird;

        return $this;
    }

    /**
     * Get originalImageThird
     *
     * @return string
     */
    public function getOriginalImageThird()
    {
        return $this->originalImageThird;
    }

    /**
     * Set sport
     *
     * @param \AppBundle\Entity\Sport $sport
     *
     * @return ArchiveAnnouncementMaterial
     */
    public function setSport(\AppBundle\Entity\Sport $sport = null)
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * Get sport
     *
     * @return \AppBundle\Entity\Sport
     */
    public function getSport()
    {
        return $this->sport;
    }
}
