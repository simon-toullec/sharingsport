<?php

namespace AnnouncementMaterialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * ReservationMaterial
 *
 * @ORM\Table(name="reservation_material")
 * @ORM\Entity(repositoryClass="AnnouncementMaterialBundle\Repository\ReservationMaterialRepository")
 */
class ReservationMaterial
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Announcement")
     * @ORM\JoinColumn(name="idAnnouncement", referencedColumnName="id")
     */
    private $idAnnouncement;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    private $idUser;
	
	/**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAnnouncement
     *
     * @param integer $idAnnouncement
     *
     * @return ReservationMaterial
     */
    public function setIdAnnouncement($idAnnouncement)
    {
        $this->idAnnouncement = $idAnnouncement;

        return $this;
    }

    /**
     * Get idAnnouncement
     *
     * @return int
     */
    public function getIdAnnouncement()
    {
        return $this->idAnnouncement;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return ReservationMaterial
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
	
	/**
     * Set type
     *
     * @param string $type
     *
     * @return ReservationMaterial
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}

