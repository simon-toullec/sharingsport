<?php

namespace AnnouncementMaterialBundle\Repository;

/**
 * ReservationMaterialRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ReservationMaterialRepository extends \Doctrine\ORM\EntityRepository
{
	
	public function findByCountReservationMaterialUser($user){
		
	  $query = $this->_em->createQuery('SELECT count(a) FROM AnnouncementMaterialBundle:ReservationMaterial a WHERE a.idUser = :user');
		  $query->setParameter('user', $user);
		  
		  // Utilisation de getSingleResult car la requête ne doit retourner qu'un seul résultat
		  return $query->getSingleResult();
	}
}
