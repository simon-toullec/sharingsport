<?php

namespace AnnouncementMaterialBundle\Controller;

use AppBundle\Entity\User;
use AnnouncementPartnerBundle\Entity\SportUser;
use AppBundle\Entity\Announcement;
use AppBundle\Entity\ConversationReceive;
use AppBundle\Entity\ConversationSend;
use AnnouncementMaterialBundle\Entity\ReservationMaterial;
use AnnouncementPartnerBundle\Entity\Participation;
use AnnouncementMaterialBundle\Entity\ArchiveAnnouncementMaterial;
use AnnouncementPartnerBundle\Entity\ArchiveAnnouncementPartner;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Comur\ImageBundle\Form\Type\CroppableImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;
use AppBundle\Model\Stripe;
use Symfony\Component\Config\Definition\Exception\Exception;
use Nzo\UrlEncryptorBundle\Annotations\ParamDecryptor;


use \DateTime;
use \DateTimeZone;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
	/**
     * @Route("/material/test", name="material_test")
     */
	 public function materialTestAction(){
		
		return $this->render('AnnouncementMaterialBundle:Default:test.html.twig');
	 }
    /**
     * @Route("/create/materialAnnouncement", name="create_material_announcement")
     */
	 public function createAnnouncementMaterialAction(){
		//creation d'une annonce materiel


		$announcement = new Announcement();
		$request=Request::createFromGlobals();

		if(null !== $this->getUser() ){
			if(is_dir('./../../../web/AnnouncementMaterial/'.$this->getUser()->getEmail()) == false){//vérification si dossier existe déjà
						echo ''.getcwd();

			//creation du dossier de l'annonce materiel avec le nom de l'email
			mkdir('./../../../web/AnnouncementMaterial/'.$this->getUser()->getEmail());
			//fonctionne


				}
		}

		//creation du formulaire de creation de materiel
        $form = $this->createFormBuilder($announcement)
			->add('place', TextType::class, [ 'attr' => [
			'id' => 'locality'] ])
			->add('sport', EntityType::class,
                ['class'=>'AppBundle:Sport',
                 'choice_label'=>'title',])

            ->add('dateBegin', DateTimeType::class, [
                    'widget' => 'single_text'
                ]
            )
			->add('dateEnd', DateTimeType::class, [
                    'widget' => 'single_text',

                ]
            )

			->add('state', TextType::class, [ 'attr' => [
						'id' => 'country',
						]
					])
			->add('region', TextType::class,[ 'attr' => [
						'id' => 'administrative_area_level_1',
						'required' => false
						]
			])
			->add('description',TextareaType::class, [
                    'required' => false
					]
				)
			->add('quality', HiddenType::class,[ 'attr' => [
						'id' => 'quality'

						]
			])
			->add('cleanliness', HiddenType::class)

			->add('price', MoneyType::class)
			
            ->add('save', SubmitType::class, array('label' => 'Poster une annonce'))
            ->getForm();

        $form->handleRequest($request);
		$sport = $form["sport"]->getData();
		$em1 = $this->getDoctrine()->getManager();
		$sport1 = $em1->getRepository('AppBundle:Sport')->findOneByTitle($sport);


		//si le formulaire est soumis et valide
        if ($form->isSubmitted()&& $form->isValid()) {

			//si l'utilisateur n'est pas connecté
			if($this->getUser() == null){
				//on envoie un message flash pour modifier le header selon la connexion de user
				$this->addFlash(
				'notice',
				'Your changes were saved!'
				);
				return $this->redirectToRoute('app_index');

			}

			//on entre les données de l'annonce créée
			$announcement->setIdUser($this->getUser());
			$announcement->setViews(0);
			$announcement->setIsValidAnnouncer(0);
			$announcement->setIsValidAdmin(0);
			
			$dateEnd=$form["dateEnd"]->getData();
            $dateBegin=$form["dateBegin"]->getData();
			$announcement->setDateBegin($dateBegin);
            $announcement->setDateEnd($dateEnd);
			$announcement->setType('material');
			$announcement->setDateAnnouncement(new \DateTime(date('Y-m-d H:m:s')));
			$announcement->setSport($sport1);
			$sport = $form["sport"]->getData();

			

            $em = $this->getDoctrine()->getManager();


			//enregistrement de l'annonce
            $em->persist($announcement);
            $em->flush();
			
			$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementMaterialBundle:ReservationMaterial');
			$reservationMaterial = $repository->findOneBy(
			array('idUser'=>$this->getUser()->getId(), 'idAnnouncement'=>$announcement->getid()));

				
			$messageAsk = \Swift_Message::newInstance()
				->setSubject('En attente de la validation de votre annonce')
				->setFrom(array('reservation@sharingsport.fr'))
				->setTo($this->getUser()->getEmail())
				->setCharset('utf-8')
				->setContentType('text/html')
				->setBody($this->render('AppBundle:Emails:reservation-ask.html.twig',array('reservationMaterial'=>$reservationMaterial,'form'=>$form->createView())));

				$this->get('mailer')->send($messageAsk);

			//redirection vers la page d'accueil
            return $this->redirectToRoute('create_material_image',array('announcement'=>$announcement->getId()));
        }
		return $this->render('AnnouncementMaterialBundle:Creation:create-announcement-material.html.twig', array('form' => $form->createView()));
	}
	
	
	
	/**
     * @Route("/material/createImage/{announcement}", name="create_material_image")
     */
	public function createImageAction($announcement){
		// dossier courant


		$request=Request::createFromGlobals();
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementCreate = $repository->findOneById($announcement);

		$form = $this->createFormBuilder($announcementCreate)
		->add('imageFirst', CroppableImageType::class, array(
					'uploadConfig' => array(
						'uploadRoute' => 'comur_api_upload',        //optional
						'uploadUrl' => $announcementCreate->getUploadRootDir($this->getUser()),       // required - see explanation below (you can also put just a dir path)
						'webDir' => $announcementCreate->getUploadDir($this->getUser()),              // required - see explanation below (you can also put just a dir path)
						'fileExt' => '*.jpg;*.gif;*.png;*.jpeg',    //optional
						'libraryDir' => null,                       //optional
						'libraryRoute' => 'comur_api_image_library', //optional
						'showLibrary' => true,                      //optional
						'saveOriginal' => 'originalImageFirst'           //optional
					),
					'cropConfig' => array(
						'minWidth' => 200,
						'minHeight' => 200,
						'aspectRatio' => true,              //optional
						'cropRoute' => 'comur_api_crop',    //optional
						'forceResize' => false,             //optional
						'thumbs' => array(                  //optional
							array(
								'maxWidth' => 200,
								'maxHeight' => 200,
								'useAsFieldImage' => true  //optional
							)
						)
					)
				))
				->add('imageTwice', CroppableImageType::class, array( 'required' => false,
					'uploadConfig' => array(
						'uploadRoute' => 'comur_api_upload',        //optional
						'uploadUrl' => $announcementCreate->getUploadRootDir($this->getUser()),       // required - see explanation below (you can also put just a dir path)
						'webDir' => $announcementCreate->getUploadDir($this->getUser()),              // required - see explanation below (you can also put just a dir path)
						'fileExt' => '*.jpg;*.gif;*.png;*.jpeg',    //optional
						'libraryDir' => null,                       //optional
						'libraryRoute' => 'comur_api_image_library', //optional
						'showLibrary' => true,                      //optional
						'saveOriginal' => 'originalImageTwice'           //optional

					),
					'cropConfig' => array(
						'minWidth' => 200,
						'minHeight' => 200,
						'aspectRatio' => true,              //optional
						'cropRoute' => 'comur_api_crop',    //optional
						'forceResize' => false,             //optional
						'thumbs' => array(                  //optional
							array(
								'maxWidth' => 200,
								'maxHeight' => 200,
								'useAsFieldImage' => true  //optional
							)
						)
					)
				))
				->add('imageThird', CroppableImageType::class, array( 'required' => false,
					'uploadConfig' => array(
						'uploadRoute' => 'comur_api_upload',        //optional
						'uploadUrl' => $announcementCreate->getUploadRootDir($this->getUser()),       // required - see explanation below (you can also put just a dir path)
						'webDir' => $announcementCreate->getUploadDir($this->getUser()),              // required - see explanation below (you can also put just a dir path)
						'fileExt' => '*.jpg;*.gif;*.png;*.jpeg',    //optional
						'libraryDir' => null,                       //optional
						'libraryRoute' => 'comur_api_image_library', //optional
						'showLibrary' => true,                      //optional
						'saveOriginal' => 'originalImageThird'//optional

					),
					'cropConfig' => array(
						'minWidth' => 200,
						'minHeight' => 200,
						'aspectRatio' => true,              //optional
						'cropRoute' => 'comur_api_crop',    //optional
						'forceResize' => false,             //optional
						'thumbs' => array(                  //optional
							array(
								'maxWidth' => 200,
								'maxHeight' => 200,
								'useAsFieldImage' => true  //optional
							)
						)
					)
				))
            ->add('save', SubmitType::class, array('label' => 'Enregistrer image'))
            ->getForm();

			$form->handleRequest($request);
			$image = $form["imageFirst"]->getData()->getImageFirst();

			if( $form->isSubmitted()){
				//$announcementCreate = $form->getData();
				$announcementCreate->setImageFirst($image);
				$em = $this->getDoctrine()->getManager();
				$em->persist($announcementCreate);
				$em->flush($announcementCreate);

				return $this->redirectToRoute('app_index');

			}
		return $this->render('AnnouncementMaterialBundle:Creation:create-image.html.twig', array('form' => $form->createView()));

	}

	/**
     * @Route("/material/addImage/{announcement}", name="add_material_image")
     */
	public function addImageAction($announcement){
		// dossier courant


		$request=Request::createFromGlobals();
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementCreate = $repository->findOneById($announcement);

		$form = $this->createFormBuilder($announcementCreate)
		->add('imageTwice', CroppableImageType::class, array( 'required' => false,
					'uploadConfig' => array(
						'uploadRoute' => 'comur_api_upload',        //optional
						'uploadUrl' => $announcementCreate->getUploadRootDir($this->getUser()),       // required - see explanation below (you can also put just a dir path)
						'webDir' => $announcementCreate->getUploadDir($this->getUser()),              // required - see explanation below (you can also put just a dir path)
						'fileExt' => '*.jpg;*.gif;*.png;*.jpeg',    //optional
						'libraryDir' => null,                       //optional
						'libraryRoute' => 'comur_api_image_library', //optional
						'showLibrary' => true,                      //optional
						'saveOriginal' => 'originalImageTwice'           //optional

					),
					'cropConfig' => array(
						'minWidth' => 200,
						'minHeight' => 200,
						'aspectRatio' => true,              //optional
						'cropRoute' => 'comur_api_crop',    //optional
						'forceResize' => false,             //optional
						'thumbs' => array(                  //optional
							array(
								'maxWidth' => 200,
								'maxHeight' => 200,
								'useAsFieldImage' => true  //optional
							)
						)
					)
				))
				->add('imageThird', CroppableImageType::class, array( 'required' => false,
					'uploadConfig' => array(
						'uploadRoute' => 'comur_api_upload',        //optional
						'uploadUrl' => $announcementCreate->getUploadRootDir($this->getUser()),       // required - see explanation below (you can also put just a dir path)
						'webDir' => $announcementCreate->getUploadDir($this->getUser()),              // required - see explanation below (you can also put just a dir path)
						'fileExt' => '*.jpg;*.gif;*.png;*.jpeg',    //optional
						'libraryDir' => null,                       //optional
						'libraryRoute' => 'comur_api_image_library', //optional
						'showLibrary' => true,                      //optional
						'saveOriginal' => 'originalImageThird'//optional

					),
					'cropConfig' => array(
						'minWidth' => 200,
						'minHeight' => 200,
						'aspectRatio' => true,              //optional
						'cropRoute' => 'comur_api_crop',    //optional
						'forceResize' => false,             //optional
						'thumbs' => array(                  //optional
							array(
								'maxWidth' => 200,
								'maxHeight' => 200,
								'useAsFieldImage' => true  //optional
							)
						)
					)
				))
            ->add('save', SubmitType::class, array('label' => 'Enregistrer les images'))
            ->getForm();

			$form->handleRequest($request);
			$imageTwice = $form["imageTwice"]->getData()->getImageTwice();
			$imageThird = $form["imageThird"]->getData()->getImageThird();

			if( $form->isSubmitted()){
				//$announcementCreate = $form->getData();
				$announcementCreate->setImageTwice($imageTwice);
				$announcementCreate->setImageThird($imageThird);
				$em = $this->getDoctrine()->getManager();
				$em->persist($announcementCreate);
				$em->flush($announcementCreate);

				return $this->redirectToRoute('app_index');

			}
		return $this->render('AnnouncementMaterialBundle:Creation:add-image.html.twig', array('form' => $form->createView()));

	}
	
	/**
     * @Route("/display/announcementMaterialDesc/{place}/{sport}/{dateBegin}/{dateEnd}", name="display_material_desc_announcement")
     */
	public function announcementMaterialDescAction($place, $sport,$dateBegin, $dateEnd){
		//affichage de toutes les annonces materiel et partenaire
		
		$quality = $_POST['quality_desc'];
		$cleanliness = $_POST['cleanliness_desc'];
		$price = $_POST['price_desc'];
		
		$announcement = new Announcement();
		$request=Request::createFromGlobals();
		$form = $this->createFormBuilder(array($announcement,'form'))
		->add('place', TextType::Class)
		->add('sport', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
		->add('dateBegin', DateTimeType::class, [
				'widget' => 'single_text'
				
			]
		)
		->add('dateEnd', DateTimeType::class, [
				'widget' => 'single_text'
				
			]
		)
		
		->add('save', SubmitType::class, array('label' => 'GO'))
		->getForm();
		
		
		$form->handleRequest($request);
		if($form->isSubmitted() && $form->isValid()){
			
			$dateBegin = $form["dateBegin"]->getData();
			$result = $dateBegin->format('Y-m-d');
			$dateEnd = $form["dateEnd"]->getData();
			$resultEnd = $dateEnd->format('Y-m-d');

			//on envoie les données dans la page d'affichage des annonces

			//récupération de l'ensemble des données des annonces materiel et partenaire selon la date le sport et le lieu
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			if($this->getUser() == null){
				$announcementMaterial = $repository->findByDateBegUnConnectedMat($place,$sport,$dateBegin,$dateEnd);
				//$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:AnnouncementPartner');
				//$announcementPartner = $repository->findByDateBegUnConnected($place,$sport,$dateBegin);

			}else{
				$announcementMaterial = $repository->findByDateBegMat($place,$sport,$dateBegin,$dateEnd, $this->getUser()->getId());
				//$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:AnnouncementPartner');
				//$announcementPartner = $repository->findByDateBeg($place,$sport,$dateBegin,$this->getuser()->getId());
			}



			$paginator  = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$announcementMaterial,
					$request->query->getInt('page', 1),
					8
				);



			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
			$favoriteMaterial = $repository->findByIdUser($this->getUser());




			//envoie des données dans la page d'affichage des annonces
			return $this->render('AnnouncementMaterialBundle:Default:announcement-material.html.twig', array('form'=>$form->createView(),/*'formFilter'=>$formFilter->createView(), 'formDesc'=>$formDesc->createView(), 'formAsc'=>$formAsc->createView(),*/'favoriteMaterial' =>$favoriteMaterial,'pagination'=>$pagination, /*'announcementPartner' =>$announcementPartner, */'place' => $place, 'sport' => $sport, 'dateBegin'=> $result, 'dateEnd' =>$resultEnd)
			);
		}
		/*$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementMaterial = $repository->findByDateBegUnConnectedDescMat($place,$sport,$dateBegin,$dateEnd);		
		*/
		
		$maxPrice = 250;
		$minPrice = $price;
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		/*$announcementMaterial = $repository->findByFilterMat($place, $sport, $dateBegin, $minPrice, $maxPrice, $dateEnd, $cleanliness, $quality, 0);*/
		
		if( strpos($request->headers->get('referer'),'announcementMaterialPlace') ==true){
				$announcementMaterial = $repository->findBy(array('place'=>$place, 'isValid'=>1));
				
				//echo ''.$t;
			
				
			}else{
				$announcementMaterial = $repository->findByFilterMat($place, $sport, $dateBegin, $minPrice, $maxPrice, $dateEnd, $cleanliness, $quality, 0);
			
				
			}
				
		$paginator  = $this->get('knp_paginator');
			$pagination = $paginator->paginate(
				$announcementMaterial,
				$request->query->getInt('page', 1),
				8
			);

		

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$favoriteMaterial = $repository->findByIdUser($this->getUser());

		
		
		//envoie des données dans la page d'affichage des annonces
		return $this->render('AnnouncementMaterialBundle:Default:announcement-material.html.twig', array('form'=>$form->createView(),/*'formFilter'=>$formFilter->createView(), 'formDesc'=>$formDesc->createView(), 'formAsc'=>$formAsc->createView(),*/'favoriteMaterial' =>$favoriteMaterial,'pagination'=>$pagination, /*'announcementPartner' =>$announcementPartner, */'place' => $place, 'sport' => $sport, 'dateBegin'=> $dateBegin, 'dateEnd' =>$dateEnd, 'quality'=>$quality, 'price'=>$price, 'cleanliness'=>$cleanliness)
		);
	}
	
	/**
     * @Route("/display/announcementMaterialAsc/{place}/{sport}/{dateBegin}/{dateEnd}", name="display_material_asc_announcement")
     */
	public function announcementMaterialAscAction($place, $sport,$dateBegin, $dateEnd){ 
		//affichage de toutes les annonces materiel et partenaire
		/*$test = $_POST['form_quality'];
		echo 'test= '.$test;
		echo''.$t;*/
		$quality = $_POST['quality_asc'];
		$cleanliness = $_POST['cleanliness_asc'];
		$price = $_POST['price_asc'];
		
		$announcement = new Announcement();
		$request=Request::createFromGlobals();
		$form = $this->createFormBuilder(array($announcement,'form'))
		->add('place', TextType::Class)
		->add('sport', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
		->add('dateBegin', DateTimeType::class, [
				'widget' => 'single_text'
				
			]
		)
		->add('dateEnd', DateTimeType::class, [
				'widget' => 'single_text'
				
			]
		)
		
		->add('save', SubmitType::class, array('label' => 'GO'))
		->getForm();
		
		
		$form->handleRequest($request);
		if($form->isSubmitted() && $form->isValid()){
			
			$dateBegin = $form["dateBegin"]->getData();
			$result = $dateBegin->format('Y-m-d');
			$dateEnd = $form["dateEnd"]->getData();
			$resultEnd = $dateEnd->format('Y-m-d');
			$place = $form["place"]->getData();
			$sport =$form["sport"]->getData();

			//on envoie les données dans la page d'affichage des annonces

			//récupération de l'ensemble des données des annonces materiel et partenaire selon la date le sport et le lieu
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		
			$announcementMaterial = $repository->findByDateBegUnConnectedMat($place,$sport,$dateBegin,$dateEnd);
				//$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:AnnouncementPartner');
				//$announcementPartner = $repository->findByDateBegUnConnected($place,$sport,$dateBegin);

			
			



			$paginator  = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$announcementMaterial,
					$request->query->getInt('page', 1),
					8
				);



			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
			$favoriteMaterial = $repository->findByIdUser($this->getUser());




			//envoie des données dans la page d'affichage des annonces
			return $this->render('AnnouncementMaterialBundle:Default:announcement-material.html.twig', array('form'=>$form->createView(),/*'formFilter'=>$formFilter->createView(), 'formDesc'=>$formDesc->createView(), 'formAsc'=>$formAsc->createView(),*/'favoriteMaterial' =>$favoriteMaterial,'pagination'=>$pagination, /*'announcementPartner' =>$announcementPartner, */'place' => $place, 'sport' => $sport, 'dateBegin'=> $result, 'dateEnd' =>$resultEnd,'quality'=>$quality, 'price'=>$price, 'cleanliness'=>$cleanliness)
			);
		}
		$maxPrice = 250;
		$minPrice = $price;
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementMaterial = $repository->findByFilterMat($place, $sport, $dateBegin, $minPrice, $maxPrice, $dateEnd, $cleanliness, $quality, 1);
		
		if( strpos($request->headers->get('referer'),'announcementMaterialPlace') ==true){
				$announcementMaterial = $repository->findBy(array('place'=>$place, 'isValid'=>1));
				
			
				
		}else{
			$announcementMaterial = $repository->findByFilterMat($place, $sport, $dateBegin, $minPrice, $maxPrice, $dateEnd, $cleanliness, $quality, 1);
		
			
		}
		$paginator  = $this->get('knp_paginator');
			$pagination = $paginator->paginate(
				$announcementMaterial,
				$request->query->getInt('page', 1),
				8
			);

		

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$favoriteMaterial = $repository->findByIdUser($this->getUser());

		
		
		//envoie des données dans la page d'affichage des annonces
		return $this->render('AnnouncementMaterialBundle:Default:announcement-material.html.twig', array('form'=>$form->createView(),/*'formFilter'=>$formFilter->createView(), 'formDesc'=>$formDesc->createView(), 'formAsc'=>$formAsc->createView(),*/'favoriteMaterial' =>$favoriteMaterial,'pagination'=>$pagination, /*'announcementPartner' =>$announcementPartner, */'place' => $place, 'sport' => $sport, 'dateBegin'=> $dateBegin, 'dateEnd' =>$dateEnd,'quality'=>$quality, 'price'=>$price, 'cleanliness'=>$cleanliness)
		);
	}
	
	/**
     * @Route("/display/announcementMaterialFilter/{place}/{sport}/{dateBegin}/{dateEnd}", name="display_material_filter_announcement")
     */
	public function announcementMaterialFilterAction($place, $sport,$dateBegin, $dateEnd){ 
		//affichage de toutes les annonces materiel et partenaire
		$request=Request::createFromGlobals();
		
			
			
		if(isset($_POST['quality_filter'])){
			$quality = $_POST['quality_filter'];
			$cleanliness = $_POST['cleanliness_filter'];
			$price = $_POST['price_filter'];
			
		}
		
		/*echo 'place='.$place;
		echo 'qualtity'.$quality;
		echo 'cleanliness='.$cleanliness;
		echo 'price'.$price;*/
		//echo''.$t;
		$announcement = new Announcement();
		
		$form = $this->createFormBuilder(array($announcement,'form'))
		->add('place', TextType::Class)
		->add('sport', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
		->add('dateBegin', DateTimeType::class, [
				'widget' => 'single_text'
				
			]
		)
		->add('dateEnd', DateTimeType::class, [
				'widget' => 'single_text'
				
			]
		)
		->add('save', SubmitType::class, array('label' => 'GO'))
		->getForm();
		
		
		$form->handleRequest($request);
		if($form->isSubmitted() && $form->isValid()){
			if(isset($_POST['quality_filter'])){
			$quality = $_POST['quality_filter'];
			$cleanliness = $_POST['cleanliness_filter'];
			$price = $_POST['price_filter'];
			
			}else{
				$quality = $_POST['quality'];
				$cleanliness = $_POST['cleanliness'];
				$price = $_POST['price'];
				
			}
			$dateBegin = $form["dateBegin"]->getData();
			$result = $dateBegin->format('Y-m-d');
			$dateEnd = $form["dateEnd"]->getData();
			$resultEnd = $dateEnd->format('Y-m-d');
			
			//on envoie les données dans la page d'affichage des annonces

			//récupération de l'ensemble des données des annonces materiel et partenaire selon la date le sport et le lieu
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			
			$announcementMaterial = $repository->findByDateBegUnConnectedMat($place,$sport,$dateBegin,$dateEnd);
			//$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:AnnouncementPartner');
			//$announcementPartner = $repository->findByDateBegUnConnected($place,$sport,$dateBegin);

			

			

			$paginator  = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$announcementMaterial,
					$request->query->getInt('page', 1),
					8
				);



			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
			$favoriteMaterial = $repository->findByIdUser($this->getUser());


			


			//envoie des données dans la page d'affichage des annonces
			return $this->render('AnnouncementMaterialBundle:Default:announcement-material.html.twig', array('form'=>$form->createView(),/*'formFilter'=>$formFilter->createView(), 'formDesc'=>$formDesc->createView(), 'formAsc'=>$formAsc->createView(),*/'favoriteMaterial' =>$favoriteMaterial,'pagination'=>$pagination, /*'announcementPartner' =>$announcementPartner, */'place' => $place, 'sport' => $sport, 'dateBegin'=> $result, 'dateEnd' =>$resultEnd,'quality'=>$quality, 'price'=>$price, 'cleanliness'=>$cleanliness)
			);
		}
		$maxPrice = 250;
		$minPrice = $price;
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementMaterial = $repository->findByFilterMat($place, $sport, $dateBegin, $minPrice, $maxPrice, $dateEnd, $cleanliness, $quality, 1);
		
		
		if( strpos($request->headers->get('referer'),'announcementMaterialPlace') ==true){
				$announcementMaterial = $repository->findBy(array('place'=>$place, 'isValid'=>1));
				
			
				
			}else{
				$announcementMaterial = $repository->findByFilterMat($place, $sport, $dateBegin, $minPrice, $maxPrice, $dateEnd, $cleanliness, $quality, 1);
			
				
			}
				
		$paginator  = $this->get('knp_paginator');
			$pagination = $paginator->paginate(
				$announcementMaterial,
				$request->query->getInt('page', 1),
				8
			);

		

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$favoriteMaterial = $repository->findByIdUser($this->getUser());

		
		
		//envoie des données dans la page d'affichage des annonces
		return $this->render('AnnouncementMaterialBundle:Default:announcement-material.html.twig', array('form'=>$form->createView(),/*'formFilter'=>$formFilter->createView(), 'formDesc'=>$formDesc->createView(), 'formAsc'=>$formAsc->createView(),*/'favoriteMaterial' =>$favoriteMaterial,'pagination'=>$pagination, /*'announcementPartner' =>$announcementPartner, */'place' => $place, 'sport' => $sport, 'dateBegin'=> $dateBegin, 'dateEnd' =>$dateEnd,'quality'=>$quality, 'price'=>$price, 'cleanliness'=>$cleanliness)
		);
	}
	
	
	/**
     * @Route("/display/announcementMaterial/{place}/{sport}/{dateBegin}/{dateEnd}", name="display_material_announcement")
     */
	public function announcementMaterialAction($place, $sport,$dateBegin, $dateEnd){
		//affichage de toutes les annonces materiel et partenaire
		$quality = $_GET['quality'];
		$cleanliness = $_GET['cleanliness'];
		$price = $_GET['price'];
		//$request=Request::createFromGlobals();
		/*$quality = $_POST['quality'];
			$cleanliness = $_POST['cleanliness'];
			$price = $_POST['price'];*/
		
		$announcement = new Announcement();
		$request=Request::createFromGlobals();
		$form = $this->createFormBuilder(array($announcement,'form'))
		->add('place', TextType::Class)
		->add('sport', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
		->add('dateBegin', DateTimeType::class, [
				'widget' => 'single_text'
				
			]
		)
		->add('dateEnd', DateTimeType::class, [
				'widget' => 'single_text'
				
			]
		)
		
		->add('save', SubmitType::class, array('label' => 'GO'))
		->getForm();

		
		
		
		
		/*$formFilter->handleRequest($request);
		$formDesc->handleRequest($request);
		$formAsc->handleRequest($request);*/
		$form->handleRequest($request);
		if($form->isSubmitted() && $form->isValid()){
			$quality = $_POST['quality'];
			$cleanliness = $_POST['cleanliness'];
			$price = $_POST['price'];
		
			$dateBegin = $form["dateBegin"]->getData();
			$result = $dateBegin->format('Y-m-d');
			$dateEnd = $form["dateEnd"]->getData();
			$resultEnd = $dateEnd->format('Y-m-d');

			//on envoie les données dans la page d'affichage des annonces

			//récupération de l'ensemble des données des annonces materiel et partenaire selon la date le sport et le lieu
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			
			$announcementMaterial = $repository->findByDateBegUnConnectedMat($place,$sport,$dateBegin,$dateEnd);
			



			$paginator  = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$announcementMaterial,
					$request->query->getInt('page', 1),
					8
				);



			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
			$favoriteMaterial = $repository->findByIdUser($this->getUser());




			//envoie des données dans la page d'affichage des annonces
			return $this->render('AnnouncementMaterialBundle:Default:announcement-material.html.twig', array('form'=>$form->createView(),/*'formFilter'=>$formFilter->createView(), 'formDesc'=>$formDesc->createView(), 'formAsc'=>$formAsc->createView(),*/'favoriteMaterial' =>$favoriteMaterial,'pagination'=>$pagination, /*'announcementPartner' =>$announcementPartner, */'place' => $place, 'sport' => $sport, 'dateBegin'=> $result, 'dateEnd' =>$resultEnd,'quality'=>$quality, 'price'=>$price, 'cleanliness'=>$cleanliness)
			);
		}

		
		
	
	

		//récupération de l'ensemble des données des annonces materiel et partenaire selon la date le sport et le lieu
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');

		if( strpos($request->headers->get('referer'),'announcementMaterialPlace') ==true){
			$announcementMaterial = $repository->findBy(array('place'=>$place, 'isValid'=>1));
		
			
		}else{
			$announcementMaterial = $repository->findByDateBegUnConnectedMat($place,$sport,$dateBegin,$dateEnd);
			
		
		}


		$paginator  = $this->get('knp_paginator');
			$pagination = $paginator->paginate(
				$announcementMaterial,
				$request->query->getInt('page', 1),
				8
			);



		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$favoriteMaterial = $repository->findByIdUser($this->getUser());




		//envoie des données dans la page d'affichage des annonces
		return $this->render('AnnouncementMaterialBundle:Default:announcement-material.html.twig', array('form'=>$form->createView(),/*'formFilter'=>$formFilter->createView(),'formDesc'=>$formDesc->createView(), 'formAsc'=>$formAsc->createView(),*/ 'favoriteMaterial' =>$favoriteMaterial,'pagination'=>$pagination, /*'announcementPartner' =>$announcementPartner, */'place' => $place, 'sport' => $sport, 'dateBegin'=>$dateBegin, 'dateEnd' =>$dateEnd,'quality'=>$quality, 'price'=>$price, 'cleanliness'=>$cleanliness)
        );

	}
	
	
	
	
	/**
     * @Route("/reservation/{idAnnouncement}/{type}", name="reservation_material")
     */
	public function reservationMaterialAction($idAnnouncement,$type){
		//page de validation de la réservation

		$request=Request::createFromGlobals();
		$reservationMaterial = new ReservationMaterial();
		$form = $this->createFormBuilder($reservationMaterial)
            ->add('save', SubmitType::class, array('label' => 'Valider la réservation'))
            ->getForm();

		$form->handleRequest($request);
		$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementMaterialBundle:ReservationMaterial');
		$reservationMaterial = $repository->findOneBy(
		array('idUser'=>$this->getUser()->getId(), 'idAnnouncement'=>$idAnnouncement));


		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcement = $repository->findOneById($idAnnouncement);

		$formRes = $this->createFormBuilder(null)
			->add('name', TextType::Class)
			->add('email', TextType::Class)
			->add('number', TextType::Class)
			->add('year', TextType::Class)
			->add('month', TextType::Class)
			->add('cvc', TextType::Class)
            ->add('save', SubmitType::class, array('label' => 'Valider la réservation'))
            ->getForm();

		$formRes->handleRequest($request);
		if ($formRes->isSubmitted() ) {
			/*echo '<SCRIPT language="Javascript">alert(\''.$idAnnouncement.'\', \'Information !\');</SCRIPT>';*/
				$name = $formRes["name"]->getData();
				$token = $_POST["stripeToken"];
				$email = $formRes["email"]->getData();

				/*$token = $_POST['stripeToken'];
				$email = $_POST['email'];
				$name = $_POST['name'];*/

				//vérification si email et name pas vide
				if( filter_var($email, FILTER_VALIDATE_EMAIL) && !empty($name) && !empty($token)){

					$stripe = new Stripe('sk_test_tjqJB3uGlQoCNOxByAcz4efh');
					$customer = $stripe->api('customers',[
					'source' => $token,
					'description' => $name,
					'email' => $email ]
					);
					/*$ch = curl_init();
					$data = [
					'source' => $token,
					'description' => $name,
					'email' => $email];
					//curl est une librairie PHP
					curl_setopt_array(//ça permet de récupérer les informations de l'utilisateur
						$ch, [
						CURLOPT_URL => 'https://api.stripe.com/v1/customers',
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_USERPWD => 'sk_test_tjqJB3uGlQoCNOxByAcz4efh',
						CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
						CURLOPT_POSTFIELDS => http_build_query($data)
						]

					);

					$customer = json_decode(curl_exec($ch));
					curl_close($ch);*/

					//amount est en centimes
					$charge = $stripe->api('charges', [ //abonnement 35min
					'amount' => 1000,
					'currency'=> 'eur',
					'customer' => $customer->id

					]);
					
					var_dump($charge);
					die('Votre paiement a bien été enregistré.');

					//pour générer une facture il faut aller dans la docs stripe/charges et il faut récupérer l'id voir min34
				}

				//charger pour faire payer l'utilisateur

				//paiement plan est un paiement récurrent exemple; 19€/mois min26 créer un plan et 33min

		}


		if ($form->isSubmitted()&& $form->isValid()) {
			$reservationMaterial = new ReservationMaterial();
			$reservationMaterial->setIdUser($this->getUser());
			$reservationMaterial->setIdAnnouncement($announcement);
			$reservationMaterial->setType($type);

			$em = $this->getDoctrine()->getManager();
			$em->persist($reservationMaterial);
			$em->flush();
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			$idAn = $repository->findOneById($idAnnouncement);
			$dateBegin =  date_format($idAn->getDateBegin(),'Y-m-d');

			//Envoi du mail de confirmation
			/*$email = new \Swift_Message('Hello Email')
					->setFrom('reservation@sharingsport.fr')//de qui vient le mail
					->setTo($this->getUser()->getEmail())
					->setCharset('utf-8')
					->setCcontentType('text/html')
					->setBody($this->renderView(''));
			$this->get('mailer')->send($message);*/

			$messageAsk = \Swift_Message::newInstance()
				->setSubject('Validation de la demande de réservation')
				->setFrom(array('reservation@sharingsport.fr'))
				->setTo($this->getUser()->getEmail())
				->setCharset('utf-8')
				->setContentType('text/html')
				->setBody($this->render('AppBundle:Emails:reservationAsk.html.twig',array('reservationMaterial'=>$reservationMaterial,'form'=>$form->createView())));

				$this->get('mailer')->send($messageAsk);

				$messageAnnouncer = \Swift_Message::newInstance()
					->setSubject('Une nouvelle réservation')
					->setFrom(array('reservation@sharingsport.fr'))
					->setTo($idAn->getIdUser()->getEmail())
					->setCharset('utf-8')
					->setContentType('text/html')
					->setBody($this->render('AppBundle:Emails:reservationAnnouncer.html.twig',array('reservationMaterial'=>$reservationMaterial,'form'=>$form->createView())));

					$this->get('mailer')->send($messageAnnouncer);

			return $this->redirectToRoute('display_announcement_material',array('place'=>$idAn->getPlace(),'sport'=>$idAn->getSport()->getTitle(),'dateBegin'=>$dateBegin));
		}


		return $this->render('AnnouncementMaterialBundle:Default:reservation-material.html.twig',array('reservationMaterial'=>$reservationMaterial,'form'=>$form->createView(),
		'formRes'=>$formRes->createView()));


	}
	
	/**
     * @Route("/display/reservation", name="display_reservation")
     */
	public function displayReservationAction(){
		//affiche les reservations et les participations de l'utilisateur

		$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementMaterialBundle:ReservationMaterial');
		$reservationMaterial = $repository->findByIdUser($this->getUser());
		$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:Participation');
		$participation = $repository->findByIdUser($this->getUser());

		return $this->render('AnnouncementMaterialBundle:Default:display-reservation.html.twig', array('reservationMaterial' => $reservationMaterial,
		'participation'=>$participation));

	}
	
	/**
     * @Route("/delete/reservation/{idAnnouncement}/{type}", name="delete_reservation")
     */
	public function deleteReservationMaterialAction($idAnnouncement, $type){
		$request=Request::createFromGlobals();
		$em = $this->getDoctrine()->getManager();

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcement = $repository->findOneById($idAnnouncement);
		$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementMaterialBundle:ReservationMaterial');
		$reservationMaterial = $repository->findOneBy(
		array('idUser'=>$this->getUser(), 'idAnnouncement'=>$announcement));

		$em->remove($reservationMaterial);
		$em->flush($reservationMaterial);


		return $this->redirectToRoute('app_index');
	}
	
	
	/**
     * @Route("/delete/materialAnnouncement", name="delete_material_announcement")
     */
	public function deleteAnnouncementMaterialAction(){
		$id = $_GET['id'];
		echo 'id= '.$id;
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementMaterial = $repository->findOneById($id);
		$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementMaterialBundle:ReservationMaterial');
		$reservationMaterial = $repository->findOneByIdAnnouncement($id);
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$favoriteMaterial = $repository->findOneByIdAnnouncementMaterial($id);


		//$announcementCreate = $form->getData();
		
		$announcementArchive = new ArchiveAnnouncementMaterial();
		$announcementArchive->setDescription($announcementMaterial->getDescription());
		$announcementArchive->setGoods($announcementMaterial->getGoods());
		$announcementArchive->setIdUser($announcementMaterial->getIdUser());
		$announcementArchive->setPlace($announcementMaterial->getPlace());
		$announcementArchive->setPrice($announcementMaterial->getPrice());
		$announcementArchive->setState($announcementMaterial->getState());
		$announcementArchive->setTitle($announcementMaterial->getTitle());
		$announcementArchive->setDateBegin($announcementMaterial->getDateBegin());
		$announcementArchive->setDateEnd($announcementMaterial->getDateEnd());
		$announcementArchive->setQuality($announcementMaterial->getQuality());
		$announcementArchive->setCleanliness($announcementMaterial->getCleanliness());
		$announcementArchive->setSport($announcementMaterial->getSport());
		$announcementArchive->setRegion($announcementMaterial->getRegion());
		$announcementArchive->setPostCode($announcementMaterial->getPostCode());
		$announcementArchive->setType($announcementMaterial->getType());
		$announcementArchive->setImageFirst($announcementMaterial->getImageFirst());
		$announcementArchive->setOriginalImageFirst($announcementMaterial->getOriginalImageFirst());
		$announcementArchive->setImageTwice($announcementMaterial->getImageTwice());
		$announcementArchive->setOriginalImageTwice($announcementMaterial->getOriginalImageTwice());
		$announcementArchive->setImageThird($announcementMaterial->getImageThird());
		$announcementArchive->setOriginalImageThird($announcementMaterial->getOriginalImageThird());
		$announcementArchive->setViews($announcementMaterial->getViews());
		$announcementArchive->setDateAnnouncement($announcementMaterial->getDateAnnouncement());
		$announcementArchive->setNumberDays($announcementMaterial->getNumberDays());

		$em = $this->getDoctrine()->getManager();
		if($reservationMaterial != null){

			$em->remove($reservationMaterial);
			$em->flush($reservationMaterial);
		}
		if($favoriteMaterial != null){

			$em->remove($favoriteMaterial);
			$em->flush($favoriteMaterial);
		}

		$em->remove($announcementMaterial);
		$em->flush($announcementMaterial);


		return $this->render('AnnouncementMaterialBundle:Modification:delete-announcement-material.html.twig',array('form'=>$form->createView()));

	}
	
	

	
	/**
     * @Route("/validate/materialAnnouncement", name="validate_material_announcement")
     */
	public function validateAnnouncementMaterialAction(){
		//page admin
		$id = $_GET['id'];
		echo ''.$id;
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementMaterial = $repository->findOneById($id);
		

		$announcementMaterial->setIsValidAdmin(1);
		$em = $this->getDoctrine()->getManager();
		$em->persist($announcementMaterial);
		$em->flush();

		$messageAsk = \Swift_Message::newInstance()
				->setSubject('Votre annonce a été validée.')
				->setFrom(array('reservation@sharingsport.fr'))
				->setTo($announcementMaterial->getIdUser()->getEmail())
				->setCharset('utf-8')
				->setContentType('text/html')
				->setBody($this->render('AppBundle:Emails:validation-announcement.html.twig', array('announcement'=>$annnouncementMaterial)));

				$this->get('mailer')->send($messageAsk);

		
		return $this->render('AppBundle:Default:index.html.twig');
		
	}
	
	/**
     * @Route("/edit/materialAnnouncement/{id}", name="edit_material_announcement")
     */
	public function editAnnouncementMaterialAction($id){
		 // create a task and give it some dummy data for this example
		 //modification de l'annonce selon l'id de l'annonce matériel

		 //on instancie l'annonce selon le paramètre
        $em = $this->getDoctrine()->getManager();
        $announcement = $em->getRepository('AppBundle:Announcement')->findOneById($id);
        $request=Request::createFromGlobals();

		//creation du formulaire
        $form = $this->createFormBuilder($announcement)

            ->add('description', TextareaType::class)

            ->add('dateBegin', DateTimeType::class,   [
                    'widget' => 'single_text',
                ]
            )
			->add('dateEnd', DateTimeType::class,   [
                    'widget' => 'single_text',
                ]
            )
			->add('imageFirst', CroppableImageType::class, array(
					'uploadConfig' => array(
						'uploadRoute' => 'comur_api_upload',        //optional
						'uploadUrl' => $announcement->getUploadRootDir($this->getUser()),       // required - see explanation below (you can also put just a dir path)
						'webDir' => $announcement->getUploadDir($this->getUser()),              // required - see explanation below (you can also put just a dir path)
						'fileExt' => '*.jpg;*.gif;*.png;*.jpeg',    //optional
						'libraryDir' => null,                       //optional
						'libraryRoute' => 'comur_api_image_library', //optional
						'showLibrary' => true,                      //optional
						'saveOriginal' => 'originalImageFirst'           //optional
					),
					'cropConfig' => array(
						'minWidth' => 200,
						'minHeight' => 200,
						'aspectRatio' => true,              //optional
						'cropRoute' => 'comur_api_crop',    //optional
						'forceResize' => false,             //optional
						'thumbs' => array(                  //optional
							array(
								'maxWidth' => 200,
								'maxHeight' => 200,
								'useAsFieldImage' => true  //optional
							)
						)
					)
				))

           

			->add('place', TextType::class)
			->add('goods', TextType::class)
			->add('quality', HiddenType::class)
			->add('sport', EntityType::class,
                ['class'=>'AppBundle:Sport',
                 'choice_label'=>'title',])
			->add('price', MoneyType::class)
			->add('state',TextType::class)
			->add('cleanliness',HiddenType::class)
			->add('region',HiddenType::class)
			->add('postcode',HiddenType::class)
            ->add('save', SubmitType::class, array('label' => 'Modifier l\'annonce'))
            ->getForm();

        $form->handleRequest($request);

		//si le formulaire est soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {

            $announcement = $form->getData();

            $em = $this->getDoctrine()->getManager();

        

			//enregistrement des données de l'annonce
            $em->persist($announcement);
            $em->flush();

			//redirection vers la page d'accueil
            return $this->redirectToRoute('app_index');
        }


		//on envoie le formulaire de modification de l'annonce materiel
		return $this->render('AnnouncementMaterialBundle:Modification:edit-announcement-material.html.twig', array(
            'form'=> $form->createView(), 'announcementMaterial'=>$announcement
        ) );
	}
}
