<?php

namespace AnnouncementPartnerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Level
 *
 * @ORM\Table(name="level")
 * @ORM\Entity(repositoryClass="AnnouncementPartnerBundle\Repository\LevelRepository")
 */
class Level
{
   /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=255)
     * @ORM\Id
     */
    private $title;

 
    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Level
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }
}

