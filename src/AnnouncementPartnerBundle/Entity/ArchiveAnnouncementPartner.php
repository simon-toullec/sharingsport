<?php

namespace AnnouncementPartnerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArchiveAnnouncementPartner
 *
 * @ORM\Table(name="archive_announcement_partner")
 * @ORM\Entity(repositoryClass="AnnouncementPartnerBundle\Repository\ArchiveAnnouncementPartnerRepository")
 */
class ArchiveAnnouncementPartner
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


	/**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAnnouncement", type="datetimetz", nullable=true)
     */
    private $dateAnnouncement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAvailable", type="datetimetz", nullable=true)
     */
    private $dateAvailable;
	
	/**
     * @var \Date
     *
     * @ORM\Column(name="dateBegin", type="date", nullable=true)
     */
    private $dateBegin;
	
	/**
     * @var \Date
     *
     * @ORM\Column(name="dateEnd", type="date", nullable=true)
     */
    private $dateEnd;
	

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="views", type="integer", nullable=true)
     */
    private $views;

	/**
     * @var integer
     *
     * @ORM\Column(name="numberDays", type="integer", nullable=true)
     */
    private $numberDays;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $imageFirst;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $originalImageFirst;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    private $idUser;

    /**
     * @ORM\ManyToOne(targetEntity="Level")
     * @ORM\JoinColumn(name="level", referencedColumnName="id")
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=255)
     */
    private $place;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sport")
     * @ORM\JoinColumn(name="sport_id", referencedColumnName="id")
     */
    private $sport;

    /**
     * @var time
     *
     * @ORM\Column(name="time", type="time")
     */
    private $time;
	
	/**
     * @var int
     *
     * @ORM\Column(name="numberPlaces", type="integer")
     */
    private $numberPlaces;
	
	/**
     * @var int
     *
     * @ORM\Column(name="remainingPlaces", type="integer")
     */
    private $remainingPlaces;

    /**
     * @var int
     *
     * @ORM\Column(name="distance", type="integer", nullable=true)
     */
    private $distance;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

	
	/**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255)
     */
    private $state;
	
	/**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255)
     */
    private $region;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="postcode", type="integer", nullable=true)
     */
    private $postcode;
	
	/**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateAnnouncement
     *
     * @param \DateTime $dateAnnouncement
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setDateAnnouncement($dateAnnouncement)
    {
        $this->dateAnnouncement = $dateAnnouncement;

        return $this;
    }

    /**
     * Get dateAnnouncement
     *
     * @return \DateTime
     */
    public function getDateAnnouncement()
    {
        return $this->dateAnnouncement;
    }

    /**
     * Set dateAvailable
     *
     * @param \DateTime $dateAvailable
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setDateAvailable($dateAvailable)
    {
        $this->dateAvailable = $dateAvailable;

        return $this;
    }

    /**
     * Get dateAvailable
     *
     * @return \DateTime
     */
    public function getDateAvailable()
    {
        return $this->dateAvailable;
    }

    /**
     * Set dateBegin
     *
     * @param \DateTime $dateBegin
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setDateBegin($dateBegin)
    {
        $this->dateBegin = $dateBegin;

        return $this;
    }

    /**
     * Get dateBegin
     *
     * @return \DateTime
     */
    public function getDateBegin()
    {
        return $this->dateBegin;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set views
     *
     * @param integer $views
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set imageFirst
     *
     * @param string $imageFirst
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setImageFirst($imageFirst)
    {
        $this->imageFirst = $imageFirst;

        return $this;
    }

    /**
     * Get imageFirst
     *
     * @return string
     */
    public function getImageFirst()
    {
        return $this->imageFirst;
    }

    /**
     * Set originalImageFirst
     *
     * @param string $originalImageFirst
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setOriginalImageFirst($originalImageFirst)
    {
        $this->originalImageFirst = $originalImageFirst;

        return $this;
    }

    /**
     * Get originalImageFirst
     *
     * @return string
     */
    public function getOriginalImageFirst()
    {
        return $this->originalImageFirst;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set numberPlaces
     *
     * @param integer $numberPlaces
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setNumberPlaces($numberPlaces)
    {
        $this->numberPlaces = $numberPlaces;

        return $this;
    }

    /**
     * Get numberPlaces
     *
     * @return integer
     */
    public function getNumberPlaces()
    {
        return $this->numberPlaces;
    }

    /**
     * Set remainingPlaces
     *
     * @param integer $remainingPlaces
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setRemainingPlaces($remainingPlaces)
    {
        $this->remainingPlaces = $remainingPlaces;

        return $this;
    }

    /**
     * Get remainingPlaces
     *
     * @return integer
     */
    public function getRemainingPlaces()
    {
        return $this->remainingPlaces;
    }

    /**
     * Set distance
     *
     * @param integer $distance
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * Get distance
     *
     * @return integer
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set postcode
     *
     * @param integer $postcode
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return integer
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    
    /**
     * Set sport
     *
     * @param \AppBundle\Entity\Sport $sport
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setSport(\AppBundle\Entity\Sport $sport = null)
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * Get sport
     *
     * @return \AppBundle\Entity\Sport
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * Set numberDays
     *
     * @param integer $numberDays
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setNumberDays($numberDays)
    {
        $this->numberDays = $numberDays;

        return $this;
    }

    /**
     * Get numberDays
     *
     * @return integer
     */
    public function getNumberDays()
    {
        return $this->numberDays;
    }

    /**
     * Set level
     *
     * @param \AnnouncementPartnerBundle\Entity\Level $level
     *
     * @return ArchiveAnnouncementPartner
     */
    public function setLevel(\AnnouncementPartnerBundle\Entity\Level $level = null)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return \AnnouncementPartnerBundle\Entity\Level
     */
    public function getLevel()
    {
        return $this->level;
    }
}
