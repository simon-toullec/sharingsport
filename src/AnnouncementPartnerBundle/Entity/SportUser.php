<?php

namespace AnnouncementPartnerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SportUser
 *
 * @ORM\Table(name="sport_user")
 * @ORM\Entity(repositoryClass="AnnouncementPartnerBundle\Repository\SportUserRepository")
 */
class SportUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sport")
     * @ORM\JoinColumn(name="sport", referencedColumnName="id", nullable=true)
     */
    private $sport;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;
	
	/**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=255, unique=false, nullable=true)
     */
    private $place;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sport
     *
     * @param string $sport
     *
     * @return SportUser
     */
    public function setSport($sport)
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * Get sport
     *
     * @return string
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * Set user
     *
     * @param string $user
     *
     * @return SportUser
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return SportUser
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }
}
