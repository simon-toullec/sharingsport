<?php

namespace AnnouncementPartnerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;



/**
 * participation
 *
 * @ORM\Table(name="Participation")
 * @ORM\Entity(repositoryClass="AnnouncementPartnerBundle\Repository\ParticipationRepository")
 */
class Participation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="owner", type="string", length=255)
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Announcement")
     * @ORM\JoinColumn(name="idAnnouncement", referencedColumnName="id")
     */
    private $idAnnouncement;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    private $idUser;

    /**
     * @var bool
     *
     * @ORM\Column(name="validation", type="boolean")
     */
    private $validation;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set owner
     *
     * @param string $owner
     *
     * @return participation
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set idAnnouncement
     *
     * @param string $idAnnouncement
     *
     * @return participation
     */
    public function setIdAnnouncement($idAnnouncement)
    {
        $this->idAnnouncement = $idAnnouncement;

        return $this;
    }

    /**
     * Get idAnnouncement
     *
     * @return string
     */
    public function getIdAnnouncement()
    {
        return $this->idAnnouncement;
    }

    /**
     * Set idUser
     *
     * @param string $idUser
     *
     * @return participation
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return string
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set validation
     *
     * @param boolean $validation
     *
     * @return participation
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * Get validation
     *
     * @return bool
     */
    public function getValidation()
    {
        return $this->validation;
    }
}

