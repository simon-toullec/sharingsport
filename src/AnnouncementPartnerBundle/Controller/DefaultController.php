<?php

namespace AnnouncementPartnerBundle\Controller;

use AppBundle\Entity\User;
use AnnouncementPartnerBundle\Entity\SportUser;
use AppBundle\Entity\Announcement;
use AppBundle\Entity\ConversationReceive;
use AppBundle\Entity\ConversationSend;
use AnnouncementMaterialBundle\Entity\ReservationMaterial;
use AnnouncementPartnerBundle\Entity\Participation;
use AnnouncementMaterialBundle\Entity\ArchiveAnnouncementMaterial;
use AnnouncementPartnerBundle\Entity\ArchiveAnnouncementPartner;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Comur\ImageBundle\Form\Type\CroppableImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;
use AppBundle\Model\Stripe;
use Symfony\Component\Config\Definition\Exception\Exception;
use Nzo\UrlEncryptorBundle\Annotations\ParamDecryptor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    
	
	/**
     * @Route("/partner/test", name="partner_test")
     */
	public function testAction(){
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcement = $repository->findOneById(10);

		$form = $this->createFormBuilder($announcement)
			->add('imageFirst', CroppableImageType::class, array(
					'uploadConfig' => array(
						'uploadRoute' => 'comur_api_upload',        //optional
						'uploadUrl' => $announcement->getUploadRootDir($this->getUser()),       // required - see explanation below (you can also put just a dir path)
						'webDir' => $announcement->getUploadDir($this->getUser()),              // required - see explanation below (you can also put just a dir path)
						'fileExt' => '*.jpg;*.gif;*.png;*.jpeg',    //optional
						'libraryDir' => null,                       //optional
						'libraryRoute' => 'comur_api_image_library', //optional
						'showLibrary' => true,                      //optional
						'saveOriginal' => 'originalImageFirst'           //optional
					),
					'cropConfig' => array(
						'minWidth' => 200,
						'minHeight' => 200,
						'aspectRatio' => true,              //optional
						'cropRoute' => 'comur_api_crop',    //optional
						'forceResize' => false,             //optional
						'thumbs' => array(                  //optional
							array(
								'maxWidth' => 200,
								'maxHeight' => 200,
								'useAsFieldImage' => true  //optional
							)
						)
					)
				))

           

			
            ->add('save', SubmitType::class, array('label' => 'Modifier l\'annonce'))
            ->getForm();
		return $this->render('AnnouncementPartnerBundle:Default:test.html.twig', array('form'=>$form->createView()));
	}
	
	/**
     * @Route("/create/partnerAnnouncement", name="create_partner_announcement")
     */
	public function createAnnouncementPartnerAction (){
		//création d'une annonce partenaire
		
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$numberAnnouncementMatParis = $repository->findByCountAnnouncementMat("Paris, France");
		$numberAnnouncementMatNantes = $repository->findByCountAnnouncementMat("Nantes, France");
		$numberAnnouncementMatLyon = $repository->findByCountAnnouncementMat("Lyon, France");
		$numberAnnouncementMatMarseille = $repository->findByCountAnnouncementMat("Marseille, France");
		$numberAnnouncementMatLorient = $repository->findByCountAnnouncementMat("Lorient, France");

		$numberAnnouncementPartParis = $repository->findByCountAnnouncementPart("Paris, France");
		$numberAnnouncementPartNantes = $repository->findByCountAnnouncementPart("Nantes, France");
		$numberAnnouncementPartLyon = $repository->findByCountAnnouncementPart("Lyon, France");
		$numberAnnouncementPartMarseille = $repository->findByCountAnnouncementPart("Marseille, France");
		$numberAnnouncementPartLorient = $repository->findByCountAnnouncementPart("Lorient, France");

		
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Message');
		$numberViewMessage = $repository->findByCountView($this->getUser());
		$repository =$this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementLastThree = $repository->findBy( array('type'=>'material', 'isValid'=>1),
		array('id'=>'desc'),
		3,
		0);

		$announcement = new Announcement();
		$request=Request::createFromGlobals();

		//création du formulaire de création de l'annonce
        $form = $this->createFormBuilder($announcement)
			->add('place', TextType::class, [ 'attr' => [
			'id' => 'locality'] ])
			->add('sport', EntityType::class,
                ['class'=>'AppBundle:Sport',
                 'choice_label'=>'title',])

            ->add('dateBegin', DateTimeType::class, [
                    'widget' => 'single_text'/*,
					'attr' => [
						'data-date-format' => 'YYYY-MM-DD HH:mm:SS',
						'id' => 'dateBegin'
					]*/
                ]
            )
			->add('time', DateTimeType::class, [
                    'widget' => 'single_text',

                ]
            )
			->add('level', EntityType::class,
                ['class'=>'AnnouncementPartnerBundle:Level',
                 'choice_label'=>'title',])


			->add('state', TextType::class, [ 'attr' => [
						'id' => 'country',
						]
					])
			->add('region', TextType::class,[ 'attr' => [
						'id' => 'administrative_area_level_1',
						'required' => false
						]
			])
			->add('description',TextareaType::class, [
                    'required' => false
					]
				)
			->add('numberPlaces', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Poster une annonce'))
            ->getForm();


        $form->handleRequest($request);
		$sport = $form["sport"]->getData();
		$em1 = $this->getDoctrine()->getManager();
		$sport1 = $em1->getRepository('AppBundle:Sport')->findOneByTitle($sport);


		//si le formulaire est soumis et valide
        if ($form->isSubmitted()&& $form->isValid()) {

			//si l'utilisateur n'est pas connecté on envoie un message qui changera le menu du header
			if($this->getUser() == null){
				$this->addFlash(
				'notice',
				'Your changes were saved!'
				);
				return $this->redirectToRoute('create_announcement_partner');

			}
			//entré des valeurs de l'annonce créée
			$announcement->setIdUser($this->getUser());
			$announcement->setType('partner');
			$announcement->setViews(0);
			$announcement->setIsValidAnnouncer(0);
			$announcement->setIsValidAdmin(0);
			$announcement->setRemainingPlaces($form['numberPlaces']->getData());
			$announcement->setNumberPlaces($form['numberPlaces']->getData());
			$announcement->setLevel($form['level']->getData());
			$announcement->setIsValid(0);
			$announcement->setNumberDays(0);
			$time=$form["time"]->getData();
			$resultTime = $time->format('H:i:s');
			
            //$dateBegin=$form["dateBegin"]->getData();
			$announcement->setTime($time);
			$announcement->setDateAnnouncement(new \DateTime(date('Y-m-d H:m:s')));
			$announcement->setSport($sport1);
			$sport = $form["sport"]->getData();

            $em = $this->getDoctrine()->getManager();


			//enregistrement de l'annonce
            $em->persist($announcement);
            $em->flush();
			
			//formulaire de la page d'accueil, recherche des annnonces materiel
		$announcementMaterial = new Announcement();
		$request=Request::createFromGlobals();
		$formMaterial = $this->createFormBuilder($announcementMaterial)
		/*->add('location', EntityType::class,
			['class'=>'ConnectBundle:Location',
			 'choice_label'=>'title',])*/
		->add('place', TextType::Class)
		->add('sport', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
		->add('dateBegin', DateTimeType::class, [
				'widget' => 'single_text'/*,
				'attr' => [
					'data-date-format' => 'YYYY-MM-DD HH:mm:SS',
					'id' => 'dateBegin'
				]*/
			]
		)
		->add('dateEnd', DateTimeType::class, [
				'widget' => 'single_text'/*,
				'attr' => [
					'data-date-format' => 'YYYY-MM-DD HH:mm:SS',
					'id' => 'dateBegin'
				]*/
			]
		)
		
		->add('save', SubmitType::class, array('label' => 'GO'))
		->getForm();
		
		
		$announcementPartner = new Announcement();
		$request=Request::createFromGlobals();
		$formPartner = $this->createFormBuilder($announcementPartner)
		/*->add('location', EntityType::class,
			['class'=>'ConnectBundle:Location',
			 'choice_label'=>'title',])*/
		->add('place', TextType::Class)
		->add('sport', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
		->add('dateBegin', DateTimeType::class, [
				'widget' => 'single_text'/*,
				'attr' => [
					'data-date-format' => 'YYYY-MM-DD HH:mm:SS',
					'id' => 'dateBegin'
				]*/
			]
		)
		->add('time', DateTimeType::class, [
				'widget' => 'single_text'/*,
				'attr' => [
					'data-date-format' => 'YYYY-MM-DD HH:mm:SS',
					'id' => 'dateBegin'
				]*/
			]
		)
		
		->add('save', SubmitType::class, array('label' => 'GO'))
		->getForm();
			
			//on envoie un mail pour dire que l'annonce est en attente de validation par l'équipe sharingsport
			$messageAnnouncer = \Swift_Message::newInstance()
					->setSubject('En attente de validation')
					->setFrom(array('reservation@sharingsport.fr'))
					->setTo($this->getUser()->getEmail())
					->setCharset('utf-8')
					->setContentType('text/html')
					->setBody($this->render('AppBundle:Emails:validation-announcement.html.twig'));

					$this->get('mailer')->send($messageAnnouncer);
					
					

			//redirection vers la page d'accueil
            return $this->redirectToRoute('app_index');
        }
		//creation de l'annonce partenaire
		return $this->render('AnnouncementPartnerBundle:Creation:create-announcement-partner.html.twig', array('form' => $form->createView()));

	}
	
	
	public function announcementPartnerAction($place,$sport,$dateBegin, $time){
		
		//echo '    place2'.$place;
		if($this->getUser() == null){
				
				$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
				$announcementPartner = $repository->findByDateBegUnConnectedPart($place,$sport,$dateBegin,$time);

			}else{
				
				$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
				$announcementPartner = $repository->findByDateBegPart($place,$sport,$dateBegin,$this->getuser()->getId(),$time);
			}

			echo'bonnnnjour';
			$request=Request::createFromGlobals();
			$paginator  = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$announcementPartner,
					$request->query->getInt('page', 1),
					8
				);
				
		return $this->render('AnnouncementPartnerBundle:Default:announcement-partner.html.twig', array('pagination'=>$pagination));

	}
	

	/**
	 * @Route("/setlocale/{language}", name="setlocale")
	 */
	public function setLocaleAction($language = null)
	{
		if($language != null)
		{
			// On enregistre la langue en session
			$this->get('session')->set('_locale', $language);
		}

		// on tente de rediriger vers la page d'origine
		$url = $this->container->get('request')->headers->get('referer');
		if(empty($url))
		{
			$url = $this->container->get('router')->generate('index');
		}

		return new RedirectResponse($url);
	}


	
	/**
     * @Route("/display/sportUser", name="display_sport_user")
     */
	public function displaySportUserAction(){
		//affiche les sportifs selon le lieu et le sport
		$request=Request::createFromGlobals();
		$sportUser = new SportUser();
		$pagination = null;

		$form = $this->createFormBuilder($sportUser)
		->add('place', TextType::Class)
		->add('sport', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
		->add('save', SubmitType::class, array('label' => 'Valider'))
		->getForm();
		$form->handleRequest($request);

		//si le formulaire est soumis et valide
		if ($form->isSubmitted() && $form->isValid()) {
			$sport = $form['sport']->getData();
			$place = $form['place']->getData();
			
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Sport');
			$sportR = $repository->findByTitle($sport);
			
			$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:SportUser');
			$sportUser = $repository->findBy(array('sport'=>$sport->getTitle(), 'place'=>$place));
			$request=Request::createFromGlobals();
				$paginator  = $this->get('knp_paginator');
					$pagination = $paginator->paginate(
						$sportUser,
						$request->query->getInt('page', 1),
						8
					);
					
			echo 'sport= '.$sport->getTitle();
			echo 'place= '.$place;
			
			return $this->render('AnnouncementPartnerBundle:Default:display-sport-user.html.twig', array('pagination'=>$pagination,'form'=>$form->createView()));
		}
				
		return $this->render('AnnouncementPartnerBundle:Default:display-sport-user.html.twig', array('pagination'=>$pagination,'form'=>$form->createView()));
	}
	
	/**
     * @Route("/participation/{idAnnouncement}/{type}", name="participation")
     */
	public function participationPartnerAction($idAnnouncement,$type){
		//page de validation de la réservation

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$idAn = $repository->findOneById($idAnnouncement);

		if($idAn->getRemainingPlaces()<= 0){
			$this->redirectToRoute('app_index');

		}else{//s'il reste encore des places pour participer à l'activité

			$request=Request::createFromGlobals();
			$participationPartner = new Participation();
			$form = $this->createFormBuilder($participationPartner)
				->add('save', SubmitType::class, array('label' => 'Valider la participation'))
				->getForm();

			$form->handleRequest($request);
			$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:Participation');
			
			if ($form->isSubmitted()&& $form->isValid()) {

				$participationPartner->setIdUser($this->getUser());
				$participationPartner->setIdAnnouncement($idAn);
				$participationPartner->setOwner($idAn->getIdUser());
				$participationPartner->setValidation(0);

				$em = $this->getDoctrine()->getManager();
				$em->persist($participationPartner);
				$em->flush();

				$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
				$announcementPartner = $repository->findOneById($idAnnouncement);
				$announcementPartner->setRemainingPlaces($announcementPartner->getRemainingPlaces()-1);

				$em->persist($announcementPartner);
				$em->flush();

				$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
				$idAn = $repository->findOneById($idAnnouncement);
				$dateBegin =  date_format($idAn->getDateBegin(),'Y-m-d');
				return $this->redirectToRoute('app_index');
			}


			return $this->render('AnnouncementPartnerBundle:Default:participation-partner.html.twig',array('form'=>$form->createView()));

		}



	}
	
	/**
     * @Route("/delete/partnerAnnouncement", name="delete_announcement_partner")
     */
	public function deleteAnnouncementPartnerAction(){
		$id = $_GET['id'];
		echo 'id= '.$id;
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementPartner = $repository->findOneById($id);
		$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:Participation');
		$reservationPartner = $repository->findOneByIdAnnouncement($id);
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$favoritePartner = $repository->findOneByIdAnnouncementPartner($id);
		
		$announcementArchive = new ArchiveAnnouncementPartner();
		$announcementArchive->setDescription($announcementPartner->getDescription());
		$announcementArchive->setIdUser($announcementPartner->getIdUser());
		$announcementArchive->setPlace($announcementPartner->getPlace());
		$announcementArchive->setState($announcementPartner->getState());
		$announcementArchive->setTitle($announcementPartner->getTitle());
		$announcementArchive->setDateBegin($announcementPartner->getDateBegin());
		$announcementArchive->setDateEnd($announcementPartner->getDateEnd());
		$announcementArchive->setTime($announcementPartner->getTime());
		$announcementArchive->setLevel($announcementPartner->getLevel());
		$announcementArchive->setSport($announcementPartner->getSport());
		$announcementArchive->setRegion($announcementPartner->getRegion());
		$announcementArchive->setPostCode($announcementPartner->getPostCode());
		$announcementArchive->setType($announcementPartner->getType());
		$announcementArchive->setViews($announcementPartner->getViews());
		$announcementArchive->setDateAnnouncement($announcementPartner->getDateAnnouncement());
		$announcementArchive->setNumberDays($announcementPartner->getNumberDays());
		$announcementArchive->setRemainingPlaces($announcementPartner->getRemainingPlaces());
		$announcementArchive->setNumberPlaces($announcementPartner->getNumberPlaces());
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($announcementArchive);
		$em->flush();
		


		//$announcementCreate = $form->getData();

		$em = $this->getDoctrine()->getManager();
		if($reservationPartner != null){

			$em->remove($reservationPartner);
			$em->flush($reservationPartner);
		}
		if($favoritePartner != null){

			$em->remove($favoritePartner);
			$em->flush($favoritePartner);
		}

		$em->remove($announcementPartner);
		$em->flush($announcementPartner);

		if($announcementPartner->getIdUser() == $this->getUser()){//on fait une vérification si le user est est l'announcer si oui on confirme la suppression
			$messageAsk = \Swift_Message::newInstance()
				->setSubject('Votre annonce a bien été supprimée')
				->setFrom(array('reservation@sharingsport.fr'))
				->setTo($this->getUser()->getEmail())
				->setCharset('utf-8')
				->setContentType('text/html')
				->setBody($this->render('AppBundle:Emails:deleteAnnouncement.html.twig'));

				$this->get('mailer')->send($messageAsk);
		}else{// l'annonce est refusée par l'admin
			$messageAsk = \Swift_Message::newInstance()
				->setSubject('Votre annonce a été refusée par l\'équipe SharingSport')
				->setFrom(array('reservation@sharingsport.fr'))
				->setTo($this->getUser()->getEmail())
				->setCharset('utf-8')
				->setContentType('text/html')
				->setBody($this->render('AppBundle:Emails:deleteAnnouncement.html.twig'));

				$this->get('mailer')->send($messageAsk);
		}

		return $this->render('AppBundle:Modification:delete-announcement-material.html.twig',array('form'=>$form->createView()));

	}
	
	/**
     * @Route("/validate/partnerAnnouncement", name="validate_partner_announcement")
     */
	public function validateAnnouncementPartnerAction(){
		//page admin
		$id = $_GET['id'];
		
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementPartner = $repository->findOneById($id);
		
		

		$announcementPartner->setIsValidAdmin(1);
		$em = $this->getDoctrine()->getManager();
		$em->persist($announcementPartner);
		$em->flush();

		$messageAsk = \Swift_Message::newInstance()
				->setSubject('Votre annonce a été validée.')
				->setFrom(array('reservation@sharingsport.fr'))
				->setTo($announcementPartner->getIdUser()->getEmail())
				->setCharset('utf-8')
				->setContentType('text/html')
				->setBody($this->render('AppBundle:Emails:validation-announcement.html.twig', array('announcement'=>$announcementPartner)));

				$this->get('mailer')->send($messageAsk);
				
		return $this->render('AppBundle:Default:index.html.twig');
		
	}
	/**
     * @Route("/edit/partnerAnnouncement/{id}", name="edit_partner_announcement")
     */
	public function editAnnouncementPartnerAction($id){
		 // create a task and give it some dummy data for this example
		 //modification de l'annonce partenaire avec envoi du formulaire

		 //on instancie l'annonce partenaire selon le paramètre
        $em = $this->getDoctrine()->getManager();
        $announcement = $em->getRepository('AppBundle:Announcement')->findById($id);
        $request=Request::createFromGlobals();

		//création du formulaire partenaire
        $form = $this->createFormBuilder($announcement)
            ->add('title', TextType::class)
            ->add('description', TextareaType::class)
            ->add('dateAnnouncement', DateTimeType::class,  [
                    'widget' => 'single_text',
                ]
            )
            ->add('dateAvailable', DateTimeType::class,   [
                    'widget' => 'single_text',
                ]
            )

			->add('goods', TextType::class)
			->add('place', TextType::class)
			->add('price', MoneyType::class)
			->add('state',TextType::class)
			->add('cleanliness',TextType::class)
			->add('region',TextType::class)
			->add('postcode',TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Location'))
            ->getForm();

        $form->handleRequest($request);

		//le formulaire est soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {

            $announcement = $form->getData();

            $em = $this->getDoctrine()->getManager();

         

			//on enregistre la modification du formulaire
            $em->persist($announcement);
            $em->flush();

			//redirection vers la page accueil
            return $this->redirectToRoute('app_index');
        }



		//retourne la page de modification de l'annonce partenaire avec le formulaire envoyé
		return $this->render('AnnouncementPartnerBundle:Modification:edit-announcement-partner.html.twig',array('form' => $form->createView())
        );
	}

	/**
     * @Route("/display/partnerMessage/{idAsker}/{idAnnouncement}", name="display_partner_message")
     */
	public function displayMessagePartnerAction($idAsker, $idAnnouncement){
		//affichage le message cliqué depuis la page message.html.twig

	$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:MessagePartner');
	$messagePartner = $repository->findBy(array('idAsker'=>$idAsker, 'idAnnouncement'=>$idAnnouncement));



    return $this->render('AppBundle:Default:display-message.html.twig', array( 'user' => $user
		, 'messagePartner'=>$messagePartner));

	}
	
	/**
     * @Route("/edit/sportUser/{id}", name="edit_sport_user")
     */
	public function editSportUserAction($id){
	  
	  $repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:SportUser');
		$sportDisplay = $repository->findByUser($this->getUser());
				
				
	  $request=Request::createFromGlobals();
	    $formSport = $this->createFormBuilder(NULL);
			$formSport->add('sport1', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
			 ->add('sport2', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
			 ->add('sport3', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
			 ->add('sport4', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
			->add('save', SubmitType::class, array('label' => 'Enregistrer les images'))
            ->getForm();
			
		$form = $formSport->getForm();
        $form->handleRequest($request);
			
		if( $form->isSubmitted() && $form->isValid()){
			$data = $form->getData();

			$sport1 = $data['sport1'];
			$sport2 = $data['sport2'];
			$sport3 = $data['sport3'];
			$sport4 = $data['sport4'];
			
			$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:SportUser');
			$sport1Find = $repository->findOneBy(array('user'=>$this->getUser(), 'sport'=>$sport1));
			$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:SportUser');
			$sport2Find = $repository->findOneBy(array('user'=>$this->getUser(), 'sport'=>$sport2));
			$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:SportUser');
			$sport3Find = $repository->findByUser(array('user'=>$this->getUser(), 'sport'=>$sport3));
			$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:SportUser');
			$sport4Find = $repository->findByUser(array('user'=>$this->getUser(), 'sport'=>$sport4));
			//$tab[]; //= array();
			if(!isset($sport1Find)){
				$tab[] = 1;
			}
			
			if(!isset($sport2Find)){
				//$tab = $tab+$tab('2');
				$tab[] = 2;
			}
			
			if(!isset($sport3Find)){
				//$tab = $tab+$tab('3');
				$tab[] = 3;
			}
			
			if(!isset($sport4Find)){
				//$tab = $tab+$tab('4');
				$tab[] = 4;
			}
			$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:SportUser');
			$em = $this->getDoctrine()->getManager();
			foreach($tab as $value){
				
				$sport1Em = $repository->findByUser($this->getUser());
				
				$sport1Em[$value]->setSport($data['sport'.$value]);
				$sport1Em[$value]->setUser($this->getUser());
				$sport1Em[$value]->setPlace($this->getUser()->getCity());
				$em->flush();
				
			}
			/*if(!isset($sport1Find)){
				$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:SportUser');
				$sport1Em = $repository->findByUser($this->getUser());
				
				$sport1Em[0]->setSport($sport1);
				$sport1Em[0]->setUser($this->getUser());
				$sport1Em[0]->setPlace($this->getUser()->getCity());
			}
			
			
			
			if(!isset($sport2Find)){
				$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:SportUser');
				$sport1Em = $repository->findByUser($this->getUser());
				
				$sport1Em[1]->setSport($sport2);
				$sport1Em[1]->setUser($this->getUser());
				$sport1Em[1]->setPlace($this->getUser()->getCity());
			
				
			}
			
			
			
			
			
			if(!isset($sport3Find)){
				$sport1Em[2]->setSport($sport3);
				$sport1Em[2]->setUser($this->getUser());
				$sport1Em[2]->setPlace($this->getUser()->getCity());
			
			}

			
			if(!isset($sport4Find)){
				$sport1Em[3]->setSport($sport4);
				$sport1Em[3]->setUser($this->getUser());
				$sport1Em[3]->setPlace($this->getUser()->getCity());
				
				
				
			}
			 

			//on enregistre la modification du formulaire
			$em->persist($sport1Em[0]);
			$em->persist($sport1Em[1]);
			$em->persist($sport1Em[2]);
			$em->persist($sport1Em[3]);
            $em->flush();*/

			//redirection vers la page accueil
            return $this->redirectToRoute('app_index');
		}
		
	    return $this->render('AnnouncementPartnerBundle:Modification:edit-sport-user.html.twig', array( 'form' => $form->createView(), 'sportDisplay'=>$sportDisplay
		));
  }
}
