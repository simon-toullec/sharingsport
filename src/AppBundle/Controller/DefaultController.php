<?php

namespace AppBundle\Controller;
use AppBundle\Entity\User;
use AppBundle\Entity\Message;
use AppBundle\Entity\Announcement;
use AppBundle\Entity\Favorite;
use AppBundle\Entity\Evaluation;
use AppBundle\Entity\Contact;
use AnnouncementPartnerBundle\Entity\SportUser;
//use AppBundle\Entity\Announcement;
use AnnouncementPartnerBundle\Entity\Participation;
use AnnouncementMaterialBundle\Entity\ArchiveAnnouncementMaterial;
use AnnouncementPartnerBundle\Entity\ArchiveAnnouncementPartner;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Comur\ImageBundle\Form\Type\CroppableImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;
use AppBundle\Model\Stripe;
use Symfony\Component\Config\Definition\Exception\Exception;
use Nzo\UrlEncryptorBundle\Annotations\ParamDecryptor;


use \DateTime;
use \DateTimeZone;




class DefaultController extends Controller
{

	 public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $token = $response->getAccessToken();
        $facebookId =  $response->getUsername(); // Facebook ID, e.g. 537091253102004
        $username = $response->getRealName();
        $email = $response->getEmail();

        // search user in database
        $result = $this->em->getRepository('AppUserBundle:User')->findOneBy(
            array(
                'facebookId' => $facebookId
            )
        );

        if(!$result) {
            $user = new User();
            $user->setEmail($email);
            $user->setUsername($username);
            $user->setFacebookId($facebookId);

            // ..
            // save to database instructions
            // ..
        }

        return $this->loadUserByUsername($username);
    }

	public function adminAction(){
		
		$this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

		$repository =  $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementMaterial = $repository->findBy(array('isValidAdmin' => 0, 'type' =>'material'));
		
		$repository =  $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementPartner = $repository->findBy(array('isValidAdmin' => 0, 'type' =>'partner'));
		
		/*
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:AnnouncementMaterial');
		$announcement = $repository->findAll();
		$em = $this->getDoctrine()->getManager();
		for ($i=0; $i<count($announcement); $i++) {
			$date1 = new DateTime($announcement[$i]->getDateAnnouncement()->format('Y-m-d'));
			$date2 = new DateTime(date('Y-m-d'));
			$diff = $date2->diff($date1)->format("%a");
			$announcement[$i]->setNumberDays($diff);

			if($diff >= 60){//on supprime les annonces de announcement pour les mettre dans archiveAnnouncementMaterial
				$announcementArchive = new ArchiveAnnouncementMaterial();
				$announcementArchive->setDescription($announcement[$i]->getDescription());
				$announcementArchive->setGoods($announcement[$i]->getGoods());
				$announcementArchive->setIdUser($announcement[$i]->getIdUser());
				$announcementArchive->setPlace($announcement[$i]->getPlace());
				$announcementArchive->setPrice($announcement[$i]->getPrice());
				$announcementArchive->setState($announcement[$i]->getState());
				$announcementArchive->setTitle($announcement[$i]->getTitle());
				$announcementArchive->setDateBegin($announcement[$i]->getDateBegin());
				$announcementArchive->setDateEnd($announcement[$i]->getDateEnd());
				$announcementArchive->setQuality($announcement[$i]->getQuality());
				$announcementArchive->setCleanliness($announcement[$i]->getCleanliness());
				$announcementArchive->setSport($announcement[$i]->getSport());
				$announcementArchive->setRegion($announcement[$i]->getRegion());
				$announcementArchive->setPostCode($announcement[$i]->getPostCode());
				$announcementArchive->setType($announcement[$i]->getType());
				$announcementArchive->setImageFirst($announcement[$i]->getImageFirst());
				$announcementArchive->setOriginalImageFirst($announcement[$i]->getOriginalImageFirst());
				$announcementArchive->setImageTwice($announcement[$i]->getImageTwice());
				$announcementArchive->setOriginalImageTwice($announcement[$i]->getOriginalImageTwice());
				$announcementArchive->setImageThird($announcement[$i]->getImageThird());
				$announcementArchive->setOriginalImageThird($announcement[$i]->getOriginalImageThird());
				$announcementArchive->setViews($announcement[$i]->getViews());
				$announcementArchive->setDateAnnouncement($announcement[$i]->getDateAnnouncement());
				$announcementArchive->setNumberDays($announcement[$i]->getNumberDays());

				//$announcementArchive->set($announcement[$i]->get);

				$em->persist($announcementArchive);
				$em->flush();

				$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementMaterialBundle:ReservationMaterial');
				$reservation = $repository->findOneByIdAnnouncement($announcement[$i]->getId());
				$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
				$favorite = $repository->findOneByIdAnnouncementMaterial($announcement[$i]->getId());
				if($favorite != null){
					$em->remove($favorite);
					$em->flush($favorite);
				}

				if($reservation != null){
					$em->remove($reservation);
					$em->flush($reservation);
				}



				$em->remove($announcement[$i]);
				$em->flush($announcement[$i]);

				

			}



		}*/


		/*$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcement = $repository->findAll();
		for ($i=0; $i<count($announcement); $i++) {
			$date1 = new DateTime($announcement[$i]->getDateAnnouncement()->format('Y-m-d'));
			$date2 = new DateTime(date('Y-m-d'));
			$diff = $date2->diff($date1)->format("%a");
			$announcement[$i]->setNumberDays($diff);

			if($diff >= 60){//on supprime les annonces de announcement pour les mettre dans archiveAnnouncementPartnerr
				$announcementArchive = new ArchiveAnnouncement();
				$announcementArchive->setDescription($announcement[$i]->getDescription());
				$announcementArchive->setIdUser($announcement[$i]->getIdUser());
				$announcementArchive->setPlace($announcement[$i]->getPlace());
				//$announcementArchive->setPrice($announcement[$i]->getPrice());
				$announcementArchive->setState($announcement[$i]->getState());
				$announcementArchive->setTitle($announcement[$i]->getTitle());
				$announcementArchive->setDateBegin($announcement[$i]->getDateBegin());
				$announcementArchive->setDateEnd($announcement[$i]->getDateEnd());
				$announcementArchive->setTime($announcement[$i]->getTime());
				$announcementArchive->setLevel($announcement[$i]->getLevel());
				$announcementArchive->setSport($announcement[$i]->getSport());
				$announcementArchive->setRegion($announcement[$i]->getRegion());
				$announcementArchive->setPostCode($announcement[$i]->getPostCode());
				$announcementArchive->setType($announcement[$i]->getType());
				$announcementArchive->setViews($announcement[$i]->getViews());
				$announcementArchive->setDateAnnouncement($announcement[$i]->getDateAnnouncement());
				$announcementArchive->setNumberDays($announcement[$i]->getNumberDays());
				$announcementArchive->setRemainingPlaces($announcement[$i]->getRemainingPlaces());
				$announcementArchive->setNumberPlaces($announcementPartner->getNumberPlaces());
				//$announcementArchive->set($announcement[$i]->get);
				$em->persist($announcementArchive);
				$em->flush();
				//$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Message');
				//$message = $repository->findByIdAnnouncement($announcement[$i]->getId());
				$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:ReservationPartner');
				$reservation = $repository->findOneByIdAnnouncement($announcement[$i]->getId());
				$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
				$favorite = $repository->findOneByIdAnnouncementMaterial($announcement[$i]->getId());

				if($favorite != null){
					$em->remove($favorite);
					$em->flush($favorite);
				}

				if($reservation != null){
					$em->remove($reservation);
					$em->flush($reservation);
				}

				$em->remove($announcement[$i]);
				$em->flush($announcement[$i]);

			}
		}

		*/

		return $this->render('AppBundle:Default:admin.html.twig', array('announcementMaterial' => $announcementMaterial, 'announcementPartner' => $announcementPartner));
		


	}
	
	public function archiveAction(){
		//$this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

		
		$repository =  $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementMaterial = $repository->findByArchiveMat(date('Y-m-d H:i:s'));
		$announcementPartner = $repository->findByArchivePart(date('Y-m-d H:i:s'));
		return $this->render('AppBundle:Default:archive.html.twig', array('announcementMaterial' =>$announcementMaterial, 'announcementPartner'=>$announcementPartner));
		
	}
	
	public function archiveAnnouncementDeleteAction(){
		$id = $_GET['id'];
		$idDec= $this->get('nzo_url_encryptor')->decrypt($id);
		$this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

		$repository =  $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcement = $repository->findOneById($id);
		$em = $this->getDoctrine()->getManager();
		
		/* =====MATERIAL===== */
		if($announcement->getType() == 'material'){
			$announcementArchive = new ArchiveAnnouncementMaterial();
			$announcementArchive->setDescription($announcement->getDescription());
			$announcementArchive->setGoods($announcement->getGoods());
			$announcementArchive->setIdUser($announcement->getIdUser());
			$announcementArchive->setPlace($announcement->getPlace());
			$announcementArchive->setPrice($announcement->getPrice());
			$announcementArchive->setState($announcement->getState());
			$announcementArchive->setTitle($announcement->getTitle());
			$announcementArchive->setDateBegin($announcement->getDateBegin());
			$announcementArchive->setDateEnd($announcement->getDateEnd());
			$announcementArchive->setQuality($announcement->getQuality());
			$announcementArchive->setCleanliness($announcement->getCleanliness());
			$announcementArchive->setSport($announcement->getSport());
			$announcementArchive->setRegion($announcement->getRegion());
			$announcementArchive->setPostCode($announcement->getPostCode());
			$announcementArchive->setType($announcement->getType());
			$announcementArchive->setImageFirst($announcement->getImageFirst());
			$announcementArchive->setOriginalImageFirst($announcement->getOriginalImageFirst());
			$announcementArchive->setImageTwice($announcement->getImageTwice());
			$announcementArchive->setOriginalImageTwice($announcement->getOriginalImageTwice());
			$announcementArchive->setImageThird($announcement->getImageThird());
			$announcementArchive->setOriginalImageThird($announcement->getOriginalImageThird());
			$announcementArchive->setViews($announcement->getViews());
			$announcementArchive->setDateAnnouncement($announcement->getDateAnnouncement());
			$announcementArchive->setNumberDays($announcement->getNumberDays());

			//$announcementArchive->set($announcement[$i]->get);

			$em->persist($announcementArchive);
			$em->flush();

			$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementMaterialBundle:ReservationMaterial');
			$reservation = $repository->findOneByIdAnnouncement($announcement->getId());
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
			$favorite = $repository->findOneByIdAnnouncement($announcement->getId());
			if($favorite != null){
				$em->remove($favorite);
				$em->flush($favorite);
			}

			if($reservation != null){
				$em->remove($reservation);
				$em->flush($reservation);
			}



			$em->remove($announcement);
			$em->flush($announcement);
		}else{
			$announcementArchive = new ArchiveAnnouncementPartner();
				$announcementArchive->setDescription($announcement->getDescription());
				$announcementArchive->setIdUser($announcement->getIdUser());
				$announcementArchive->setPlace($announcement->getPlace());
				$announcementArchive->setState($announcement->getState());
				$announcementArchive->setTitle($announcement->getTitle());
				$announcementArchive->setDateBegin($announcement->getDateBegin());
				$announcementArchive->setDateEnd($announcement->getDateEnd());
				$announcementArchive->setTime($announcement->getTime());
				$announcementArchive->setLevel($announcement->getLevel());
				$announcementArchive->setSport($announcement->getSport());
				$announcementArchive->setRegion($announcement->getRegion());
				$announcementArchive->setPostCode($announcement->getPostCode());
				$announcementArchive->setType($announcement->getType());
				$announcementArchive->setViews($announcement->getViews());
				$announcementArchive->setDateAnnouncement($announcement->getDateAnnouncement());
				$announcementArchive->setNumberDays($announcement->getNumberDays());
				$announcementArchive->setRemainingPlaces($announcement->getRemainingPlaces());
				$announcementArchive->setNumberPlaces($announcement->getNumberPlaces());

				$em->persist($announcementArchive);
				$em->flush();

				$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementPartnerBundle:Participation');
				$participation = $repository->findOneByIdAnnouncement($announcement->getId());
				$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
				$favorite = $repository->findOneByIdAnnouncement($announcement->getId());

				if($favorite != null){
					$em->remove($favorite);
					$em->flush($favorite);
				}

				if($participation != null){
					$em->remove($participation);
					$em->flush($participation);
				}

				$em->remove($announcement);
				$em->flush($announcement);
		}
		
		
	}


    public function indexAction()
    {	//page d'accueil du site


		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$numberAnnouncementMatParis = $repository->findByCountAnnouncementMat("Paris, France");
		$numberAnnouncementMatNantes = $repository->findByCountAnnouncementMat("Nantes, France");
		$numberAnnouncementMatLyon = $repository->findByCountAnnouncementMat("Lyon, France");
		$numberAnnouncementMatMarseille = $repository->findByCountAnnouncementMat("Marseille, France");
		$numberAnnouncementMatLorient = $repository->findByCountAnnouncementMat("Lorient, France");

		$numberAnnouncementPartParis = $repository->findByCountAnnouncementPart("Paris, France");
		$numberAnnouncementPartNantes = $repository->findByCountAnnouncementPart("Nantes, France");
		$numberAnnouncementPartLyon = $repository->findByCountAnnouncementPart("Lyon, France");
		$numberAnnouncementPartMarseille = $repository->findByCountAnnouncementPart("Marseille, France");
		$numberAnnouncementPartLorient = $repository->findByCountAnnouncementPart("Lorient, France");


		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Message');
		$numberViewMessage = $repository->findByCountView($this->getUser());
		/*$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$oneAnnouncement = $repository->findOneById('39');*/
		$repository =$this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementLastThree = $repository->findBy( array('type'=>'material', 'isValid'=>1),
		array('id'=>'desc'),
		3,
		0);

		$place = "Paris, France";
		$qb = $repository->createQueryBuilder('a');
		$qb->select('count(a.id)');
		$qb->where('a.place = :place');
		$qb->setParameter('place', $place);
		//$nombre = $qb->getQuery()->getSingleScalarResult();

		//formulaire de la page d'accueil, recherche des annnonces materiel
		$announcementMaterial = new Announcement();
		$request=Request::createFromGlobals();
		$formMaterial = $this->createFormBuilder($announcementMaterial)
		/*->add('location', EntityType::class,
			['class'=>'ConnectBundle:Location',
			 'choice_label'=>'title',])*/
		->add('place', TextType::Class)
		->add('sport', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
		->add('dateBegin', DateTimeType::class, [
				'widget' => 'single_text'/*,
				'attr' => [
					'data-date-format' => 'YYYY-MM-DD HH:mm:SS',
					'id' => 'dateBegin'
				]*/
			]
		)
		->add('dateEnd', DateTimeType::class, [
				'widget' => 'single_text'/*,
				'attr' => [
					'data-date-format' => 'YYYY-MM-DD HH:mm:SS',
					'id' => 'dateBegin'
				]*/
			]
		)
		
		->add('save', SubmitType::class, array('label' => 'GO'))
		->getForm();
		$formMaterial->handleRequest($request);
		
		$place = $formMaterial["place"]->getData();
		$sport = $formMaterial["sport"]->getData();

		

		//si le formulaire est soumis et valide
		if ($formMaterial->isSubmitted() && $formMaterial->isValid()) {
			$quality = $_POST['quality'];
			$cleanliness = $_POST['cleanliness'];
			$price = $_POST['price'];
			
			$dateBegin = $formMaterial["dateBegin"]->getData();
			$result = $dateBegin->format('Y-m-d');

			$dateEnd = $formMaterial["dateEnd"]->getData();
			$resultEnd = $dateEnd->format('Y-m-d');


			echo 'Place: '.$place;

			//return $this->redirectToRoute('app_announcement_material',$place,$sport);
			//$this->redirectToRoute('app_announcement_material', array('place' => $place, 'sport' => $sport));
			//$this->redirectToRoute('app_announcement_material', ['place' => $place], ['sport' => $sport]);

			//on envoie les données dans la page d'affichage des annonces
			return $this->redirectToRoute('display_material_announcement', array(
			'place' =>$place, 'sport' => $sport->getTitle(),'dateBegin' =>$result, 'dateEnd'=>$resultEnd,'quality'=>$quality, 'price'=>$price, 'cleanliness'=>$cleanliness));

		}
		
		
		$announcementPartner = new Announcement();
		$request=Request::createFromGlobals();
		$formPartner = $this->createFormBuilder($announcementPartner)
		/*->add('location', EntityType::class,
			['class'=>'ConnectBundle:Location',
			 'choice_label'=>'title',])*/
		->add('place', TextType::Class)
		->add('sport', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
		->add('dateBegin', DateTimeType::class, [
				'widget' => 'single_text'/*,
				'attr' => [
					'data-date-format' => 'YYYY-MM-DD HH:mm:SS',
					'id' => 'dateBegin'
				]*/
			]
		)
		->add('time', DateTimeType::class, [
				'widget' => 'single_text'/*,
				'attr' => [
					'data-date-format' => 'YYYY-MM-DD HH:mm:SS',
					'id' => 'dateBegin'
				]*/
			]
		)
		
		->add('save', SubmitType::class, array('label' => 'GO'))
		->getForm();
		$formPartner->handleRequest($request);
		
		//si le formulaire est soumis et valide
		if ($formPartner->isSubmitted() && $formPartner->isValid()) {
			$dateBegin = $formPartner["dateBegin"]->getData();
			$result = $dateBegin->format('Y-m-d');
			$place = $formPartner["place"]->getData();
			$time = $formPartner["time"]->getData();
			echo 'place='.$place;
			$resultTime = $time->format('H:i:s');


			//echo 'Place: '.$place;

			//return $this->redirectToRoute('app_announcement_material',$place,$sport);
			//$this->redirectToRoute('app_announcement_material', array('place' => $place, 'sport' => $sport));
			//$this->redirectToRoute('app_announcement_material', ['place' => $place], ['sport' => $sport]);

			//on envoie les données dans la page d'affichage des annonces
			return  $this->forward('AppBundle:Default:announcementPartner', array(
			'place' =>$place, 'sport' => $sport->getTitle(),'dateBegin' =>$result, 'time'=>$resultTime));

		}
		

		$user =$this->getUser();
		//si utilisateur est null
		if($user == null){
			//on envoie sur la page de index
			return $this->render('AppBundle:Default:index.html.twig',array('announcementLastThree'=>$announcementLastThree,
			'numberAnnouncementParis' =>$numberAnnouncementMatParis,
			'numberAnnouncementMarseille' =>$numberAnnouncementMatMarseille,
			'numberAnnouncementNantes' =>$numberAnnouncementMatNantes,
			'numberAnnouncementLyon' =>$numberAnnouncementMatLyon,
			'numberAnnouncementLorient' =>$numberAnnouncementMatLorient,
			'numberViewMessage' => $numberViewMessage,
			'form' => $formMaterial->createView(),
			'formPartner' => $formPartner->createView()));

		}
		else{
			//user est connecté on envoie sur la page index avec menu changé (mode profil)
			return $this->render('AppBundle:Default:index.html.twig',array('announcementLastThree'=>$announcementLastThree,
			'numberAnnouncementParis' =>$numberAnnouncementMatParis,
			'numberAnnouncementMarseille' =>$numberAnnouncementMatMarseille,
			'numberAnnouncementNantes' =>$numberAnnouncementMatNantes,
			'numberAnnouncementLyon' =>$numberAnnouncementMatLyon,
			'numberAnnouncementLorient' =>$numberAnnouncementMatLorient,
			'numberViewMessage' => $numberViewMessage,
			'formPartner' => $formPartner->createView(),
			'form' => $formMaterial->createView(), 'user' =>$user));
		}



	}



   public function updateFavoriteAction(){
	   //fonction qui intéragit avec la fonction ajax dans la page d'affichage des announce

		//return $this->render('AppBundle:Default:index.html.twig',array('form' => $form->createView()));

		//récupération des données de la fonction ajax()
	   $id = $_GET['id'];
       $type = $_GET['type'];
	   $user = $_GET['user'];
	   

	   //si l'annonce est de type matériel
	   if($type == "material"){
		    $favorite = new Favorite();
			$em = $this->getDoctrine()->getManager();
			$favorite->setIdUser($this->getUser()->getId());
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			//on cherche l' annonce selon le type et id
			$announcement = $repository->findOneBy(
				  array('type' => $type, 'id' => $id));
			$favorite->setIdAnnouncementMaterial($announcement);
			/*$favorite->setIdVal(6);
			$em->persist($favorite);
			$em->flush();*/

			$favorite->setIdVal($this->getUser()->getId()+"/"+$id);
			//enregistrement du favoris matériel
			$em->persist($favorite);
			$em->flush();

	   }else{
		   //traitement des annonces partenaires
		   $favorite = new Favorite();
		   $em = $this->getDoctrine()->getManager();
			$favorite->setIdUser($this->getUser()->getId());
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			$announcement = $repository->findOneBy(
				  array('type' => $type, 'id' => $id));
			$favorite->setIdAnnouncementPartner($announcement->getId());

			//enregistrement du favoris partenaire
			$favorite->setIdVal($this->getUser()->getId()+"/"+$id);
			$em->persist($favorite);
			$em->flush();

			
	   }


		$this->redirectToRoute('app_about_us');

   }

	public function createChoiceAction(){
		//renvoi la page qui permet de choisir entre deposer annonce materiel ou partenaire
		return $this->render('AppBundle:Creation:create-choice.html.twig');

	}

	public function announcementPlaceAction($place){
		//affiche les annonces selon le lieu depuis l'index en cliquant sur une image du lieu
		
		$request=Request::createFromGlobals();
		$announcement = new Announcement();
		$request=Request::createFromGlobals();
		$form = $this->createFormBuilder($announcement)
		/*->add('location', EntityType::class,
			['class'=>'ConnectBundle:Location',
			 'choice_label'=>'title',])*/
		->add('place', TextType::Class)
		->add('sport', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
		->add('dateBegin', DateTimeType::class, [
				'widget' => 'single_text'/*,
				'attr' => [
					'data-date-format' => 'YYYY-MM-DD HH:mm:SS',
					'id' => 'dateBegin'
				]*/
			]
		)
		->add('dateEnd', DateTimeType::class, [
				'widget' => 'single_text'
				
			]
		)
		->add('save', SubmitType::class, array('label' => 'GO'))
		->getForm();
		$form->handleRequest($request);

		//si le formulaire est soumis et valide
		if ($form->isSubmitted() && $form->isValid()) {
			$dateBegin = $form["dateBegin"]->getData();
			$result = $dateBegin->format('Y-m-d');

			$quality = $_POST['quality'];
			$cleanliness = $_POST['cleanliness'];
			$price = $_POST['price'];
			$dateEnd = $form["dateEnd"]->getData();
			$resultEnd = $dateEnd->format('Y-m-d'); 


			//on envoie les données dans la page d'affichage des annonces
			return $this->redirectToRoute('display_material_announcement', array('place' =>$place, 'sport' => $sport->getTitle(),'dateBegin' =>$result,'dateEnd' => date('Y-m-d', strtotime('+4 months')), 'price' => 0, 'quality' => 6, 'cleanliness' =>-1));

		}

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementMaterial = $repository->findBy(array('place'=>$place, 'isValid'=>1,'type'=>'material'));

		$paginator  = $this->get('knp_paginator');
			$pagination = $paginator->paginate(
				$announcementMaterial, /* query NOT result */
				$request->query->getInt('page', 1)/*page number*/,
				8
			);


		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$favoriteMaterial = $repository->findBy(array('idUser'=>$this->getUser(),'type'=>'material'));

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementPartner = $repository->findBy(array('place'=>$place, 'isValid'=>1,'type'=>'partner'));

		//envoie des données dans la page d'affichage des annonces
		return $this->render('AnnouncementMaterialBundle:Default:announcement-material.html.twig', array('form'=>$form->createView(),'favoriteMaterial' =>$favoriteMaterial,'pagination' =>$pagination, 'announcementPartner' =>$announcementPartner, 'sport'=>'sport','place' => $place, 'price' => 0, 'quality' => 6, 'cleanliness' =>-1, 'dateBegin' => '2016-12-31', 'dateEnd' => date('Y-m-d', strtotime('+4 months')))
        );
	}

	public function announcementPartnerAction($place,$sport,$dateBegin, $time){
		
	//????????????????
		if($this->getUser() == null){
				
				$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
				$announcementPartner = $repository->findByDateBegUnConnectedPart($place,$sport,$dateBegin,$time);

			}else{
				
				$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
				$announcementPartner = $repository->findByDateBegPart($place,$sport,$dateBegin,$this->getuser()->getId(),$time);
			}

			echo'bonnnnjour';
			$request=Request::createFromGlobals();
			$paginator  = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$announcementPartner,
					$request->query->getInt('page', 1),
					8
				);
				
		return $this->render('AnnouncementPartnerBundle:Default:announcement-partner.html.twig', array('pagination'=>$pagination));

	}
	
	

	public function displayAnnouncementAction($id, $type){
		//affichage de l'annonce en détail lors du click sur l'image 
		if($type == "material"){
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			$announcement = $repository->findOneBy(array('type' => $type, 'id' => $id));
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:User');
			$user = $repository->findOneById($announcement->getIdUser());
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Sport');
			$sport = $repository->findOneByTitle($announcement->getSport());

			$views = $announcement->getViews();
			$announcement->setViews($views + 1);
			$em = $this->getDoctrine()->getManager();
			$em->persist($announcement);
			$em->flush();
		}else{

			

			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			$announcement = $repository->findOneBy(
			  array('type' => $type, 'id' => $id));
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:User');
			$user = $repository->findById($announcement->getIdUser());
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			$sport = $repository->findOneBySport($announcement->getSport());
			
			$views = $announcement->getViews();
			$announcement->setViews($views + 1);
			$em = $this->getDoctrine()->getManager();
			$em->persist($announcement);
			$em->flush();
		}




		return $this->render('AppBundle:Default:display-announcement.html.twig', array('announcement' =>$announcement, 'user'=>$user, 'sport2' =>$sport));



	}
	

	


	public function profileModifyAction(){
		//modification du profil


		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$numberAnnouncementUser = $repository->findByCountAnnouncementUser($this->getUser());

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$numberFavoriteMaterialUser = $repository->findByCountFavoriteMaterialUser($this->getUser());

		$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementMaterialBundle:ReservationMaterial');
		$numberReservationMaterialUser = $repository->findByCountReservationMaterialUser($this->getUser());


		$user = $this->getUser();
		//$user = new User();
		//$user->setImage('apple-icon.png');
		$request=Request::createFromGlobals();
        $form = $this->createFormBuilder($user)
            ->add('name', TextType::class)
            ->add('firstname', TextType::class)
            ->add('age', DateTimeType::class,  [
                    'widget' => 'single_text',
                ]
            )
            ->add('gender', TextType::class)
            ->add('bio', TextAreaType::class)
			->add('numberPhone', TextType::class)
			->add('stateLoc', TextType::class)
			->add('city', TextType::class)
			->add('image', CroppableImageType::class, array(
					'uploadConfig' => array(
						'uploadRoute' => 'comur_api_upload',        //optional
						'uploadUrl' => $user->getUploadRootDir(),       // required - see explanation below (you can also put just a dir path)
						'webDir' => $user->getUploadDir(),              // required - see explanation below (you can also put just a dir path)
						'fileExt' => '*.jpg;*.gif;*.png;*.jpeg',    //optional
						'libraryDir' => null,                       //optional
						'libraryRoute' => 'comur_api_image_library', //optional
						'showLibrary' => true,                      //optional
						'saveOriginal' => 'originalImage'           //optional
					),
					'cropConfig' => array(
						'minWidth' => 200,
						'minHeight' => 200,
						'aspectRatio' => true,              //optional
						'cropRoute' => 'comur_api_crop',    //optional
						'forceResize' => false,             //optional
						'thumbs' => array(                  //optional
							array(
								'maxWidth' => 200,
								'maxHeight' => 200,
								'useAsFieldImage' => true  //optional
							)
						)
					)
				))

            ->add('save', SubmitType::class, array('label' => 'Modifier profile'))
            ->getForm();

        $form->handleRequest($request);

		//formulaire soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();

            $em = $this->getDoctrine()->getManager();


			//enregistrement des données de l'utilisateur
            $em->persist($user);
            $em->flush();

			//redirection vers la page d'accueil
            return $this->redirectToRoute('app_index');
        }


		//affichage de la page profil avec l'envoi du formulaire
		return $this->render('AppBundle:Default:profile-modify.html.twig', array('form' => $form->createView(), 'user' => $user,
		'numberFavoriteMaterialUser'=>$numberFavoriteMaterialUser,
		'numberAnnouncementUser'=>$numberAnnouncementUser,
		'numberReservationMaterialUser'=>$numberReservationMaterialUser));

	}

	public function displayProfileAnnouncementAction(){
		//affiche les annonces du profile

		$request=Request::createFromGlobals();
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementMaterial = $repository->findBy(array('idUser'=>$this->getUser(),'type'=>'material', 'isValid'=>1));
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementPartner = $repository->findBy(array('idUser'=>$this->getUser(),'type'=>'partner', 'isValid'=>1));

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$numberAnnouncementUser = $repository->findByCountAnnouncementUser($this->getUser());

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$numberFavoriteMaterialUser = $repository->findByCountFavoriteMaterialUser($this->getUser());

		$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementMaterialBundle:ReservationMaterial');
		$numberReservationMaterialUser = $repository->findByCountReservationMaterialUser($this->getUser());


		$paginator  = $this->get('knp_paginator');
			$pagination = $paginator->paginate(
				$announcementMaterial, /* query NOT result */
				$request->query->getInt('page', 1)/*page number*/,
				8
			);

		$form = $this->createFormBuilder($announcementMaterial)
            ->add('save', SubmitType::class, array('label' => 'Supprimer l\'annonce'))
            ->getForm();


		return $this->render('AppBundle:Default:display-profile-announcement.html.twig', array('form' => $form->createView(),'pagination' => $pagination,
		'announcementPartner'=>$announcementPartner,
		'numberFavoriteMaterialUser'=>$numberFavoriteMaterialUser,
		'numberAnnouncementUser'=>$numberAnnouncementUser,
		'numberReservationMaterialUser'=>$numberReservationMaterialUser));

	}

	public function displayFavoriteAction(){
		//affichage des favoris ...??
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$favoriteMaterial = $repository->findBy(array($this->getUser(),'type'=>'material'));
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$favoritePartner = $repository->findBy(array($this->getUser(),'type'=>'partner'));

		return $this->render('AppBundle:Default:favorite.html.twig', array('favoriteMaterial' => $favoriteMaterial,
		'favoritePartner'=>$favoritePartner));


	}

	public function deleteAnnouncementMaterialSecurityAction(){
		$id = $_GET['id'];
		$em = $this->getDoctrine()->getManager();
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcement = $repository->findOneById($id);
		$messageAsk = \Swift_Message::newInstance()
				->setSubject('Votre annonce a été refusée.')
				->setFrom(array('reservation@sharingsport.fr'))
				->setTo($announcement->getIdUser()->getEmail())
				->setCharset('utf-8')
				->setContentType('text/html')
				->setBody($this->render('AppBundle:Emails:decline-announcement.html.twig', array('announcement' =>$announcement)));

				$this->get('mailer')->send($messageAsk);
		$announcement->setIsValidAdmin(null);
		//$em->remove($announcement);
		//$em->flush($announcement);
		
		$em->persist($announcement);
        $em->flush();
		
		
	}

	public function deleteAnnouncementPartnerSecurityAction(){
		$id = $_GET['id'];
		$em = $this->getDoctrine()->getManager();
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcement = $repository->findOneById($id);
		$messageAsk = \Swift_Message::newInstance()
				->setSubject('Votre annonce a été refusée.')
				->setFrom(array('reservation@sharingsport.fr'))
				->setTo($announcement->getIdUser()->getEmail())
				->setCharset('utf-8')
				->setContentType('text/html')
				->setBody($this->render('AppBundle:Emails:decline-announcement.html.twig', array('announcement' =>$announcement)));

				$this->get('mailer')->send($messageAsk);
		$announcement->setIsValidAdmin(null);
		//$em->remove($announcement);
		//$em->flush($announcement);
		
		$em->persist($announcement);
        $em->flush();
		
		
	}

	public function deleteFavoriteAction(){
		//suppression du favoris
		$id = $_GET['id'];
		$em = $this->getDoctrine()->getManager();
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$favoriteMaterial = $repository->findOneById($id);

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$favoritePartner = $repository->findOneById($id);

		if($favoriteMaterial != null){
			$em->remove($favoriteMaterial);
			$em->flush($favoriteMaterial);
		}else{
			$em->remove($favoritePartner);
			$em->flush($favoritePartner);

		}

		return $this->redirectToRoute('app_index');

	}
	
	
	public function mailDeclineAnnouncerAction($id){
		//ici on le user a cliqué sur le mail de redirection pour pouvoir modifier la description de son annonce qui comportait des mots vulgaires
		$idDec= $this->get('nzo_url_encryptor')->decrypt($id);
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcement = $repository->findById($idDec);
		
		$request=Request::createFromGlobals();
		//creation du formulaire
        $form = $this->createFormBuilder($announcement)
			->add('description',TextAreaType::class)
            ->add('save', SubmitType::class, array('label' => 'Modifier l\'annonce'))
            ->getForm();

        $form->handleRequest($request);
		
		//si le formulaire est soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			$announcement = $repository->findOneById($idDec);

            $em = $this->getDoctrine()->getManager();
			$announcement->setIsValidAdmin(0);
			$announcement->setDescription($form['description']->getData());

			//enregistrement des données de l'annonce
            $em->persist($announcement);
            $em->flush();
			return $this->redirectToRoute('app_index');
		}
		
		return $this->render('AppBundle:Default:mail-decline-announcer.html.twig',array('announcement'=>$announcement,'form'=> $form->createView()));
	}
	
	
	public function mailValidateAnnouncerAction($id){
		//mail de validation envoyé dans le mail de l'announcer pour valider l'annonce partner
		//echo '$id = '.$id;
		$idDec= $this->get('nzo_url_encryptor')->decrypt($id);
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementPartner = $repository->findById($idDec);
		$request=Request::createFromGlobals();
		//creation du formulaire
        $form = $this->createFormBuilder($announcementPartner)
            ->add('save', SubmitType::class, array('label' => 'Modifier l\'annonce'))
            ->getForm();

        $form->handleRequest($request);

		//si le formulaire est soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {

            $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			$announcementPartner = $repository->findOneById($idDec);

            $em = $this->getDoctrine()->getManager();
			$announcementPartner->setIsValidAnnouncer(1);

			//enregistrement des données de l'annonce
            $em->persist($announcementPartner);
            $em->flush();
			
			if($announcementPartner->getIsValidAnnouncer() == 1 && $announcementPartner->getIsValidAdmin() == 1){
				$announcementPartner->setIsValid(1);

				//enregistrement des données de l'annonce
				$em->persist($announcementPartner);
				$em->flush();
			}
			//redirection vers la page d'accueil
            return $this->redirectToRoute('app_index');
        }
		
		return $this->render('AppBundle:Default:mail-validate-announcer.html.twig',array('announcementPartner'=>$announcementPartner,'form'=> $form->createView()));
	}
	

	

	public function favoriteAction($idUser){
		//???????
		//gros bug c'est cette route qui est lancée alors que dans le routing c'est displayFavoriteAction

		//$em = $this->getDoctrine()->getManager();
        /*$announcement = $em->getRepository('AppBundle:Announcement')->findBySport($sport);
		$announcement = $em->getRepository('AppBundle:Announcement')->findByPlace($place);
        */
		 $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$numberAnnouncementUser = $repository->findByCountAnnouncementUser($this->getUser());

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$numberFavoriteMaterialUser = $repository->findByCountFavoriteMaterialUser($this->getUser());

		$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementMaterialBundle:ReservationMaterial');
		$numberReservationMaterialUser = $repository->findByCountReservationMaterialUser($this->getUser());



		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$favoriteMaterial = $repository->findBy(array('idUser' => $this->getUser(),'type'=>'material'));
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$favoritePartner = $repository->findBy(array('idUser' => $this->getUser(),'type'=>'partner'));

		return $this->render('AppBundle:Default:favorite.html.twig', array('favoriteMaterial' => $favoriteMaterial,
		'favoritePartner'=>$favoritePartner,
		'numberFavoriteMaterialUser'=>$numberFavoriteMaterialUser,
		'numberAnnouncementUser'=>$numberAnnouncementUser,
		'numberReservationMaterialUser'=>$numberReservationMaterialUser));
	}



	public function insertMessageMaterialAction(){
		// utilisation de la fonction ajax pour envoyer un message
		 //echo "<script>alert(\"la variable est nulle\")</script>";

		//récupération des données envoyé par la fonction ajax()
		$messageM = $_GET['message'];
		$idAnnouncer = $_GET['idAnnouncer'];
		$idAnnouncement = $_GET['idAnnouncement'];
		$type = $_GET['type'];
		echo " message: ".$messageM;
		echo 'idAnnouncer= '.$idAnnouncer;
		echo " idAnnouncement: ".$idAnnouncement;
		echo " type: ".$type;

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:User');
		$userAnnouncer = $repository->findOneById($idAnnouncer);//recherche de l'annonceur de l'annonce
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcement = $repository->findOneById($idAnnouncement);//recherche de l'annonce
		echo ' announcement= '.$userAnnouncer->getId();

		if($this->getUser() == $userAnnouncer ){// le idAnnouncer est trouvé
			//on cherche si la conversation existe
			// si l'utilisateur existe dans conversationReceive ou conversationSend

			//s'il existe dans l'une ou l'autre on ne fait pas d'insertion dans les tables conversation, on récupère l'id conversation et on insère le message
			//sinon on fait une insertion dans les tables conversations
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Conversation');
			$conversationR = $repository->findOneBy(array('idUser'=>$userAnnouncer,'idAnnouncement'=>$announcement, 'type' => 'receive'));
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Message');
			$messageMaterial = $repository->findOneBy(array('idAnnouncement'=>$announcement,'idAnnouncer'=>$userAnnouncer, 'idConversationReceive'=>$conversationR, 'type'=>'material'));
			echo ' conversationR= '.$conversationR->getId();
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Conversation');
			$idConversationSend = $repository->findOneBy(array('idUser'=>$messageMaterial->getIdViewer(),'idAnnouncement'=>$announcement, 'type' => 'send'));


			if($type == "material"){
				$message = new Message();
				$message->setType("material");

			}else{//message d'une annonce partner
				//message des annonces partenaires
				$message = new Message();
				$message->setType("partner");

				//envoi du mesage
			}
			$numberMessages = $idConversationSend->getNumberMessages();
			$idConversationSend->setViews($numberMessages + 1);
			$em = $this->getDoctrine()->getManager();
			$em->persist($idConversationSend);
			$em->flush();

			$numberMessages = $conversationR->getNumberMessages();
			$conversationR->setViews($numberMessages + 1);
			$em = $this->getDoctrine()->getManager();
			$em->persist($conversationR);
			$em->flush();

			$message->setIdViewer($idConversationSend->getIdUser());
			$message->setIdAnnouncer($userAnnouncer);
			$message->setIdConversationSend($idConversationSend);
			$result = new \DateTime(date('Y-m-d H:m:s'));
			$message->setDateMessage($result);
			$message->setIdConversationReceive($conversationR);
			$message->setView(0);//la vue du message pour le user qui reçoit le message
			$message->setIdAnnouncement($announcement);
			$message->setMessage($messageM);
			$message->setIdSender($this->getUser());
			$message->setIdReceiver($idConversationSend->getIdUser());
			$em = $this->getDoctrine()->getManager();
			$em->persist($message);
			$em->flush();



		}else{//si le user est idViewer

			//on cherche si la conversation existe
			// si l'utilisateur existe dans conversationReceive ou conversationSend

			//s'il existe dans l'une ou l'autre on ne fait pas d'insertion dans les tables conversation, on récupère l'id conversation et on insère le message
			//sinon on fait une insertion dans les tables conversations


			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Conversation');
			$conversationS = $repository->findOneBy(array('idUser'=>$this->getUser(),'idAnnouncement'=>$idAnnouncement, 'type' => 'send'));//ici c'est le idViewer qui est trouvé

			if($type == "material"){//si le type de message est matériel
				$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			} else{
				$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			}


			if($conversationS == null){//si la conversation n'a pas été crée

				//création de la conversation envoyeur
				$result = new \DateTime(date('Y-m-d H:m:s'));
				$conversationSend = new Conversation();
				$conversationSend->setIdUser($this->getUser());
				$conversationSend->setIdAnnouncement($announcement);
				$conversationSend->setTitle($messageM);
				$conversationSend->setType('send');
				$conversationSend->setDateMessage($result);
				$conversationSend->setNumberMessages(1);



				//echo 'userAnnouncer= '.$userAnnouncer->getId();
				//création de la conversation receveur
				$conversationReceive = new Conversation();
				$conversationReceive->setDateMessage($result);
				$conversationReceive->setIdUser($userAnnouncer);
				$conversationReceive->setIdAnnouncement($announcement);
				$conversationReceive->setTitle($messageM);
				$conversationReceive->setType('receive');
				$conversationReceive->setNumberMessages(1);


				//enregistrement de la conversation receveur
				$em = $this->getDoctrine()->getManager();
				$em->persist($conversationReceive);
				$em->flush();
				$em->persist($conversationSend);
				$em->flush();

				//il faut ensuite envoyer le message dans la classe messageMaterial ou partner



				if($type == "material"){
					$message = new Message();
					$message->setType("material");
					

				}else{//message d'une annonce partner
					//message des annonces partenaires
					$message = new Message();
					$message->setType("partner");
					//envoi du mesage
				}
				$message->setIdViewer($conversationSend->getIdUser());
				$message->setIdAnnouncer($userAnnouncer);
				$message->setIdConversationSend($conversationSend);
				$result = new \DateTime(date('Y-m-d H:m:s'));
				$message->setDateMessage($result);
				$message->setIdConversationReceive($conversationReceive);
				$message->setView(0);//la vue du message pour le user qui reçoit le message
				$message->setIdAnnouncement($announcement);
				$message->setMessage($messageM);
				$message->setIdSender($this->getUser());
				$message->setIdReceiver($userAnnouncer);
				$em = $this->getDoctrine()->getManager();
				$em->persist($message);
				$em->flush();


			}else{//la conversation existe déjà
				$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Conversation');
				$conversationR = $repository->findOneBy(array('idUser'=>$userAnnouncer,'idAnnouncement'=>$idAnnouncement, 'type' => 'receive'));

				if($type == "material"){
					$message = new Message();
					$message->setType("material");

				}else{//message d'une annonce partner
					//message des annonces partenaires
					$message = new Message();
					$message->setType("partner");
					//envoi du mesage
				}

				$message->setIdViewer($conversationS->getIdUser());
				$message->setIdAnnouncer($userAnnouncer);
				$message->setIdConversationSend($conversationS);
				$result = new \DateTime(date('Y-m-d H:m:s'));
				$message->setDateMessage($result);
				$message->setIdConversationReceive($conversationR);
				$message->setView(0);//la vue du message pour le user qui reçoit le message
				$message->setIdAnnouncement($announcement);
				$message->setMessage($messageM);
				$message->setIdSender($this->getUser());
				$message->setIdReceiver($userAnnouncer);
				$em = $this->getDoctrine()->getManager();
				$em->persist($message);
				$em->flush();

			}



		}


		return $this->redirectToRoute('display_message_material', array('idViewer'=>$this->getUser(),'idAnnouncement'=>$idAnnouncement));

	}

	public function displayMessageAction($idViewer, $idAnnouncement){
		//affichage le message cliqué depuis la page message.html.twig

		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Conversation');
		$conversationSend = $repository->findOneBy(array('idUser'=>$this->getUser(),'idAnnouncement'=>$idAnnouncement, 'type' => 'send'));
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Conversation');
		$conversationReceive = $repository->findOneBy(array('idUser'=>$this->getUser(),'idAnnouncement'=>$idAnnouncement,  'type' => 'receive'));



		if($conversationReceive != null){
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Message');
			$messagePartner = $repository->findByIdConversationReceive($conversationReceive->getId());
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Message');
			$messageMaterial = $repository->findByIdConversationReceive($conversationReceive->getId());

		}else{
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Message');
			$messagePartner = $repository->findByIdConversationSend($conversationSend->getId());
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Message');
			$messageMaterial = $repository->findByIdConversationSend($conversationSend->getId());

		}

		/*for($messageMaterial as $messageMaterial){
			if($messageMaterial->getView() == 0){
				$messageMaterial->setView(1);
			}

		}*/
		$em = $this->getDoctrine()->getManager();
		$messageMaterialView = $repository->findByNotView($this->getUser());
		for ($i=0; $i<count($messageMaterialView); $i++) {
			if ($messageMaterialView[$i]->getView() == 0) {
				$messageMaterialView[$i]->setView(1);
				$em->persist($messageMaterialView[$i]);
				$em->flush();
			}

		}




		/*$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Message');
		$messagePartner = $repository->findBy(array('idViewer'=>$idViewer, 'idAnnouncement'=>$idAnnouncement));


		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Message');

		$messageMaterial = $repository->findBy(array('idViewer'=>$idViewer, 'idAnnouncement'=>$idAnnouncement));*/
		$user =$this->getUser();
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcement = $repository->findOneById($idAnnouncement);
		return $this->render('AppBundle:Default:display-message.html.twig', array( 'user' => $user,'announcement'=>$announcement ,'messageMaterial'=>$messageMaterial, 'messagePartner'=>$messagePartner, 'conversationSend'=>$conversationSend,'conversationReceive'=>$conversationReceive));

	}
	

	public function displayProfileAnnouncerAction($id){
		//affiche le profile de l'annonceur
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:User');
		$user = $repository->findById($id);
		$request=Request::createFromGlobals();
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementMaterial = $repository->findBy(array('idUser'=>$user, 'isValid'=>1, 'type' =>'material'));
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementPartner = $repository->findBy(array('idUser'=>$user, 'isValid'=>1, 'type' =>'partner'));

		$paginator  = $this->get('knp_paginator');
			$pagination = $paginator->paginate(
				$announcementMaterial, /* query NOT result */
				$request->query->getInt('page', 1)/*page number*/,
				8
			);

		return $this->render('AppBundle:Default:display-profile-announcer.html.twig', array( 'announcementPartner'=>$announcementPartner,
		'pagination'=>$pagination,
		'user' => $user));
	}
	
	public function addImageAction($announcement){
		// page qui permet d'ajouter si besoin est, des images


		$request=Request::createFromGlobals();
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		$announcementCreate = $repository->findOneById($announcement);

		$form = $this->createFormBuilder($announcementCreate)
		->add('imageTwice', CroppableImageType::class, array( 'required' => false,
					'uploadConfig' => array(
						'uploadRoute' => 'comur_api_upload',        //optional
						'uploadUrl' => $announcementCreate->getUploadRootDir($this->getUser()),       // required - see explanation below (you can also put just a dir path)
						'webDir' => $announcementCreate->getUploadDir($this->getUser()),              // required - see explanation below (you can also put just a dir path)
						'fileExt' => '*.jpg;*.gif;*.png;*.jpeg',    //optional
						'libraryDir' => null,                       //optional
						'libraryRoute' => 'comur_api_image_library', //optional
						'showLibrary' => true,                      //optional
						'saveOriginal' => 'originalImageTwice'           //optional

					),
					'cropConfig' => array(
						'minWidth' => 200,
						'minHeight' => 200,
						'aspectRatio' => true,              //optional
						'cropRoute' => 'comur_api_crop',    //optional
						'forceResize' => false,             //optional
						'thumbs' => array(                  //optional
							array(
								'maxWidth' => 200,
								'maxHeight' => 200,
								'useAsFieldImage' => true  //optional
							)
						)
					)
				))
				->add('imageThird', CroppableImageType::class, array( 'required' => false,
					'uploadConfig' => array(
						'uploadRoute' => 'comur_api_upload',        //optional
						'uploadUrl' => $announcementCreate->getUploadRootDir($this->getUser()),       // required - see explanation below (you can also put just a dir path)
						'webDir' => $announcementCreate->getUploadDir($this->getUser()),              // required - see explanation below (you can also put just a dir path)
						'fileExt' => '*.jpg;*.gif;*.png;*.jpeg',    //optional
						'libraryDir' => null,                       //optional
						'libraryRoute' => 'comur_api_image_library', //optional
						'showLibrary' => true,                      //optional
						'saveOriginal' => 'originalImageThird'//optional

					),
					'cropConfig' => array(
						'minWidth' => 200,
						'minHeight' => 200,
						'aspectRatio' => true,              //optional
						'cropRoute' => 'comur_api_crop',    //optional
						'forceResize' => false,             //optional
						'thumbs' => array(                  //optional
							array(
								'maxWidth' => 200,
								'maxHeight' => 200,
								'useAsFieldImage' => true  //optional
							)
						)
					)
				))
            ->add('save', SubmitType::class, array('label' => 'Enregistrer les images'))
            ->getForm();

			$form->handleRequest($request);
			$imageTwice = $form["imageTwice"]->getData()->getImageTwice();
			$imageThird = $form["imageThird"]->getData()->getImageThird();

			if( $form->isSubmitted()){
				//$announcementCreate = $form->getData();
				$announcementCreate->setImageTwice($imageTwice);
				$announcementCreate->setImageThird($imageThird);
				$em = $this->getDoctrine()->getManager();
				$em->persist($announcementCreate);
				$em->flush($announcementCreate);

				return $this->redirectToRoute('app_index');

			}
		return $this->render('AppBundle:Creation:addImage.html.twig', array('form' => $form->createView()));

	}

  public function messageAction(){
	//affichage de la page message qui affiche le premier message de la conversation

	/*$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Message');
	//$messageMaterialAnswer = $repository->findByIdAnswer($this->getUser());
	//$messageMaterial = $repository->findByIdAnnouncement($this->getUser());
	$messageMaterial = $repository->findByDistinctAnnouncement($this->getUser());
	//faire un findByIdasker et idAnswer et dans le twig faire un for tant que les IDannouncement sont diffé
	$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Message');
	$messagePartner = $repository->findByIdAnswer($this->getUser());*/
	$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Conversation');
	$conversation = $repository->findByIdUser($this->getUser());
	//$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:ConversationSend');
	//$conversationSend = $repository->findByIdUser($this->getUser());
	$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
	$numberAnnouncementUser = $repository->findByCountAnnouncementUser($this->getUser());

	$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
	$numberFavoriteMaterialUser = $repository->findByCountFavoriteMaterialUser($this->getUser());

	$repository = $this->getDoctrine()->getManager()->getRepository('AnnouncementMaterialBundle:ReservationMaterial');
	$numberReservationMaterialUser = $repository->findByCountReservationMaterialUser($this->getUser());


    return $this->render('AppBundle:Default:message.html.twig', array( 
		'conversation'=>$conversation, 'numberFavoriteMaterialUser'=>$numberFavoriteMaterialUser,
		'numberAnnouncementUser'=>$numberAnnouncementUser,
		'numberReservationMaterialUser'=>$numberReservationMaterialUser));
  }
  
  
  public function ratingAnnouncementAction($id){
		//évaluation de l'annonce après expérience
		$rating = new Evaluation();
		$request=Request::createFromGlobals();
		$form = $this->createFormBuilder($rating)
			->add('rating', HiddenType::Class)
			->add('comment', TextAreaType::Class, [
                    'required' => false
					])
			->add('save', SubmitType::class, array('label' => 'Validez'))
			->getForm();
		
		$request=Request::createFromGlobals();
		$form->handleRequest($request);

		//si le formulaire est soumis et valide
		if ($form->isSubmitted() && $form->isValid()) {
			$ratingData = $form["rating"]->getData();
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			$announcement = $repository->findOneById($id);
			$rating->setIdUser($this->getUser());
			$rating->setComment($form["comment"]->getData());
			$rating->setRating($form["rating"]->getData());
			$rating->setType($announcement->getType());
			$rating->setIdAnnouncement($announcement);
			$em = $this->getDoctrine()->getManager();
			
			$em->persist($rating);
            $em->flush();
			//redirection vers la page d'accueil
            return $this->redirectToRoute('app_index');
			
		}
		

		
		return $this->render('AppBundle:Default:rating-announcement.html.twig', array('form'=>$form->createView()));
  }
  

 /* public function paymentAction(){

  }*/
  public function pageTest2Action(){
		$sport ='Accrobranche';
		$place = 'Paris, France';
		$dateBegin ='2017-05-28';
		$announcement = new Announcement();
		$request=Request::createFromGlobals();
		$form = $this->createFormBuilder($announcement)
		->add('place', TextType::Class)
		->add('sport', EntityType::class,
			['class'=>'AppBundle:Sport',
			 'choice_label'=>'title',])
		->add('dateBegin', DateTimeType::class, [
				'widget' => 'single_text'/*,
				'attr' => [
					'data-date-format' => 'YYYY-MM-DD HH:mm:SS',
					'id' => 'dateBegin'
				]*/
			]
		)
		->add('save', SubmitType::class, array('label' => 'GO'))
		->getForm();



		$announcementFilter = new Announcement();

		$formFilter = $this->createFormBuilder($announcementFilter)
		/*->add('location', EntityType::class,
			['class'=>'ConnectBundle:Location',
			 'choice_label'=>'title',])*/

		->add('dateEnd', DateTimeType::class, [
                    'widget' => 'single_text',

                ]
            )



			->add('quality', TextType::class,[ 'attr' => [
						'id' => 'quality'

						]
			])
			->add('cleanliness', TextType::class)

			->add('price', MoneyType::class)
		->add('save', SubmitType::class, array('label' => 'Filtre'))
		->getForm();
		$formFilter->handleRequest($request);

		//si le formulaire est soumis et valide
		if ($formFilter->isSubmitted() && $formFilter->isValid()) {

			$dateEnd = $formFilter["dateEnd"]->getData();
			$resultEnd = $dateEnd->format('Y-m-d');
			$minPrice = $formFilter["price"]->getData();
			$cleanliness = $formFilter["cleanliness"]->getData();
			$quality = $formFilter["quality"]->getData();
			$maxPrice = 250;


			echo 'Place: '.$place;




			//return $this->redirectToRoute('app_announcement_material',$place,$sport);
			//$this->redirectToRoute('app_announcement_material', array('place' => $place, 'sport' => $sport));
			//$this->redirectToRoute('app_announcement_material', ['place' => $place], ['sport' => $sport]);

			//on envoie les données dans la page d'affichage des annonces
			/*return $this->render('AppBundle:Default:announcementMaterial.html.twig', array('place' =>$place, 'sport' => $sport->getTitle(),'dateBegin' =>$resultEnd,
			'dateEnd' =>$resultEnd, 'priceMin'=>$priceMin, 'priceMax'=>$priceMax, 'cleanliness'=>$cleanliness, 'quality'=>$quality));*/


			//récupération de l'ensemble des données des annonces materiel et partenaire selon la date le sport et le lieu
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			if($this->getUser() == null){
				$announcementMaterial = $repository->findByFilterMat($place, $sport, $dateBegin, $minPrice, $maxPrice, $dateEnd, $cleanliness, $quality, $order);
				//$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
				//$announcementPartner = $repository->findByDateBegUnConnected($place,$sport,$dateBegin);

			}else{
				$announcementMaterial = $repository->findByFilterMat($place, $sport, $dateBegin, $minPrice, $maxPrice, $dateEnd, $cleanliness, $quality);
				//$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
				//$announcementPartner = $repository->findByDateBeg($place,$sport,$dateBegin,$this->getuser()->getId());
			}



			$paginator  = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$announcementMaterial,
					$request->query->getInt('page', 1),
					8
				);


			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
			$favoriteMaterial = $repository->findByIdUser($this->getUser());




			//envoie des données dans la page d'affichage des annonces
			return $this->render('AppBundle:Default:announcement-material.html.twig', array('form'=>$form->createView(),'formFilter'=>$formFilter->createView(),'favoriteMaterial' =>$favoriteMaterial,'pagination'=>$pagination, 'announcementPartner' =>$announcementPartner, 'place' => $place, 'sport' => $sport)
			);

		}






		//si le formulaire est soumis et valide
		if ($form->isSubmitted() && $form->isValid()) {
			$dateBegin = $form["dateBegin"]->getData();
			$result = $dateBegin->format('Y-m-d');



			//echo 'Place: '.$place;

			//return $this->redirectToRoute('app_announcement_material',$place,$sport);
			//$this->redirectToRoute('app_announcement_material', array('place' => $place, 'sport' => $sport));
			//$this->redirectToRoute('app_announcement_material', ['place' => $place], ['sport' => $sport]);

			//on envoie les données dans la page d'affichage des annonces

			//récupération de l'ensemble des données des annonces materiel et partenaire selon la date le sport et le lieu
			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			if($this->getUser() == null){
				$announcementMaterial = $repository->findByDateBegUnConnectedMat($place,$sport,$dateBegin);
				$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
				$announcementPartner = $repository->findByDateBegUnConnectedPart($place,$sport,$dateBegin);

			}else{
				$announcementMaterial = $repository->findByDateBegMat($place,$sport,$dateBegin,$this->getUser()->getId());
				$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
				$announcementPartner = $repository->findByDateBegPart($place,$sport,$dateBegin,$this->getuser()->getId());
			}



			$paginator  = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
					$announcementMaterial,
					$request->query->getInt('page', 1),
					8
				);



			$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
			$favoriteMaterial = $repository->findByIdUser($this->getUser());




			//envoie des données dans la page d'affichage des annonces
			return $this->render('AnnouncementMaterialBundle:Default:announcement-material.html.twig', array('form'=>$form->createView(),'formFilter'=>$formFilter->createView(), 'favoriteMaterial' =>$favoriteMaterial,'pagination'=>$pagination, 'announcementPartner' =>$announcementPartner, 'place' => $place, 'sport' => $sport)
			);
		}




		//récupération de l'ensemble des données des annonces materiel et partenaire selon la date le sport et le lieu
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
		if($this->getUser() == null){
			$announcementMaterial = $repository->findByDateBegUnConnectedMat($place,$sport,$dateBegin);
			/*$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			$announcementPartner = $repository->findByDateBegUnConnected($place,$sport,$dateBegin,$time);*/

		}else{
			$announcementMaterial = $repository->findByDateBegMat($place,$sport,$dateBegin,$this->getUser()->getId());
			/*$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Announcement');
			$announcementPartner = $repository->findByDateBeg($place,$sport,$dateBegin,$this->getuser()->getId(),$time);*/
		}



		$paginator  = $this->get('knp_paginator');
			$pagination = $paginator->paginate(
				$announcementMaterial,
				$request->query->getInt('page', 1),
				8
			);



		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Favorite');
		$favoriteMaterial = $repository->findByIdUser($this->getUser());



	

		//envoie des données dans la page d'affichage des annonces
		return $this->render('AppBundle:Default:pageTest2.html.twig',  array('form'=>$form->createView(),'formFilter'=>$formFilter->createView(), 'favoriteMaterial' =>$favoriteMaterial,'pagination'=>$pagination, /*'announcementPartner' =>$announcementPartner, */'place' => $place, 'sport' => $sport)
        );

	}
	public function pageTestAction(){
		//page de test



		//creation d'une annonce materiel

		$announcement = new Announcement();
		$request=Request::createFromGlobals();

		/*if(is_dir(''.$this->getUser()->getEmail())== false){//vérification si dossier existe déjà
			//creation du dossier de l'annonce materiel avec le nom de l'email
			mkdir('./AnnouncementMaterial/'.$this->getUser()->getEmail());
			//voir plus bas sur le rename pour modifier la route

		}*/

		//creation du formulaire de creation de materiel
        $form = $this->createFormBuilder($announcement)
			->add('place', TextType::class, [ 'attr' => [
			'id' => 'locality'] ])
			->add('sport', EntityType::class,
                ['class'=>'AppBundle:Sport',
                 'choice_label'=>'title',])

            ->add('dateBegin', DateTimeType::class, [
                    'widget' => 'single_text'/*,
					'attr' => [
						'data-date-format' => 'YYYY-MM-DD HH:mm:SS',
						'id' => 'dateBegin'
					]*/
                ]
            )
			->add('dateEnd', DateTimeType::class, [
                    'widget' => 'single_text',

                ]
            )
			->add('title', TextType::class)
			->add('state', TextType::class, [ 'attr' => [
						'id' => 'country',
						]
					])
			->add('region', TextType::class,[ 'attr' => [
						'id' => 'administrative_area_level_1',
						'required' => false
						]
			])
			->add('description',TextareaType::class, [
                    'required' => false
					]
				)
			->add('quality', HiddenType::class,[ 'attr' => [
						'id' => 'quality'

						]
			])
			->add('cleanliness', HiddenType::class)

			->add('price', MoneyType::class)
			->add('imageFirst', CroppableImageType::class, array(
					'uploadConfig' => array(
						'uploadRoute' => 'comur_api_upload',        //optional
						'uploadUrl' => $announcement->getUploadRootDir($this->getUser()),       // required - see explanation below (you can also put just a dir path)
						'webDir' => $announcement->getUploadDir($this->getUser()),              // required - see explanation below (you can also put just a dir path)
						'fileExt' => '*.jpg;*.gif;*.png;*.jpeg',    //optional
						'libraryDir' => null,                       //optional
						'libraryRoute' => 'comur_api_image_library', //optional
						'showLibrary' => true,                      //optional
						'saveOriginal' => 'originalImageFirst'           //optional
					),
					'cropConfig' => array(
						'minWidth' => 200,
						'minHeight' => 200,
						'aspectRatio' => true,              //optional
						'cropRoute' => 'comur_api_crop',    //optional
						'forceResize' => false,             //optional
						'thumbs' => array(                  //optional
							array(
								'maxWidth' => 200,
								'maxHeight' => 200,
								'useAsFieldImage' => true  //optional
							)
						)
					)
				))

            ->add('save', SubmitType::class, array('label' => 'Poster une annonce'))
            ->getForm();

        $form->handleRequest($request);
		$sport = $form["sport"]->getData();
		$em1 = $this->getDoctrine()->getManager();
		$sport1 = $em1->getRepository('AppBundle:Sport')->findOneByTitle($sport);


		//si le formulaire est soumis et valide
        if ($form->isSubmitted()&& $form->isValid()) {

			//si l'utilisateur n'est pas connecté
			if($this->getUser() == null){
				//on envoie un message flash pour modifier le header selon la connexion de user
				$this->addFlash(
				'notice',
				'Your changes were saved!'
				);
				return $this->redirectToRoute('create_material_announcement');

			}

			//on entre les données de l'annonce créée
			$announcement->setIdUser($this->getUser());
			//$announcement->setType('mat'.$announcement->getId());
			//echo "<script type='text/javascript'>alert('$announcement->getPostcode()');</script>";
            //$announcement = $form->getData();
			$title =$form["title"]->getData();
			$dateEnd=$form["dateEnd"]->getData();
            $dateBegin=$form["dateBegin"]->getData();
			$announcement->setDateBegin($dateBegin);
            $announcement->setDateEnd($dateEnd);
			$announcement->setType('material');
			//$announcement->setDateAnnouncement(date('Y-m-d H:i:s'));
			$announcement->setSport($sport1);
			$sport = $form["sport"]->getData();

			//on renomme le dossier qui était un dossier temporaire, avec le id de l'annonce pour être unique
			rename("./AnnouncementMaterial/".$this->getUser()->getEmail(),"./AnnouncementMaterial/".$announcement->getId());


            $em = $this->getDoctrine()->getManager();


			//enregistrement de l'annonce
            $em->persist($announcement);
            $em->flush();
			//redirection vers la page d'accueil
            return $this->redirectToRoute('app_index');
        }

		return $this->render('AppBundle:Default:page-test.html.twig', array('form' => $form->createView()));
	}


	public function setLocaleAction($language = null)
	{
		if($language != null)
		{
			// On enregistre la langue en session
			$this->get('session')->set('_locale', $language);
		}

		// on tente de rediriger vers la page d'origine
		$url = $this->container->get('request')->headers->get('referer');
		if(empty($url))
		{
			$url = $this->container->get('router')->generate('index');
		}

		return new RedirectResponse($url);
	}

	public function aboutUsAction(){

		return $this->render('AppBundle:Footer:about-us.html.twig');
	}

  public function blogAction(){

    return $this->render('AppBundle:Footer:blog.html.twig');
  }

  public function mobileAction(){

    return $this->render('AppBundle:Footer:mobile.html.twig');
  }

  public function projectAction(){

    return $this->render('AppBundle:Footer:project.html.twig');
  }

  public function helpAction(){

    return $this->render('AppBundle:Footer:help.html.twig');
  }

  public function presseAction(){

    return $this->render('AppBundle:Footer:presse.html.twig');
  }

  public function businessAction(){

    return $this->render('AppBundle:Footer:business.html.twig');
  }

  public function contactAction(){
	  
	  
	$contact = new Contact();
	$request=Request::createFromGlobals();


	$form = $this->createFormBuilder($contact)
		->add('title', TextType::class)
		->add('message', TextAreaType::class)
		->add('save', SubmitType::class, array('label' => 'Poser une question'))
		->getForm();

	$form->handleRequest($request);
	
	//si le formulaire est soumis et valide
	if ($form->isSubmitted()&& $form->isValid()) {


		$contact->setTitle($form['title']->getData());
		$contact->setMessage($form['message']->getData());
		$contact->setIdUser($this->getUser());
		$contact->setDateContact(new DateTime(date('Y-m-d H:i:s')));
		
		$em = $this->getDoctrine()->getManager();


		//enregistrement de l'annonce
		$em->persist($contact);
		$em->flush();
		//redirection vers la page d'accueil
		return $this->redirectToRoute('app_index');
	}
	
	
	
    return $this->render('AppBundle:Footer:contact.html.twig', array('form' => $form->createView()));
  }


}
