<?php

namespace AppBundle\Controller;


use AppBundle\Entity\DeviceToken;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\PropertyAccess;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Finder\Finder;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseNullableUserEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use AppBundle\Entity\Announcement;
use AppBundle\Entity\AnnouncementMaterial;
use AppBundle\Entity\AnnouncementPartner;

use UserBundle\Entity\UserFollow;
use UserBundle\Entity\UserMetadata;
use UserBundle\Entity\UserInterest;

use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\Config\FileLocator;



class RestController extends Controller
{

	public function load(array $configs, ContainerBuilder $container){
		$loader = new XmlFileLoader(
			$container,
			new FileLocator(__DIR__.'/../Resources/config')
		);
		$loader->load('services.xml');
	}

	/**
	* @Rest\View()
	* @Rest\Post("/getAuthentification/")
	*/

	public function getAuthentification(Request $request)
	{
		//Parameters
		$password       = $this->getRequest->request->get('password');
		$email          = $request->request->get('email');
		$deviceToken    = $request->request->get('deviceToken');
		$os             = $request->request->get('os');

		//Parameters treatments
		$em = $this->getDoctrine()->getManager();
		$repository = $em->getRepository('AppBundle:User');
		$user       = $repository->findOneByEmail($email);

		if(!$user) {
			$response = array('success' => 0, 'error'  => 'email_false');
		}
		else{
			if(!$this->get('security.password_encoder')->isPasswordValid($user, $password)) {
				$response = array( 'success' => 0, 'error'  => 'password_false' );
			}
			else {


				//Add device token for user
				if(!empty($deviceToken) ){
					$deviceTokenObject = new DeviceToken();
					$deviceTokenObject->setUser($user);
					$deviceTokenObject->setOs($os);
					$deviceTokenObject->setDeviceToken($deviceToken);

					$em->persist($deviceTokenObject);
					$em->flush();
				}

			}

			$response = array( 'success' => 1, 'result' => array('id' => $user->getId(), 'display_name' => $user->getUsername(), 'user_email' => $user->getEmail(), 'user_login' => $user->getUsername() ) );

		}

		// CrÈation d'une vue FOSRestBundle
		$view = View::create($response);
		$view->setFormat('json');

		return $view;
	}

	/**
     * @Rest\View()
     * @Rest\Post("/getAccountInfos/")
     */
    public function getAccountInfos(Request $request)
    {
        //Parameters
        $userId = $request->request->get('user_id');

        //Parameters treatments
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:User');
        $user = $repository->findOneById($userId);

        $response = array( 'success' => 1, 'result' => array( 'ID' => $user->getId(), 'username' => $user->getUsername(), 'firstname' => $user->getFirstname(), 'name' => $user->getName(),  'gender' => $user->getGender(), 'city' => $user->getCity()) );

        $view = View::create($response);
        $view->setFormat('json');

        return $view;
    }

	/**
	* @Rest\View()
	* @Rest\Post("/getIndexAction/")
	*/
	public function getIndexAction(Request $request)
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:AnnouncementMaterial');
		$numberAnnouncementParis = $repository->findByCountAnnouncement("Paris, France");
		$numberAnnouncementNantes = $repository->findByCountAnnouncement("Nantes, France");
		$numberAnnouncementLyon = $repository->findByCountAnnouncement("Lyon, France");

		$result[] = array('numberAnnouncementParis' =>$numberAnnouncementParis,
		'numberAnnouncementNantes' =>$numberAnnouncementNantes,
		'numberAnnouncementLyon' =>$numberAnnouncementLyon);
		$response = array( 'success' => 1, 'result'  => $result);

		// Création d'une vue FOSRestBundle
		$view = View::create($response);
		$view->setFormat('json');
		return $view;

	}

	/**
	* @Rest\View()
	* @Rest\Post("/getPlaceAnnouncementsMaterials/")
	*/
	public function getPlaceAnnouncementsMaterials(Request $request)
	{

		//Parameters
		$placeChoosen = $request->request->get('place');

		//Parameters treatments
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:AnnouncementMaterial');
		$announcements = $repository->findByPlace($placeChoosen);
		$result = array();

		if ( !empty($announcements)) {
			foreach ($announcements as $announcement){
				$result[] = array('announcement_id' => $announcement->getId(), 'announcement_title' => $announcement->getTitle(),  'announcement_idUser' => $announcement->getId(), 'announcement_description' => $announcement->getDescription(), 'announcement_price' => $announcement->getPrice(), 'announcement_date' => $announcement->getDateBegin(), 'dateEnd' => $announcement->getDateEnd(), 'announcement_city' => $announcement->getPlace() );
				$response = array( 'success' => 1, 'result'  => $result);
			}
		}

		else {
			$response = array( 'error' => 0, 'result' => "error");
		}

		// Création d'une vue FOSRestBundle
		$view = View::create($response);
		$view->setFormat('json');
		return $view;
	}

	/**
	* @Rest\View()
	* @Rest\Post("/getAnnouncementsPartners/")
	*/
	public function getAnnouncementsPartners(Request $request)
	{
		//Parameters treatments
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:AnnouncementPartner');
		$announcements = $repository->findAll();
		$result = array();

		if ($announcements > 1){

			foreach ($announcements as $announcement){
				$result[] = array('announcement_id' => $announcement->getId(), 'announcement_title' => $announcement->getTitle(), 'announcement_description' => $announcement->getDescription(), 'announcement_price' => $announcement->getPrice(), 'announcement_date' => $announcement->getDateBegin(), 'dateEnd' => $announcement->getDateEnd(), 'announcement_city' => $announcement->getPlace() );
				$response = array( 'success' => 1, 'result'  => $result);
			}

		}
		else {
			$response = array( 'error' => 0, 'result' => "error");
		}

		// Création d'une vue FOSRestBundle
		$view = View::create($response);
		$view->setFormat('json');
		return $view;
	}

	/**
	* @Rest\View()
	* @Rest\Post("/getDisplayAnnouncementsUser/")
	*/
	public function getDisplayAnnouncementsUser(Request $request)
	{

		//Parameters
		$currentUserId = $request->request->get('user_id');


		//Parameters treatments
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:AnnouncementMaterial');
		$announcements = $repository->findByIdUser($currentUserId);
		$result = array();

		if ( !empty($announcements)) {
			foreach ($announcements as $announcement){
				$result[] = array('announcement_id' => $announcement->getId(), 'announcement_title' => $announcement->getTitle(), 'announcement_description' => $announcement->getDescription(), 'announcement_price' => $announcement->getPrice(), 'announcement_date' => $announcement->getDateBegin(), 'dateEnd' => $announcement->getDateEnd(), 'announcement_city' => $announcement->getPlace() );
				$response = array( 'success' => 1, 'result'  => $result);
			}
		}
		else {
			$response = array( 'error' => 0, 'result' => "error");
		}

		// Création d'une vue FOSRestBundle
		$view = View::create($response);
		$view->setFormat('json');
		return $view;
	}

	/**
	* @Rest\View()
	* @Rest\Post("/getDisplayAnnouncementsFavoris/")
	*/
	public function getDisplayAnnouncementsFavoris(Request $request)
	{

		//Parameters
		$currentUserId = $request->request->get('user_id');


		//Parameters treatments
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:FavoriteMaterial');
		$favoriteMaterial = $repository->findByIdUser($currentUserId);
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:FavoritePartner');
		$favoritePartner = $repository->findByIdUser($currentUserId);

		$response = array( 'success' => 1, 'result' => array( 'favoriteMaterial' => $favoriteMaterial,
		'favoritePartner'=>$favoritePartner) );

		$result = array();

		// Création d'une vue FOSRestBundle
		$view = View::create($response);
		$view->setFormat('json');
		return $view;
	}

	/**
	* @Rest\View()
	* @Rest\Post("/getDetailsAnnouncement/")
	*/
	public function getDetailsAnnouncement(Request $request)
	{
		//Parameters
		$announcementId = $request->query->get('91');
		//Parameters treatments
		$em = $this->getDoctrine()->getManager();
		$repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:AnnouncementMaterial');
		$announcement = $repository->findById(91);

		$result = array( 'success' => 1, 'result' => array('announcement_sport' => $announcement->getId(), 'announcement_price' => $announcement->getPrice()));

		// Création d'une vue FOSRestBundle
		$view = View::create($result);
		$view->setFormat('json');
		return $view;
	}




	//Send

	/**
	* @Rest\View
	* @Rest\Post("/sendAnnouncementMaterial/")
	*/
	public function sendAnnouncementMaterial(Request $request)
	{
		$announcement = new Announcement();

		$announcement-> setTitle($request->get('title'))
		->setDescription($request->get('description'));

		$em = $this->get('doctrine.orm.entity_manager');
		$em->persist($announcement);
		$em->flush();

		return $announcement;

	}

}
