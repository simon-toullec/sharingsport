<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Favorite
 *
 * @ORM\Table(name="favorite")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FavoriteRepository")
 */
class Favorite
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
 
    /**
     * @ORM\ManyToOne(targetEntity="Announcement")
     * @ORM\JoinColumn(name="idAnnouncement", referencedColumnName="id")
     */
    private $idAnnouncement;

    /**
     * @var int
     **@ORM\ManyToOne(targetEntity="User", cascade={"persist"})
     * @ORM\Column(name="idUser", type="bigint")
     */
    private $idUser;
	
	/**
     * @var int
     *
     * @ORM\Column(name="idVal", type="integer", unique=true)
     */
	private $idVal;
	
	/**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=true, length=255)
     */
    private $type;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAnnouncement
     *
     * @param integer $idAnnouncement
     *
     * @return Favorite
     */
    public function setIdAnnouncement($idAnnouncement)
    {
        $this->idAnnouncement = $idAnnouncement;

        return $this;
    }

    /**
     * Get idAnnouncement
     *
     * @return int
     */
    public function getIdAnnouncement()
    {
        return $this->idAnnouncement;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return Favorite
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
	
	
	/**
     * Get idVal
     *
     * @return int
     */
    public function getIdVal()
    {
        return $this->idVal;
    }
	
	/**
     * Set idVal
     *
     * @param integer $idVal
     *
     * @return Favorite
     */
    public function setIdVal($idVal)
    {
        $this->idVal = $idVal;

        return $this;
    }
	
	/**
     * Set type
     *
     * @param string $type
     *
     * @return Conversation
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

   
}
