<?php

namespace AppBundle\Entity;
use AnnouncementMaterialBundle\Entity\AnnouncementMaterial;
use Doctrine\ORM\Mapping as ORM;
//use AppBundle\Entity\User;
/**
 * Conversation
 *
 * @ORM\Table(name="conversation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ConversationRepository")
 */
class Conversation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    private $idUser;
	
	/**
     * @ORM\ManyToOne(targetEntity="Announcement")
     * @ORM\JoinColumn(name="idAnnouncement", referencedColumnName="id")
     */
    private $idAnnouncement;
	
	/**
     * @var \Date
     *
     * @ORM\Column(name="dateMessage", type="datetime", nullable=true)
     */
    private $dateMessage;

	/**
     * @var int
     *
     * @ORM\Column(name="numberMessages", type="integer", nullable=true)
     */
    private $numberMessages;
	
	/**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=true, length=255)
     */
    private $type;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Conversation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set idUser
     *
     * @param string $idUser
     *
     * @return Conversation
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return string
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idAnnouncement
     *
     * @param integer $idAnnouncement
     *
     * @return Conversation
     */
    public function setIdAnnouncement($idAnnouncement)
    {
        $this->idAnnouncement = $idAnnouncement;

        return $this;
    }

    /**
     * Get idAnnouncement
     *
     * @return integer
     */
    public function getIdAnnouncement()
    {
        return $this->idAnnouncement;
    }
	
	/**
     * Set dateMessage
     *
     * @param \DateTime $dateMessage
     *
     * @return Conversation
     */
    public function setDateMessage($dateMessage)
    {
        $this->dateMessage = $dateMessage;

        return $this;
    }

    /**
     * Get dateMessage
     *
     * @return \DateTime
     */
    public function getDateMessage()
    {
        return $this->dateMessage;
    }

    /**
     * Set numberMessages
     *
     * @param integer $numberMessages
     *
     * @return Conversation
     */
    public function setNumberMessages($numberMessages)
    {
        $this->numberMessages = $numberMessages;

        return $this;
    }

    /**
     * Get numberMessages
     *
     * @return integer
     */
    public function getNumberMessages()
    {
        return $this->numberMessages;
    }
	
	/**
     * Set type
     *
     * @param string $type
     *
     * @return Conversation
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
