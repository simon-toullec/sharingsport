<?php

namespace AppBundle\Entity;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 *
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImageRepository")
 */
class Image
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    protected $file;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    
    public function setFile(File $file)
    {
        $this->file = $file;

        return $this;
    }

    
    public function getFile()
    {
        return $this->file;
    }
	
	public function upload(File $file)
	{
		//generate unique filename
		$fileName = md5(uniqid()).'.'.$file->guessExtension();

		//Set other entity attribute here

		//move the file
		$file->move($targetDirectory, $fileName);

		return $fileName;
	}
}

