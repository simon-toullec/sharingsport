<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Sport;
use AppBundle\Entity\User;

/**
 * Announcement
 *
 * @ORM\Table(name="announcement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnnouncementRepository")
 */
class Announcement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAnnouncement", type="datetimetz", nullable=true)
     */
    private $dateAnnouncement;

	/**
     * @var integer
     *
     * @ORM\Column(name="numberDays", type="integer", nullable=true)
     */
    private $numberDays;
	
	/**
     * @var \Date
     *
     * @ORM\Column(name="dateBegin", type="date", nullable=true)
     */
    private $dateBegin;
	
	/**
     * @var \Date
     *
     * @ORM\Column(name="dateEnd", type="date", nullable=true)
     */
    private $dateEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="goods", type="string", length=255, nullable=true)
     */
    private $goods;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="views", type="integer", nullable=true)
     */
    private $views;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="quality", type="integer", nullable=true)
     */
    private $quality;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $imageFirst;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $originalImageFirst;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $imageTwice;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $originalImageTwice;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $imageThird;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $originalImageThird;
	
	

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=255, unique=false)
     */
    private $place;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="Sport")
     * @ORM\JoinColumn(name="sport_id", referencedColumnName="id")
     */
    private $sport;

 

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;
	
	/**
     * @var boolean
     *
     * @ORM\Column(name="cleanliness", type="boolean", nullable=true)
     */
    private $cleanliness;
	
	/**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255)
     */
    private $state;
	
	/**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255)
     */
    private $region;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="postcode", type="integer", nullable=true)
     */
    private $postcode;
	
	
	

	/**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;
	
	/**
     * @var boolean
     *
     * @ORM\Column(name="isValid", type="boolean", nullable=true)
     */
    private $isValid;
	
	
	/**
     * @var boolean
     *
     * @ORM\Column(name="isValidAdmin", type="boolean", nullable=true)
     */
    private $isValidAdmin;
	
	/**
     * @var boolean
     *
     * @ORM\Column(name="isValidAnnouncer", type="boolean", nullable=true)
     */
    private $isValidAnnouncer;
	
	
	
	 /**
     * @ORM\ManyToOne(targetEntity="AnnouncementPartnerBundle\Entity\Level")
     * @ORM\JoinColumn(name="level", referencedColumnName="id")
     */
    private $level;
	
	/**
     * @var time
     *
     * @ORM\Column(name="time", type="time", nullable=true)
     */
    private $time;
	
	/**
     * @var int
     *
     * @ORM\Column(name="numberPlaces", type="integer", nullable=true)
     */
    private $numberPlaces;
	
	/**
     * @var int
     *
     * @ORM\Column(name="remainingPlaces", type="integer", nullable=true)
     */
    private $remainingPlaces;
	
	

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
     * Set id
     *
     * @param integer $id
     *
     * @return Announcement
     */
    public function setId($id)
    {
        $this->price = $id;

        return $this;
    }
	
	/**
     * Set originalImageFirst
     *
     * @param string $originalImageFirst
     *
     * @return Announcement
     */
    public function setOriginalImageFirst($originalImageFirst)
    {
        $this->originalImageFirst = $originalImageFirst;

        return $this;
    }

    /**
     * Get originalImageFirst
     *
     * @return string
     */
    public function getOriginalImageFirst()
    {
        return $this->originalImageFirst;
    }
	
	/**
     * Set imageFirst
     *
     * @param string $imageFirst
     *
     * @return Announcement
     */
    public function setImageFirst($imageFirst)
    {
        $this->imageFirst = $imageFirst;

        return $this;
    }

    /**
     * Get imageFirst
     *
     * @return string
     */
    public function getImageFirst()
    {
		return $this->imageFirst;
    }
	
	public function getUploadRootDir($user)
	{
		// absolute path to your directory where images must be saved
		
		return __DIR__.'./../../../web/'.$this->getUploadDir($user);
	
	}

	public function getUploadDir($user)
	{
		/*if(is_dir('./AnnouncementMaterial/'.$user->getEmail().'/'.$this->getId())){
			return '/AnnouncementMaterial/'.$user->getEmail().'/'.$this->getId();
		}else{
			
			return '/AnnouncementMaterial/'.$user->getEmail().'/'.$this->getId();
		}*/
		
		//return './web/Symfony/web/AnnouncementMaterial/'.$user->getEmail().'/'.$this->getId();
		return 'AnnouncementMaterial/'.$user->getEmail().'/'.$this->getId();
		//return 'uploads/myentity';
	}

	public function getAbsolutePath($user)
	{
		return null === $this->image ? null : $this->getUploadRootDir($user).'/'.$this->imageFirst;
	}

	public function getWebPath($user)
	{
		return null === $this->image ? null : '/'.$this->getUploadDir($user).'/'.$this->imageFirst;
	}
	
	
   
    /**
     * Set description
     *
     * @param string $description
     *
     * @return Announcement
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set goods
     *
     * @param string $goods
     *
     * @return Announcement
     */
    public function setGoods($goods)
    {
        $this->goods = $goods;

        return $this;
    }

    /**
     * Get goods
     *
     * @return string
     */
    public function getGoods()
    {
        return $this->goods;
    }
	
	

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return Announcement
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return Announcement
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Announcement
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set sport
     *
     * @param integer $sport
     *
     * @return Announcement
     */
    public function setSport($sport)
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * Get sport
     *
     * @return string
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Announcement
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Announcement
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set dateBegin
     *
     * @param \DateTime $dateBegin
     *
     * @return Announcement
     */
    public function setDateBegin($dateBegin)
    {
        $this->dateBegin = $dateBegin;

        return $this;
    }

    /**
     * Get dateBegin
     *
     * @return \DateTime
     */
    public function getDateBegin()
    {
        return $this->dateBegin;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Announcement
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set quality
     *
     * @param integer $quality
     *
     * @return Announcement
     */
    public function setQuality($quality)
    {
        $this->quality = $quality;

        return $this;
    }

    /**
     * Get quality
     *
     * @return integer
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * Set cleanliness
     *
     * @param boolean $cleanliness
     *
     * @return Announcement
     */
    public function setCleanliness($cleanliness)
    {
        $this->cleanliness = $cleanliness;

        return $this;
    }

    /**
     * Get cleanliness
     *
     * @return boolean
     */
    public function getCleanliness()
    {
        return $this->cleanliness;
    }
	
	/**
	 * toString
	 * @return string
	 */
	public function toString() 
	{
		return $this->getName();
	}

    /**
     * Set region
     *
     * @param string $region
     *
     * @return Announcement
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set postcode
     *
     * @param integer $postcode
     *
     * @return Announcement
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return integer
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

	
	/*public function __toString()
	{
		return (string) $this->getSport();
	}*/

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Announcement
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set imageTwice
     *
     * @param string $imageTwice
     *
     * @return Announcement
     */
    public function setImageTwice($imageTwice)
    {
        $this->imageTwice = $imageTwice;

        return $this;
    }

    /**
     * Get imageTwice
     *
     * @return string
     */
    public function getImageTwice()
    {
        return $this->imageTwice;
    }

    /**
     * Set originalImageTwice
     *
     * @param string $originalImageTwice
     *
     * @return Announcement
     */
    public function setOriginalImageTwice($originalImageTwice)
    {
        $this->originalImageTwice = $originalImageTwice;

        return $this;
    }

    /**
     * Get originalImageTwice
     *
     * @return string
     */
    public function getOriginalImageTwice()
    {
        return $this->originalImageTwice;
    }

    /**
     * Set imageThird
     *
     * @param string $imageThird
     *
     * @return Announcement
     */
    public function setImageThird($imageThird)
    {
        $this->imageThird = $imageThird;

        return $this;
    }

    /**
     * Get imageThird
     *
     * @return string
     */
    public function getImageThird()
    {
        return $this->imageThird;
    }

    /**
     * Set originalImageThird
     *
     * @param string $originalImageThird
     *
     * @return Announcement
     */
    public function setOriginalImageThird($originalImageThird)
    {
        $this->originalImageThird = $originalImageThird;

        return $this;
    }

    /**
     * Get originalImageThird
     *
     * @return string
     */
    public function getOriginalImageThird()
    {
        return $this->originalImageThird;
    }

    /**
     * Set views
     *
     * @param integer $views
     *
     * @return Announcement
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set dateAnnouncement
     *
     * @param \DateTime $dateAnnouncement
     *
     * @return Announcement
     */
    public function setDateAnnouncement($dateAnnouncement)
    {
        $this->dateAnnouncement = $dateAnnouncement;

        return $this;
    }

    /**
     * Get dateAnnouncement
     *
     * @return \DateTime
     */
    public function getDateAnnouncement()
    {
        return $this->dateAnnouncement;
    }

    /**
     * Set numberDays
     *
     * @param integer $numberDays
     *
     * @return Announcement
     */
    public function setNumberDays($numberDays)
    {
        $this->numberDays = $numberDays;

        return $this;
    }

    /**
     * Get numberDays
     *
     * @return integer
     */
    public function getNumberDays()
    {
        return $this->numberDays;
    }

    /**
     * Set isValid
     *
     * @param boolean $isValid
     *
     * @return Announcement
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;

        return $this;
    }

    /**
     * Get isValid
     *
     * @return boolean
     */
    public function getIsValid()
    {
        return $this->isValid;
    }

    /**
     * Set isValidAdmin
     *
     * @param boolean $isValidAdmin
     *
     * @return Announcement
     */
    public function setIsValidAdmin($isValidAdmin)
    {
        $this->isValidAdmin = $isValidAdmin;

        return $this;
    }

    /**
     * Get isValidAdmin
     *
     * @return boolean
     */
    public function getIsValidAdmin()
    {
        return $this->isValidAdmin;
    }

    /**
     * Set isValidAnnouncer
     *
     * @param boolean $isValidAnnouncer
     *
     * @return Announcement
     */
    public function setIsValidAnnouncer($isValidAnnouncer)
    {
        $this->isValidAnnouncer = $isValidAnnouncer;

        return $this;
    }

    /**
     * Get isValidAnnouncer
     *
     * @return boolean
     */
    public function getIsValidAnnouncer()
    {
        return $this->isValidAnnouncer;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return Announcement
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set numberPlaces
     *
     * @param integer $numberPlaces
     *
     * @return Announcement
     */
    public function setNumberPlaces($numberPlaces)
    {
        $this->numberPlaces = $numberPlaces;

        return $this;
    }

    /**
     * Get numberPlaces
     *
     * @return integer
     */
    public function getNumberPlaces()
    {
        return $this->numberPlaces;
    }

    /**
     * Set remainingPlaces
     *
     * @param integer $remainingPlaces
     *
     * @return Announcement
     */
    public function setRemainingPlaces($remainingPlaces)
    {
        $this->remainingPlaces = $remainingPlaces;

        return $this;
    }

    /**
     * Get remainingPlaces
     *
     * @return integer
     */
    public function getRemainingPlaces()
    {
        return $this->remainingPlaces;
    }

    /**
     * Set level
     *
     * @param \AnnouncementPartnerBundle\Entity\Level $level
     *
     * @return Announcement
     */
    public function setLevel(\AnnouncementPartnerBundle\Entity\Level $level = null)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return \AnnouncementPartnerBundle\Entity\Level
     */
    public function getLevel()
    {
        return $this->level;
    }
}
