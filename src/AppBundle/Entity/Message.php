<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MessageRepository")
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="idAnnouncer", referencedColumnName="id")
     */
    private $idAnnouncer;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="idViewer", referencedColumnName="id")
     */
    private $idViewer;
	
	/**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="idSender", referencedColumnName="id")
     */
    private $idSender;
	
	/**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="idReceiver", referencedColumnName="id")
     */
    private $idReceiver;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=true, length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255)
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="Announcement")
     * @ORM\JoinColumn(name="idAnnouncement", referencedColumnName="id")
     */
    private $idAnnouncement;
	
	/**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Conversation")
     * @ORM\JoinColumn(name="idConversationSend", referencedColumnName="id")
     */
    private $idConversationSend;
	
	/**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Conversation")
     * @ORM\JoinColumn(name="idConversationReceive", referencedColumnName="id")
     */
    private $idConversationReceive;

	/**
     * @var \Date
     *
     * @ORM\Column(name="dateMessage", type="datetime", nullable=true)
     */
    private $dateMessage;
	
	/**
     * @var boolean
     *
     * @ORM\Column(name="view", type="boolean", nullable=true)
     */
    private $view;
	

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    

    

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Message
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set idAnnouncement
     *
     * @param integer $idAnnouncement
     *
     * @return Message
     */
    public function setIdAnnouncement($idAnnouncement)
    {
        $this->idAnnouncement = $idAnnouncement;

        return $this;
    }

    /**
     * Get idAnnouncement
     *
     * @return int
     */
    public function getIdAnnouncement()
    {
        return $this->idAnnouncement;
    }

    /**
     * Set dateMessage
     *
     * @param \DateTime $dateMessage
     *
     * @return Message
     */
    public function setDateMessage($dateMessage)
    {
        $this->dateMessage = $dateMessage;

        return $this;
    }

    /**
     * Get dateMessage
     *
     * @return \DateTime
     */
    public function getDateMessage()
    {
        return $this->dateMessage;
    }

    /**
     * Set idAnnouncer
     *
     * @param integer $idAnnouncer
     *
     * @return Message
     */
    public function setIdAnnouncer($idAnnouncer)
    {
        $this->idAnnouncer = $idAnnouncer;

        return $this;
    }

    /**
     * Get idAnnouncer
     *
     * @return integer
     */
    public function getIdAnnouncer()
    {
        return $this->idAnnouncer;
    }

    /**
     * Set idViewer
     *
     * @param integer $idViewer
     *
     * @return Message
     */
    public function setIdViewer($idViewer)
    {
        $this->idViewer = $idViewer;

        return $this;
    }

    /**
     * Get idViewer
     *
     * @return integer
     */
    public function getIdViewer()
    {
        return $this->idViewer;
    }

    /**
     * Set idConversationSend
     *
     * @param integer $idConversationSend
     *
     * @return Message
     */
    public function setIdConversationSend($idConversationSend)
    {
        $this->idConversationSend = $idConversationSend;

        return $this;
    }

    /**
     * Get idConversationSend
     *
     * @return integer
     */
    public function getIdConversationSend()
    {
        return $this->idConversationSend;
    }

    /**
     * Set idConversationReceive
     *
     * @param integer $idConversationReceive
     *
     * @return Message
     */
    public function setIdConversationReceive($idConversationReceive)
    {
        $this->idConversationReceive = $idConversationReceive;

        return $this;
    }

    /**
     * Get idConversationReceive
     *
     * @return integer
     */
    public function getIdConversationReceive()
    {
        return $this->idConversationReceive;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return Message
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set view
     *
     * @param boolean $view
     *
     * @return Message
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view
     *
     * @return boolean
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set idSender
     *
     * @param \AppBundle\Entity\User $idSender
     *
     * @return MessageMaterial
     */
    public function setIdSender(\AppBundle\Entity\User $idSender = null)
    {
        $this->idSender = $idSender;

        return $this;
    }

    /**
     * Get idSender
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdSender()
    {
        return $this->idSender;
    }

    /**
     * Set idReceiver
     *
     * @param \AppBundle\Entity\User $idReceiver
     *
     * @return Message
     */
    public function setIdReceiver(\AppBundle\Entity\User $idReceiver = null)
    {
        $this->idReceiver = $idReceiver;

        return $this;
    }

    /**
     * Get idReceiver
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdReceiver()
    {
        return $this->idReceiver;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Message
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
