<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user",
*      uniqueConstraints={@ORM\UniqueConstraint(name="users_email_unique",columns={"email"})}
* )
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id" ,type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
     * @ORM\Column(name="name",type="string", nullable=true)
     */
    protected $name;
	
	/**
     * @ORM\Column(name="firstname",type="string", nullable=true)
     */
    protected $firstname;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="age", type="date", nullable=true)
     */
    protected $age;
	
	
	/**
     * @var string
     * @ORM\Column(name="bio", type="string", nullable=true)
     */
	 protected $bio;
	
	 
	 /**
     * @var string
     * @ORM\Column(name="levelReputation", type="string", nullable=true)
     */
	 protected $levelReputation;
	 
	 
	  /**
     * @var string
     * @ORM\Column(name="levelSport", type="string", nullable=true)
     */
	 protected $levelSport;
	 
	 /**
     * @var string
     * @ORM\Column(name="gender", type="string", nullable=true)
     */
	 protected $gender;
	 
	
	 
	 /**
     * @var int
     * @ORM\Column(name="numberPhone", type="integer", nullable=true)
     */
	 protected $numberPhone;
	 
	 
	 /**
     * @var string
     * @ORM\Column(name="stateLoc", type="string", nullable=true)
     */
	 protected $stateLoc;
	 
	 
	 
	 /**
     * @var string
     * @ORM\Column(name="city", type="string", nullable=true)
     */
    protected $city;
	
	
	
	
	/**
     * @var string
     * @ORM\Column(name="facebookID", type="string", nullable=true)
     */
    protected $facebookID;

	 /** @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true) */
    protected $facebook_access_token;
	
	
	/**
     * @var string
     * @ORM\Column(name="googleID", type="string", nullable=true)
     */
    protected $googleID;
	 
	
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
	
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $originalImage;
	
	/**
     * Set originalImage
     *
     * @param string $originalImage
     *
     * @return User
     */
    public function setOriginalImage($originalImage)
    {
        $this->originalImage = $originalImage;

        return $this;
    }

    /**
     * Get originalImage
     *
     * @return string
     */
    public function getOriginalImage()
    {
        return $this->originalImage;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set age
     *
     * @param \DateTime $age
     *
     * @return User
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return \DateTime
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set bio
     *
     * @param string $bio
     *
     * @return User
     */
    public function setBio($bio)
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get bio
     *
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Set levelReputation
     *
     * @param string $levelReputation
     *
     * @return User
     */
    public function setLevelReputation($levelReputation)
    {
        $this->levelReputation = $levelReputation;

        return $this;
    }

    /**
     * Get levelReputation
     *
     * @return string
     */
    public function getLevelReputation()
    {
        return $this->levelReputation;
    }

    /**
     * Set levelSport
     *
     * @param string $levelSport
     *
     * @return User
     */
    public function setLevelSport($levelSport)
    {
        $this->levelSport = $levelSport;

        return $this;
    }

    /**
     * Get levelSport
     *
     * @return string
     */
    public function getLevelSport()
    {
        return $this->levelSport;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set numberPhone
     *
     * @param integer $numberPhone
     *
     * @return User
     */
    public function setNumberPhone($numberPhone)
    {
        $this->numberPhone = $numberPhone;

        return $this;
    }

    /**
     * Get numberPhone
     *
     * @return integer
     */
    public function getNumberPhone()
    {
        return $this->numberPhone;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return User
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }
	
	/**
     * Set image
     *
     * @param string $image
     *
     * @return string
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
		return $this->image;
    }
	
	public function getUploadRootDir()
	{
		// absolute path to your directory where images must be saved
		return __DIR__.'/../../../web/'.$this->getUploadDir();
	}

	public function getUploadDir()
	{
		return 'Profile/'.$this->getId();
	}

	public function getAbsolutePath()
	{
		return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
	}

	public function getWebPath()
	{
		return null === $this->image ? null : '/'.$this->getUploadDir().'/'.$this->image;
	}

    /**
     * Set facebookID
     *
     * @param string $facebookID
     *
     * @return User
     */
    public function setFacebookID($facebookID)
    {
        $this->facebookID = $facebookID;

        return $this;
    }

    /**
     * Get facebookID
     *
     * @return string
     */
    public function getFacebookID()
    {
        return $this->facebookID;
    }

    /**
     * Set googleID
     *
     * @param string $googleID
     *
     * @return User
     */
    public function setGoogleID($googleID)
    {
        $this->googleID = $googleID;

        return $this;
    }

    /**
     * Get googleID
     *
     * @return string
     */
    public function getGoogleID()
    {
        return $this->googleID;
    }

    /**
     * Set stateLoc
     *
     * @param string $stateLoc
     *
     * @return User
     */
    public function setStateLoc($stateLoc)
    {
        $this->stateLoc = $stateLoc;

        return $this;
    }

    /**
     * Get stateLoc
     *
     * @return string
     */
    public function getStateLoc()
    {
        return $this->stateLoc;
    }
	
	/**
     * Set facebookAccessToken
     *
     * @param string $facebookAccessToken
     *
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebook_access_token = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebookAccessToken
     *
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebook_access_token;
    }
	
	/**
     * Gets the last login time.
     *
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

}
