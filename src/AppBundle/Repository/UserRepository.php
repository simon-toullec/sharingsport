<?php
// src/AppBundle/Repository/UserRepository.php
namespace AppBundle\Repository;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }
	
	
	public function findByAnnouncement($announcement){
		
		$query = $this->_em->createQuery('SELECT u, a FROM User u JOIN a.idUser a WHERE a.id = :announcement');
		  $query->setParameter('announcement', $announcement);
		  
		  // Utilisation de getSingleResult car la requête ne doit retourner qu'un seul résultat
		  return $query->getSingleResult();
		
	}
	

}