<?php

namespace AppBundle\Repository;

/**
 * AnnouncementRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AnnouncementRepository extends \Doctrine\ORM\EntityRepository
{
	
	/* ======MATERIAL====== */
	
	/*public function findByDateB($place,$sport,$dateBegin)
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('a')
       ->from('AppBundle:Announcement', 'a')
       ->where('a.dateBegin = :datebegin')
         ->setParameter('dateBegin', $dateBegin)
       ->andWhere('a.sport = : sport')
		->setParameter('sport', $sport)
	   ->andWhere('a.place = : place')
	    ->setParameter('place', $place);

        return $qb->getQuery()
               ->getResult();
    }*/


	public function findByDateBegMat($place,$sport,$dateBegin, $dateEnd,$user){
		/*ici on cherche toutes les annonces materiels selon le lieu, le sport
		et la date de début, qui n'ont pas été réservées, et qui n'affiche pas les annonces de l'utilisateur
		*/
	  $reservation = $this->_em->createQuery('SELECT r FROM AnnouncementMaterialBundle:ReservationMaterial r');
		$isValid = 1;

	  $qb = $this->createQueryBuilder('a');
	  $qb->where('a.place = :place')
		   ->setParameter('place', $place)
		  ->andWhere('a.sport = :sport')
		   ->setParameter('sport', $sport)
		 ->andWhere('a.dateBegin = :dateBegin')
		   ->setParameter('dateBegin', $dateBegin)
		  ->andWhere('a.dateEnd = :dateEnd')
		   ->setParameter('dateEnd', $dateEnd)
		 ->andWhere('a.isValid = :isValid')
			->setParameter('isValid', $isValid)
		 ->andWhere('a.id not in (select IDENTITY(r.idAnnouncement) From AnnouncementMaterialBundle:ReservationMaterial r)')
		 /*->andWhere('a.id not in (select an.id FROM AppBundle:Announcement an WHERE an.idUser = :user)')
			->setParameter('user', $user)*/;

	  //$qb->where($qb->expr()->notIn('r', $reservation));

	  return $qb
		->getQuery()
		->getResult()
	  ;
	}
	
	public function findByArchiveMat($now){
		
		/*return $this->getEntityManager() ->createQuery('SELECT a FROM AppBundle:Announcement a WHERE DATEDIFF("2017-06-28","2017-07-28")>60 and a.type in("material")')->getResult();
		*/
		$material = 'material';
		$qb = $this->createQueryBuilder('a');
		$qb->where('DATE_DIFF(:now,a.dateAnnouncement)>60')
			->setParameter('now',$now)
			->andWhere('a.type in(:material)')
				->setParameter('material',$material);
					
		return $qb
		->getQuery()
		->getResult()
	  ;
	}
	
	public function findByArchivePart($now){
		
		$partner = 'partner';
		$qb = $this->createQueryBuilder('a');
		$qb->where('DATE_DIFF(:now,a.dateAnnouncement)>60')
			->setParameter('now',$now)
			->andWhere('a.type in(:partner)')
				->setParameter('partner',$partner);
					
		return $qb
		->getQuery()
		->getResult()
	  ;
		
	}

	public function findByDateBegUnConnectedMat($place,$sport,$dateBegin,$dateEnd){
		/*ici on cherche toutes les annonces materiels selon le lieu, le sport
		et la date de début, qui n'ont pas été réservées, et qui n'affiche pas les annonces de l'utilisateur
		*/
	  $reservation = $this->_em->createQuery('SELECT r FROM AnnouncementMaterialBundle:ReservationMaterial r');
	  $isValid = 1;

	  $qb = $this->createQueryBuilder('a');
	  $qb->where('a.place = :place')
		   ->setParameter('place', $place)
		  ->andWhere('a.sport = :sport')
		   ->setParameter('sport', $sport)
		 ->andWhere('a.dateBegin = :dateBegin')
		   ->setParameter('dateBegin', $dateBegin)
		  ->andWhere('a.dateEnd = :dateEnd')
		   ->setParameter('dateEnd', $dateEnd)
		 ->andWhere('a.isValid = :isValid')
			->setParameter('isValid', $isValid)
		 ->andWhere('a.id not in (select IDENTITY(r.idAnnouncement) From AnnouncementMaterialBundle:ReservationMaterial r)')
		 ;

	  //$qb->where($qb->expr()->notIn('r', $reservation));

	  return $qb
		->getQuery()
		->getResult()
	  ;
	}
	
	public function findByDateBegUnConnectedDescMat($place,$sport,$dateBegin,$dateEnd){
		/*ici on cherche toutes les annonces materiels selon le lieu, le sport
		et la date de début, qui n'ont pas été réservées, et qui n'affiche pas les annonces de l'utilisateur
		*/
	  $reservation = $this->_em->createQuery('SELECT r FROM AnnouncementMaterialBundle:ReservationMaterial r');
	  $isValid = 1;

	  $qb = $this->createQueryBuilder('a');
	  $qb->where('a.place = :place')
		   ->setParameter('place', $place)
		  ->andWhere('a.sport = :sport')
		   ->setParameter('sport', $sport)
		 ->andWhere('a.dateBegin = :dateBegin')
		   ->setParameter('dateBegin', $dateBegin)
		  ->andWhere('a.dateEnd = :dateEnd')
		   ->setParameter('dateEnd', $dateEnd)
		 ->andWhere('a.isValid = :isValid')
			->setParameter('isValid', $isValid)
		 ->andWhere('a.id not in (select IDENTITY(r.idAnnouncement) From AnnouncementMaterialBundle:ReservationMaterial r)')
		 ->orderBy('a.price', 'DESC');

	  //$qb->where($qb->expr()->notIn('r', $reservation));

	  return $qb
		->getQuery()
		->getResult()
	  ;
	}
	
	public function findByDateBegUnConnectedAscMat($place,$sport,$dateBegin,$dateEnd){
		/*ici on cherche toutes les annonces materiels selon le lieu, le sport
		et la date de début, qui n'ont pas été réservées, et qui n'affiche pas les annonces de l'utilisateur
		*/
	  $reservation = $this->_em->createQuery('SELECT r FROM AnnouncementMaterialBundle:ReservationMaterial r');
	  $isValid = 1;

	  $qb = $this->createQueryBuilder('a');
	  $qb->where('a.place = :place')
		   ->setParameter('place', $place)
		  ->andWhere('a.sport = :sport')
		   ->setParameter('sport', $sport)
		 ->andWhere('a.dateBegin = :dateBegin')
		   ->setParameter('dateBegin', $dateBegin)
		  ->andWhere('a.dateEnd = :dateEnd')
		   ->setParameter('dateEnd', $dateEnd)
		 ->andWhere('a.isValid = :isValid')
			->setParameter('isValid', $isValid)
		 ->andWhere('a.id not in (select IDENTITY(r.idAnnouncement) From AnnouncementMaterialBundle:ReservationMaterial r)')
		 ->orderBy('a.price', 'ASC');

	  //$qb->where($qb->expr()->notIn('r', $reservation));

	  return $qb
		->getQuery()
		->getResult()
	  ;
	}

	public function findByCountAnnouncementPart($place){

	  $query = $this->_em->createQuery('SELECT count(a) FROM AppBundle:Announcement a WHERE a.place = :place');
		  $query->setParameter('place', $place);

		  // Utilisation de getSingleResult car la requête ne doit retourner qu'un seul résultat
		  return $query->getSingleResult();
	}
	
	
	public function findByCountAnnouncementMat($place){
		$type='material';
	  $query = $this->_em->createQuery('SELECT count(a) FROM AppBundle:Announcement a WHERE a.place = :place AND a.type = :type');
		  $query->setParameter('place', $place)
		  ->setParameter('type', $type);

		  // Utilisation de getSingleResult car la requête ne doit retourner qu'un seul résultat
		  return $query->getSingleResult();
	}


	public function findByCountAnnouncementUser($user){

	  $query = $this->_em->createQuery('SELECT count(a) FROM AppBundle:Announcement a WHERE a.idUser = :user');
		  $query->setParameter('user', $user);

		  // Utilisation de getSingleResult car la requête ne doit retourner qu'un seul résultat
		  return $query->getSingleResult();
	}

	public function findByFilterMat($place,$sport,$dateBegin, $minPrice,$maxPrice,$dateEnd, $cleanliness, $quality, $order){
		
		if($order == 1){
			
			if($quality == 6){//si la qualité n'est pas spécifiée

				if($cleanliness == 0){


					$qb = $this->createQueryBuilder('a');
					$qb->where('a.price BETWEEN :minPrice AND :maxPrice')
						->setParameter('minPrice', $minPrice)
						->setParameter('maxPrice', $maxPrice)
				  ->andWhere('a.dateEnd = :dateEnd')
				   ->setParameter('dateEnd', $dateEnd)
				 ->andWhere('a.cleanliness in(0)')
					->andWhere('a.place = :place')
				   ->setParameter('place', $place)
				  ->andWhere('a.sport = :sport')
				   ->setParameter('sport', $sport)
				 ->andWhere('a.dateBegin = :dateBegin')
				   ->setParameter('dateBegin', $dateBegin)
				   ->orderBy('a.price', 'ASC');





				}else if($cleanliness == 1){
					$qb = $this->createQueryBuilder('a');
					$qb->where('a.price BETWEEN :minPrice AND :maxPrice')
				   ->setParameter('minPrice', $minPrice)
				   ->setParameter('maxPrice', $maxPrice)
				  ->andWhere('a.dateEnd = :dateEnd')
				   ->setParameter('dateEnd', $dateEnd)
				 ->andWhere('a.cleanliness in(1)')
				 ->andWhere('a.place = :place')
				   ->setParameter('place', $place)
				  ->andWhere('a.sport = :sport')
				   ->setParameter('sport', $sport)
				 ->andWhere('a.dateBegin = :dateBegin')
				   ->setParameter('dateBegin', $dateBegin)
				   ->orderBy('a.price', 'ASC');
				   /*echo 'place='.$place;
				    echo 'sport='.$sport;
					 echo 'dateBegin='.$dateBegin;
					 echo 'dateEnd='.$dateEnd;
					  echo 'minprice='.$minPrice;
					   echo 'maxprice='.$maxPrice;
					    echo 'cleanliness='.$cleanliness;
						 echo 'quality='.$quality;*/
				   //echo ''.$t;
				   

				}else{
					$qb = $this->createQueryBuilder('a');
					$qb->where('a.price BETWEEN :minPrice AND :maxPrice')
				   ->setParameter('minPrice', $minPrice)
				   ->setParameter('maxPrice', $maxPrice)
				  ->andWhere('a.dateEnd = :dateEnd')
				   ->setParameter('dateEnd', $dateEnd)
				 ->andWhere('a.cleanliness in(0,1)')
				 ->andWhere('a.place = :place')
				   ->setParameter('place', $place)
				  ->andWhere('a.sport = :sport')
				   ->setParameter('sport', $sport)
				 ->andWhere('a.dateBegin = :dateBegin')
				   ->setParameter('dateBegin', $dateBegin)
				   ->orderBy('a.price', 'ASC');
				}
			}else{
				if($cleanliness == 0){


					$qb = $this->createQueryBuilder('a');
					$qb->where('a.price BETWEEN :minPrice AND :maxPrice')
				   ->setParameter('minPrice', $minPrice)
				   ->setParameter('maxPrice', $maxPrice)
				  ->andWhere('a.dateEnd = :dateEnd')
				   ->setParameter('dateEnd', $dateEnd)
				 ->andWhere('a.cleanliness in(0)')
				 ->andWhere('a.place = :place')
				   ->setParameter('place', $place)
				  ->andWhere('a.sport = :sport')
				   ->setParameter('sport', $sport)
				 ->andWhere('a.dateBegin = :dateBegin')
				   ->setParameter('dateBegin', $dateBegin)
				   ->orderBy('a.price', 'ASC');


				}else if($cleanliness == 1){

					$qb = $this->createQueryBuilder('a');
					$qb->where('a.price BETWEEN :minPrice AND :maxPrice')
				   ->setParameter('minPrice', $minPrice)
				   ->setParameter('maxPrice', $maxPrice)
				  ->andWhere('a.dateEnd = :dateEnd')
				   ->setParameter('dateEnd', $dateEnd)
				   ->andWhere('a.quality = :quality')
				   ->setParameter('quality', $quality)
				 ->andWhere('a.cleanliness in(1)')
				 ->andWhere('a.place = :place')
				   ->setParameter('place', $place)
				  ->andWhere('a.sport = :sport')
				   ->setParameter('sport', $sport)
				 ->andWhere('a.dateBegin = :dateBegin')
				   ->setParameter('dateBegin', $dateBegin)
				   ->orderBy('a.price', 'ASC');

				}else{

					$qb = $this->createQueryBuilder('a');
					$qb->where('a.price BETWEEN :minPrice AND :maxPrice')
				   ->setParameter('minPrice', $minPrice)
				   ->setParameter('maxPrice', $maxPrice)
				  ->andWhere('a.dateEnd = :dateEnd')
				   ->setParameter('dateEnd', $dateEnd)
				   ->andWhere('a.quality = :quality')
				   ->setParameter('quality', $quality)
				 ->andWhere('a.cleanliness in(0,1)')
				 -> andWhere('a.place = :place')
				   ->setParameter('place', $place)
				  ->andWhere('a.sport = :sport')
				   ->setParameter('sport', $sport)
				 ->andWhere('a.dateBegin = :dateBegin')
				   ->setParameter('dateBegin', $dateBegin)
				   ->orderBy('a.price', 'ASC');
				}

			}
			
			
		}else{//order ==0
			if($quality == 6){//si la qualité n'est pas spécifiée

				if($cleanliness == 0){


					$qb = $this->createQueryBuilder('a');
					$qb->where('a.price BETWEEN :minPrice AND :maxPrice')
						->setParameter('minPrice', $minPrice)
						->setParameter('maxPrice', $maxPrice)
				  ->andWhere('a.dateEnd = :dateEnd')
				   ->setParameter('dateEnd', $dateEnd)
				 ->andWhere('a.cleanliness in(0)')
					->andWhere('a.place = :place')
				   ->setParameter('place', $place)
				  ->andWhere('a.sport = :sport')
				   ->setParameter('sport', $sport)
				 ->andWhere('a.dateBegin = :dateBegin')
				   ->setParameter('dateBegin', $dateBegin)
				   ->orderBy('a.price', 'DESC');





				}else if($cleanliness == 1){
					$qb = $this->createQueryBuilder('a');
					$qb->where('a.price BETWEEN :minPrice AND :maxPrice')
				   ->setParameter('minPrice', $minPrice)
				   ->setParameter('maxPrice', $maxPrice)
				  ->andWhere('a.dateEnd = :dateEnd')
				   ->setParameter('dateEnd', $dateEnd)
				 ->andWhere('a.cleanliness in(1)')
				 ->andWhere('a.place = :place')
				   ->setParameter('place', $place)
				  ->andWhere('a.sport = :sport')
				   ->setParameter('sport', $sport)
				 ->andWhere('a.dateBegin = :dateBegin')
				   ->setParameter('dateBegin', $dateBegin)
				   ->orderBy('a.price', 'DESC');
				   

				}else{
					$qb = $this->createQueryBuilder('a');
					$qb->where('a.price BETWEEN :minPrice AND :maxPrice')
				   ->setParameter('minPrice', $minPrice)
				   ->setParameter('maxPrice', $maxPrice)
				  ->andWhere('a.dateEnd = :dateEnd')
				   ->setParameter('dateEnd', $dateEnd)
				 ->andWhere('a.cleanliness in(0,1)')
				 ->andWhere('a.place = :place')
				   ->setParameter('place', $place)
				  ->andWhere('a.sport = :sport')
				   ->setParameter('sport', $sport)
				 ->andWhere('a.dateBegin = :dateBegin')
				   ->setParameter('dateBegin', $dateBegin)
				   ->orderBy('a.price', 'DESC');
				}
			}else{//order ==0 quality!=6
				if($cleanliness == 0){


					$qb = $this->createQueryBuilder('a');
					$qb->where('a.price BETWEEN :minPrice AND :maxPrice')
				   ->setParameter('minPrice', $minPrice)
				   ->setParameter('maxPrice', $maxPrice)
				  ->andWhere('a.dateEnd = :dateEnd')
				   ->setParameter('dateEnd', $dateEnd)
				   ->andWhere('a.quality = :quality')
				   ->setParameter('quality', $quality)
				 ->andWhere('a.cleanliness in(0)')
				 ->andWhere('a.place = :place')
				   ->setParameter('place', $place)
				  ->andWhere('a.sport = :sport')
				   ->setParameter('sport', $sport)
				 ->andWhere('a.dateBegin = :dateBegin')
				   ->setParameter('dateBegin', $dateBegin)
				   ->orderBy('a.price', 'DESC');


				}else if($cleanliness == 1){

					$qb = $this->createQueryBuilder('a');
					$qb->where('a.price BETWEEN :minPrice AND :maxPrice')
				   ->setParameter('minPrice', $minPrice)
				   ->setParameter('maxPrice', $maxPrice)
				  ->andWhere('a.dateEnd = :dateEnd')
				   ->setParameter('dateEnd', $dateEnd)
				   ->andWhere('a.quality = :quality')
				   ->setParameter('quality', $quality)
				 ->andWhere('a.cleanliness in(1)')
				 ->andWhere('a.place = :place')
				   ->setParameter('place', $place)
				  ->andWhere('a.sport = :sport')
				   ->setParameter('sport', $sport)
				 ->andWhere('a.dateBegin = :dateBegin')
				   ->setParameter('dateBegin', $dateBegin)
				   ->orderBy('a.price', 'DESC');

				}else{

					$qb = $this->createQueryBuilder('a');
					$qb->where('a.price BETWEEN :minPrice AND :maxPrice')
				   ->setParameter('minPrice', $minPrice)
				   ->setParameter('maxPrice', $maxPrice)
				  ->andWhere('a.dateEnd = :dateEnd')
				   ->setParameter('dateEnd', $dateEnd)
				   ->andWhere('a.quality = :quality')
				   ->setParameter('quality', $quality)
				 ->andWhere('a.cleanliness in(0,1)')
				 -> andWhere('a.place = :place')
				   ->setParameter('place', $place)
				  ->andWhere('a.sport = :sport')
				   ->setParameter('sport', $sport)
				 ->andWhere('a.dateBegin = :dateBegin')
				   ->setParameter('dateBegin', $dateBegin)
				   ->orderBy('a.price', 'DESC');
				}

			}
		}
		



		return $qb
		->getQuery()
		->getResult()
	  ;
	}
	
	
	
	
	
	
	
	
	
	
	/* ======PARTNER====== */
	public function findByDateBegPart($place,$sport,$dateBegin,$user,$time){
		/*ici on cherche toutes les annonces materiels selon le lieu, le sport
		et la date de début, qui n'ont pas été réservées, et qui n'affiche pas les annonces de l'utilisateur
		*/
	  $reservation = $this->_em->createQuery('SELECT p FROM AnnouncementPartnerBundle:Participation p');
	  
	 
	  $qb = $this->createQueryBuilder('a');
	  $qb->where('a.place = :place')
		   ->setParameter('place', $place)
		  ->andWhere('a.sport = :sport')
		   ->setParameter('sport', $sport)
		 ->andWhere('a.dateBegin = :dateBegin')
		   ->setParameter('dateBegin', $dateBegin)
		 ->andWhere('a.time = :time')
		   ->setParameter('time', $time)
		 ->andWhere('a.id not in (select IDENTITY(p.idAnnouncement) From AnnouncementPartnerBundle:Participation p)')
		 ->andWhere('a.id not in (select an.id FROM AppBundle:Announcement an WHERE an.idUser = :user and an.remainingPlaces in(0)
		 and an.isValid in(0))')
			->setParameter('user', $user);
		   
	  //$qb->where($qb->expr()->notIn('r', $reservation));

	  return $qb
		->getQuery()
		->getResult()
	  ;
	}
	
	public function findByDateBegUnConnectedPart($place,$sport,$dateBegin, $time){
		/*ici on cherche toutes les annonces materiels selon le lieu, le sport
		et la date de début, qui n'ont pas été réservées, et qui n'affiche pas les annonces de l'utilisateur
		*/
	  $reservation = $this->_em->createQuery('SELECT p FROM AnnouncementPartnerBundle:Participation p');
	  
	 
	  $qb = $this->createQueryBuilder('a');
	  $qb->where('a.place = :place')
		   ->setParameter('place', $place)
		  ->andWhere('a.sport = :sport')
		   ->setParameter('sport', $sport)
		 ->andWhere('a.dateBegin = :dateBegin')
		   ->setParameter('dateBegin', $dateBegin)
		 ->andWhere('a.time = :time')
		   ->setParameter('time', $time)
		 ->andWhere('a.id not in (select an.id FROM AppBundle:Announcement an WHERE an.remainingPlaces in(0) and an.isValid in(0))');
		   
	  //$qb->where($qb->expr()->notIn('r', $reservation));

	  return $qb
		->getQuery()
		->getResult()
	  ;
	}

	public function findByFilterPart($place,$sport,$dateBegin, $numberPlacesMin, $numberPlacesMax, $timeMin, $timeMax, $level){

		if($level == 6){//si la qualité n'est pas spécifiée

			


				$qb = $this->createQueryBuilder('a');
				$qb->where('a.numberPlaces BETWEEN :numberPlacesMin AND :numberPlacesMax')
					->setParameter('numberPlacesMin', $numberPlacesMin)
					->setParameter('numberPlacesMax', $numberPlacesMax)
					->andWhere('a.time BETWEEN = :timeMin AND = :timeMax')
					->setParameter('timeMin', $timeMin)
					->setParameter('timeMax', $timeMax)
				->andWhere('a.place = :place')
			   ->setParameter('place', $place)
			  ->andWhere('a.sport = :sport')
			   ->setParameter('sport', $sport)
			 ->andWhere('a.dateBegin = :dateBegin')
			   ->setParameter('dateBegin', $dateBegin);





			
			
		}else{
			


				$qb = $this->createQueryBuilder('a');
				$qb->where('a.numberPlaces BETWEEN :numberPlacesMin AND :numberPlacesMax')
			   ->setParameter('numberPlacesMin', $numberPlacesMin)
			   ->setParameter('numberPlacesMax', $numberPlacesMax)
				->andWhere('a.time BETWEEN :timeMin AND :timeMax')
					->setParameter('timeMin', $timeMin)
					->setParameter('timeMax', $timeMax)
			   ->andWhere('a.level = :level')
			   ->setParameter('level', $level)
			 ->andWhere('a.place = :place')
			   ->setParameter('place', $place)
			  ->andWhere('a.sport = :sport')
			   ->setParameter('sport', $sport)
			 ->andWhere('a.dateBegin = :dateBegin')
			   ->setParameter('dateBegin', $dateBegin);


			
			

		}



		return $qb
		->getQuery()
		->getResult()
	  ;
	}
	
	

}
