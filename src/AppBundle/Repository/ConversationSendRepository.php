<?php

namespace AppBundle\Repository;

/**
 * ConversationSendRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ConversationSendRepository extends \Doctrine\ORM\EntityRepository
{
	
	public function findConversationNull($usr,$idAnnoun){
		
	 
	 /* $query = $this->_em->createQuery('SELECT a FROM AppBundle:AnnouncementMaterial a, AppBundle:ConversationSend cs WHERE a.id = :cs.idAnnouncement and a.idAnnouncement = :idAnnoun
	  and a.idUser = :usr');
		  $query->setParameters(array('usr'=> $usr,
		  'idAnnoun'=> $idAnnoun,));*/
		  /*$query = $this->_em->createQuery('SELECT cs FROM AppBundle:AnnouncementMaterial a JOIN a.id cs where cs.idAnnouncement = :idAnnoun
			and a.idUser = :usr');
		  $query->setParameters(array('usr'=> $usr,
		  'idAnnoun'=> $idAnnoun,));*/
		  
		  
		  
		  // Utilisation de getSingleResult car la requête ne doit retourner qu'un seul résultat
		  //return $query->getSingleResult();
		  //return $query->getResult();
		  
		  $qb = $this->createQueryBuilder('cs');

			// On fait une jointure avec l'entité Category avec pour alias « c »
			$qb
			  ->join('cs.idAnnouncement', 'an')
			  ->addSelect('an')
			  ->andWhere(' an.idUser = :usr')
			  ->setParameter('usr',$usr)
			  ->andWhere(' an.id = :idAnnouncement')
			  ->setParameter('idAnnouncement',$idAnnoun)
			  
			;
			
			return $qb
			->getQuery()
			->getResult()
			;
	}
}
