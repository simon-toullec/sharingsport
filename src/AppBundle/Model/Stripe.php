<?php
namespace AppBundle\Model;
use Symfony\Component\Config\Definition\Exception\Exception;
class Stripe{

	private $api_key;

	public function __construct(string $api_key){
		$this->api_key = $api_key;

	}

	public function api($endpoint, array $data) {// :stdClass 30min
		$ch = curl_init();

		//curl est une librairie PHP
		curl_setopt_array(//ça permet de récupérer les informations de l'utilisateur
			$ch, [
				CURLOPT_URL => "https://api.stripe.com/v1/$endpoint",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_USERPWD => $this->api_key,
				CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
				CURLOPT_POSTFIELDS => http_build_query($data)
			]

		);

		$response = json_decode(curl_exec($ch));

		curl_close($ch);
		//si l'objet token existe déjà on gère l'erreur:
		/*if(property_exists($response, 'error')){
		throw new Exception($response->error->message);


	}*/
	//var_dump($response);

	return $response;

}


}
