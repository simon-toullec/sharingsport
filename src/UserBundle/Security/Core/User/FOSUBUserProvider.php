<?php
namespace UserBundle\Security\Core\User;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserChecker;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\GraphUser;
use Facebook\Facebook;
use Facebook\FacebookResponse;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Exceptions\FacebookResponseException;
use AppBundle\Entity\Image;

class FOSUBUserProvider extends BaseClass
{
    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $username = $response->getUsername();
        //on connect - get the access token and the user ID
        $service = $response->getResourceOwner()->getName();
        $setter = 'set'.ucfirst($service);
        $setter_id = $setter.'Id';
        $setter_token = $setter.'AccessToken';
        //we "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }
        //we connect current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());
        $this->userManager->updateUser($user);
    }
    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $user = $this->userManager->findUserBy(array($this->getProperty($response) => $username));
        $email = $response->getEmail();
		//when the user is registrating
        if (null === $user) {
			 //check if the user has a normal account
            $user = $this->userManager->findUserByEmail($email);
			
            if (null === $user || !$user instanceof UserInterface) {
                //if the user does not have a normal account, set it up:
                $user = $this->userManager->createUser();
                $user->setEmail($email);
                $user->setPlainPassword(md5(uniqid()));
                $user->setEnabled(true);

            }
            $fb = new Facebook([
				  'app_id' => '255611071557186',
				  'app_secret' => '4f235d4b148a4e404056900d47e113db',
				  'default_graph_version' => 'v2.8',
				  ]);

			try {
			  // Returns a `Facebook\FacebookResponse` object
			  $response1 = $fb->get('/me?fields=id,name,gender', $response->getAccessToken());
			} catch(FacebookResponseException $e) {
			  echo 'Graph returned an error: ' . $e->getMessage();
			  exit;
			} catch(FacebookSDKException $e) {
			  echo 'Facebook SDK returned an error: ' . $e->getMessage();
			  exit;
			}

			$response2 = $fb->get('/me?locale=en_US&fields=id,last_name,first_name,email,picture,birthday', $response->getAccessToken());
			$userData = $response2->getDecodedBody();
			$name = $response1->getGraphUser();
			$user->setUsername($name['name']);
			$user->setUsernameCanonical($name['name']);
			
			
			$providerName = $response->getResourceOwner()->getName();
			$providerNameSetter = 'set'.ucfirst($providerName).'Id';
			$user->$providerNameSetter($response->getUsername());
			$user->setGender($name['gender']);
			
			
			
			
			//then set its corresponding social id
			$service = $response->getResourceOwner()->getName();
			switch ($service) {
				case 'google':
					$user->setGoogleID($username);
					break;
				case 'facebook':
					$user->setFacebookID($username);
					break;
			}
			$this->userManager->updateUser($user);
			
			if( is_dir('./Profile/'.$user->getId()) == false){
				//suppression de l'image
				mkdir('./Profile/'.$user->getId());
				
				$url = $userData["picture"]["data"]["url"];
				$name = basename($url);
				list($image) = split('[?]', $name);
				$upload = file_put_contents("./Profile/".$user->getId()."/".$image,file_get_contents($url));
				$user->setImage($image);
				$user->setOriginalImage($image);
			}
			
			
			
			
			
			
			return $user;
			
        } else{
			//if user exists - go with the HWIOAuth way
			$user = parent::loadUserByOAuthUserResponse($response);
			$serviceName = $response->getResourceOwner()->getName();
			$setter = 'set' . ucfirst($serviceName) . 'AccessToken';
			//update access token
			$user->$setter($response->getAccessToken());
			return $user;
		}
        
    }
}